package com.gitlab.thomasreader.tennisorganiser.core.navigation

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.QuantityType
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

abstract class KeyedViewModel<T : DefaultFragmentKey>(
    _key: DefaultFragmentKey
) : ViewModel(), KeyProvider<T> {
    final override val key: T

    init {
        try {
            @Suppress("UNCHECKED_CAST")
            this.key = _key as T
        } catch (classCastException: ClassCastException) {
            val msg = buildString(42 + 15 + 30) {
                append("Invalid key argument provided to ViewModel: ${this::class.java.simpleName}")
                classCastException.message?.let {
                    append("\n")
                    append(it)
                }
            }
            throw ClassCastException(msg)
        }
    }
}

abstract class AbstractViewModel<T : DefaultFragmentKey>(
    _key: DefaultFragmentKey,
    protected val database: TennisDatabase,
    protected val dataStore: DataStore<Preferences>
) : KeyedViewModel<T>(_key)

abstract class AbstractSavedViewModel<T : DefaultFragmentKey>(
    _key: DefaultFragmentKey,
    protected val database: TennisDatabase,
    protected val dataStore: DataStore<Preferences>,
    protected val savedStateHandle: SavedStateHandle
) : KeyedViewModel<T>(_key) {

    class NonNullRWProperty<T, K: DefaultFragmentKey>(
        private val key: String,
        private val defaultValue: T
    ) : ReadWriteProperty<AbstractSavedViewModel<K>, T> {
        override fun getValue(
            thisRef: AbstractSavedViewModel<K>,
            property: KProperty<*>
        ): T {
            return thisRef.savedStateHandle.get(key) ?: this.defaultValue
        }

        override fun setValue(
            thisRef: AbstractSavedViewModel<K>,
            property: KProperty<*>,
            value: T
        ) {
            thisRef.savedStateHandle.set(key, value)
        }
    }

    open class RWProperty<T, K: DefaultFragmentKey>(
        private val key: String,
        private val defaultValue: T
    ) : ReadWriteProperty<AbstractSavedViewModel<K>, T?> {
        private var currentValue: T? = null

        override fun getValue(
            thisRef: AbstractSavedViewModel<K>,
            property: KProperty<*>
        ): T? {
            if (this.currentValue == null) {
                this.currentValue = thisRef.savedStateHandle.get(key) ?: this.defaultValue
            }
            return this.currentValue
        }

        override fun setValue(
            thisRef: AbstractSavedViewModel<K>,
            property: KProperty<*>,
            value: T?
        ) {
            if (value == null) {
                thisRef.savedStateHandle.remove<T>(key)
            } else {
                thisRef.savedStateHandle.set(key, value)
            }
            this.currentValue = value
        }
    }

    open class MappedRWProperty<T, S>(
        private val key: String,
        private val defaultValue: T,
        private val serialiser: (T) -> S,
        private val deserialiser: (S) -> T
    ) : ReadWriteProperty<AbstractSavedViewModel<out Nothing>, T?> {
        private var currentValue: T? = null

        override fun getValue(
            thisRef: AbstractSavedViewModel<out Nothing>,
            property: KProperty<*>
        ): T? {
            if (this.currentValue == null) {
                this.currentValue = thisRef.savedStateHandle.get<S?>(key)?.let { this.deserialiser(it) } ?: this.defaultValue
            }
            return this.currentValue
        }

        override fun setValue(
            thisRef: AbstractSavedViewModel<out Nothing>,
            property: KProperty<*>,
            value: T?
        ) {
            if (value == null) {
                thisRef.savedStateHandle.remove<T>(key)
            } else {
                thisRef.savedStateHandle.set(key, this.serialiser(value))
            }
            this.currentValue = value
        }
    }
}

inline fun <reified T, K: DefaultFragmentKey> AbstractSavedViewModel<K>.savedState(
    key: String
): ReadWriteProperty<AbstractSavedViewModel<K>, T?> {
    return savedState<T?, K>(key, null)
}

inline fun <reified T, K: DefaultFragmentKey> AbstractSavedViewModel<K>.savedState(
    key: String,
    defaultValue: T
): ReadWriteProperty<AbstractSavedViewModel<K>, T?> {
    return AbstractSavedViewModel.RWProperty<T, K>(key, defaultValue)
}

inline fun <reified T, K: DefaultFragmentKey> AbstractSavedViewModel<K>.nonNullSavedState(
    key: String,
    defaultValue: T
): ReadWriteProperty<AbstractSavedViewModel<K>, T> {
    return AbstractSavedViewModel.NonNullRWProperty<T, K>(key, defaultValue)
}

inline fun <reified T: QuantityType> droidUnitSavedState(
    key: String,
    defaultValue: DroidUnit<T>
): ReadWriteProperty<AbstractSavedViewModel<out Nothing>, DroidUnit<T>?> {
    val deserialiser = when (T::class.java) {
        is Length -> DroidUnits.LengthUnits::getUnit
        is Mass -> DroidUnits.MassUnits::getUnit
        is Area -> DroidUnits.AreaUnits::getUnit
        else -> TODO()
    }
    return AbstractSavedViewModel.MappedRWProperty(
        key,
        defaultValue,
        { unit: DroidUnit<*> -> unit.name },
        deserialiser as (String) -> DroidUnit<T>
    )
}