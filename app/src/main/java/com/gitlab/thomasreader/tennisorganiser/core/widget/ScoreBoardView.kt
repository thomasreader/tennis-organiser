package com.gitlab.thomasreader.tennisorganiser.core.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.core.extension.visibleWhen
import com.gitlab.thomasreader.tennisorganiser.databinding.ScoreboardViewBinding

class ScoreBoardView: ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    val binding: ScoreboardViewBinding =
        ScoreboardViewBinding.inflate(LayoutInflater.from(context),this)

    var seed: Int? = null
        set(value) {
            if (field != value) {
                this.binding.playerSeed.visibleWhen(value != null)
                this.binding.playerSeed.text = value?.toString()
                field = value
            }
        }

    var opponentSeed: Int? = null
        set(value) {
            if (field != value) {
                this.binding.opponentSeed.visibleWhen(value != null)
                this.binding.opponentSeed.text = value?.toString()
                field = value
            }
        }

    var winner: Int = 0
        set(value) {
            if (field != value) {
                if (value > 0) {
                    this.binding.playerWon.visible()
                    this.binding.opponentWon.gone()
                } else if (value < 0) {
                    this.binding.opponentWon.visible()
                    this.binding.playerWon.gone()
                } else {
                    this.binding.playerWon.gone()
                    this.binding.opponentWon.gone()
                }
                field = value
            }
        }

    private var playerName: CharSequence? = null
        set(value) {
            if (field != value) {
                this.binding.playerName.text = value
                field = value
            }
        }

    private var partnerName: CharSequence? = null
        set(value) {
            if (field != value) {
                this.binding.partnerName.text = value
                field = value
            }
        }

    private var opponentOneName: CharSequence? = null
        set(value) {
            if (field != value) {
                this.binding.opponentNameOne.text = value
                field = value
            }
        }

    private var opponentTwoName: CharSequence? = null
        set(value) {
            if (field != value) {
                this.binding.opponentNameTwo.text = value
                field = value
            }
        }

    fun setSinglesPlayers(player: CharSequence, opponent: CharSequence) {
        this.playerName = player
        this.opponentOneName = opponent
        this.binding.doublesGroup.gone()
    }

    fun setDoublesPlayers(
        player: CharSequence,
        partner: CharSequence,
        opponentOne: CharSequence,
        opponentTwo: CharSequence
    ) {
        this.playerName = player
        this.partnerName = partner
        this.opponentOneName = opponentOne
        this.opponentTwoName = opponentTwo
        this.binding.doublesGroup.visible()
    }

    private var setOne: Pair<CharSequence, CharSequence>? = null
        set(value) {
            if (field?.first != value?.first) {
                this.binding.playerSet1.text = value?.first ?: "-"
            }
            if (field?.second != value?.second) {
                this.binding.opponentSet1.text = value?.second ?: "-"
            }
            field = value
        }

    private var setTwo: Pair<CharSequence, CharSequence>? = null
        set(value) {
            if (field?.first != value?.first) {
                this.binding.playerSet2.text = value?.first ?: "-"
            }
            if (field?.second != value?.second) {
                this.binding.opponentSet2.text = value?.second ?: "-"
            }
            field = value
        }

    private var setThree: Pair<CharSequence, CharSequence>? = null
        set(value) {
            if (field?.first != value?.first) {
                this.binding.playerSet3.text = value?.first ?: "-"
            }
            if (field?.second != value?.second) {
                this.binding.opponentSet3.text = value?.second ?: "-"
            }
            field = value
        }

    private var setFour: Pair<CharSequence, CharSequence>? = null
        set(value) {
            if (field?.first != value?.first) {
                this.binding.playerSet4.text = value?.first ?: "-"
            }
            if (field?.second != value?.second) {
                this.binding.opponentSet4.text = value?.second ?: "-"
            }
            field = value
        }

    private var setFive: Pair<CharSequence, CharSequence>? = null
        set(value) {
            if (field?.first != value?.first) {
                this.binding.playerSet5.text = value?.first ?: "-"
            }
            if (field?.second != value?.second) {
                this.binding.opponentSet5.text = value?.second ?: "-"
            }
            field = value
        }

    fun setScore(
        setOne: Pair<CharSequence, CharSequence>? = null,
        setTwo: Pair<CharSequence, CharSequence>? = null,
        setThree: Pair<CharSequence, CharSequence>? = null,
        setFour: Pair<CharSequence, CharSequence>? = null,
        setFive: Pair<CharSequence, CharSequence>? = null
    ) {
        this.setOne = setOne
        this.setTwo = setTwo
        this.setThree = setThree
        this.setFour = setFour
        this.setFive = setFive
    }

    var setCount: Int = 0
        set(value) {
            if (field != value && value >= 1) {
                val coerceSets = value.coerceAtMost(5)
                when(coerceSets) {
                    1-> {
                        this.showSetTwo(false)
                        this.showSetThree(false)
                        this.showSetFour(false)
                        this.showSetFive(false)
                    }
                    2 -> {
                        this.showSetTwo(true)
                        this.showSetThree(false)
                        this.showSetFour(false)
                        this.showSetFive(false)
                    }
                    3 -> {
                        this.showSetTwo(true)
                        this.showSetThree(true)
                        this.showSetFour(false)
                        this.showSetFive(false)
                    }
                    4 -> {
                        this.showSetTwo(true)
                        this.showSetThree(true)
                        this.showSetFour(true)
                        this.showSetFive(false)
                    }
                    5 -> {
                        this.showSetTwo(true)
                        this.showSetThree(true)
                        this.showSetFour(true)
                        this.showSetFive(true)
                    }
                }
                field = coerceSets
            }
        }

    private fun showSetTwo(show: Boolean) {
        this.binding.playerSet2.visibleWhen(show)
        this.binding.opponentSet2.visibleWhen(show)
    }

    private fun showSetThree(show: Boolean) {
        this.binding.playerSet3.visibleWhen(show)
        this.binding.opponentSet3.visibleWhen(show)
    }

    private fun showSetFour(show: Boolean) {
        this.binding.playerSet4.visibleWhen(show)
        this.binding.opponentSet4.visibleWhen(show)
    }

    private fun showSetFive(show: Boolean) {
        this.binding.playerSet5.visibleWhen(show)
        this.binding.opponentSet5.visibleWhen(show)
    }
}