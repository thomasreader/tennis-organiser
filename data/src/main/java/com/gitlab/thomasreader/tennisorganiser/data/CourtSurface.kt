package com.gitlab.thomasreader.tennisorganiser.data

import com.squareup.sqldelight.ColumnAdapter

enum class CourtSurface {
    GRASS,
    ARTIFICIAL_GRASS,
    CLAY,
    ARTIFICIAL_CLAY,
    HARD,
    CARPET;
}

fun courtSurfaceAdapter(): ColumnAdapter<CourtSurface, Long> {
    return object: ColumnAdapter<CourtSurface, Long> {
        override fun decode(databaseValue: Long): CourtSurface {
            return CourtSurface.values()[databaseValue.toInt()]
        }

        override fun encode(value: CourtSurface): Long {
            return value.ordinal.toLong()
        }
    }
}