package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.stringhistory

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.format.DateUtils
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll

import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetStringHistoryBinding
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringBed
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.restring.RestringRacquetScreen
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RacquetStringHistoryFragment: KeyedFragment(R.layout.racquet_string_history) {
    private val binding by bindView(RacquetStringHistoryBinding::bind)
    private val stringHistoryAdapter: StringHistoryAdapter = StringHistoryAdapter {
        confirmDeleteAlert(it)
    }
    private lateinit var viewModel: RacquetStringHistoryViewModel
    private var dialog: Dialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.title = getString(R.string.string_history)
            restringFab.setOnClickListener {
                backstack.goTo(RestringRacquetScreen(viewModel.key.racquetId))
            }

            stringHistoryRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = stringHistoryAdapter
                animateFABOnScroll(restringFab)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dialog?.dismiss()
        dialog = null
    }

    private fun initViewModel() {
        viewModel = viewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    if (state != null) {
                        binding.toolbar.subtitle = state.subtitle.toString(resources)
                        stringHistoryAdapter.submitList(state.stringBeds)
                    }
                }
            }
        }
    }

    private fun confirmDeleteAlert(stringJob: StringBed) {
        val formattedStrungDate = DateUtils.formatDateTime(requireContext(), stringJob.strungAt, 0)

        dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_string_title)
            .setMessage(getString(R.string.delete_string_msg, formattedStrungDate))
            .setPositiveButton(android.R.string.ok) { _, _ ->
                viewModel.deleteStringJob(stringJob.id)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .setOnDismissListener {
                dialog = null
            }
            .show()
    }
}