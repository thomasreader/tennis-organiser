CREATE TABLE string_model (
    id INTEGER NOT NULL PRIMARY KEY,
    brand TEXT NOT NULL,
    model TEXT NOT NULL,
    composition TEXT,

    CONSTRAINT string_model_brand_FK FOREIGN KEY (brand) REFERENCES brand(name),
    CONSTRAINT string_model_AK UNIQUE (brand, model)
);

insertModel {
BEGIN TRANSACTION;

INSERT OR IGNORE INTO brand(name)
VALUES (:brand);

INSERT INTO string_model(brand, model, composition)
VALUES (:brand, :model, :composition);

COMMIT TRANSACTION;
}

updateModel {
BEGIN TRANSACTION;

INSERT OR IGNORE INTO brand(name)
VALUES (:brand);

UPDATE string_model
SET
    brand = :brand,
    model = :model,
    composition = :composition
WHERE id = :id;

COMMIT TRANSACTION;
}

deleteModel:
DELETE
FROM string_model
WHERE id = :id;

selectAllModels:
SELECT
    id,
    brand,
    model,
    composition
FROM string_model;

selectAllModelsAsc:
SELECT
    id,
    brand,
    model,
    composition
FROM string_model
ORDER BY brand ASC, model ASC;

selectModelById:
SELECT
    id,
    brand,
    model,
    composition
FROM string_model
WHERE id = :id;

selectCompositions:
SELECT DISTINCT composition
FROM string_model
WHERE composition IS NOT NULL;