package com.gitlab.thomasreader.tennisorganiser.data

import com.squareup.sqldelight.ColumnAdapter

enum class MatchType {
    SINGLES_MEN,
    SINGLES_WOMEN,
    SINGLES_OTHER,
    DOUBLES_MEN,
    DOUBLES_WOMEN,
    DOUBLES_MIXED,
    DOUBLES_OTHER;

    val isSingles get(): Boolean = when (this) {
            SINGLES_MEN -> true
            SINGLES_WOMEN -> true
            SINGLES_OTHER -> true
            DOUBLES_MEN -> false
            DOUBLES_WOMEN -> false
            DOUBLES_MIXED -> false
            DOUBLES_OTHER -> false
    }

}

fun matchTypeAdapter(): ColumnAdapter<MatchType, Long> {
    return object: ColumnAdapter<MatchType, Long> {
        override fun decode(databaseValue: Long): MatchType {
            return MatchType.values()[databaseValue.toInt()]
        }

        override fun encode(value: MatchType): Long {
            return value.ordinal.toLong()
        }
    }
}