package com.gitlab.thomasreader.tennisorganiser.domain.activity

import android.os.Parcelable
import com.gitlab.thomasreader.tennisorganiser.data.SelectSetResultsById
import com.gitlab.thomasreader.tennisorganiser.data.SetType
import kotlinx.parcelize.Parcelize

val SelectSetResultsById.toSet get(): Set? {
    return when (this.type) {
        SetType.TIEBREAK -> {
            if (this.team_tiebreak_points != null && this.opponent_tiebreak_points != null) {
                Set.TieBreak(
                    points = this.team_tiebreak_points!!,
                    opponentPoints = this.opponent_tiebreak_points!!
                )
            } else null
        }
        SetType.TIEBREAK_SET -> {
            if (this.team_games != null && this.opponent_games != null) {
                Set.TieBreakSet(
                    games = this.team_games!!,
                    opponentGames = this.opponent_games!!,
                    points = this.team_tiebreak_points,
                    opponentPoints = this.opponent_tiebreak_points
                )
            } else null
        }
        SetType.ADVANTAGE_SET -> {
            if (this.team_games != null && this.opponent_games != null) {
                Set.Advantage(
                    games = this.team_games!!,
                    opponentGames = this.opponent_games!!
                )
            } else null
        }
    }
}

data class MatchScore(
    val activityId: Long,
    val numSets: Int,
    val sets: List<Set>
)

val Set.scorePair get() = this.scoreString to this.opponentScoreString

sealed interface Set: Parcelable {
    val scoreString: String
    val opponentScoreString: String

    @Parcelize
    data class TieBreak(
        val points: Int,
        val opponentPoints: Int
    ): Set {
        override val scoreString: String
            get() = this.points.toString()
        override val opponentScoreString: String
            get() = this.opponentPoints.toString()
    }

    @Parcelize
    data class TieBreakSet(
        val games: Int,
        val opponentGames: Int,
        val points: Int?,
        val opponentPoints: Int?
    ): Set {
        private val hasTieBreak get() = points != null && opponentPoints != null
        override val scoreString: String
            get() {
                return if (hasTieBreak) {
                    when (points!! > opponentPoints!!) {
                        true -> games.toString()
                        false -> "$games($points)"
                    }
                } else {
                    games.toString()
                }
            }
        override val opponentScoreString: String
            get() {
                return if (hasTieBreak) {
                    when (points!! < opponentPoints!!) {
                        true -> opponentGames.toString()
                        false -> "$opponentGames($opponentPoints)"
                    }
                } else {
                    opponentGames.toString()
                }
            }
    }

    @Parcelize
    data class Advantage(
        val games: Int,
        val opponentGames: Int
    ): Set {
        override val scoreString: String
            get() = this.games.toString()
        override val opponentScoreString: String
            get() = this.opponentGames.toString()
    }
}