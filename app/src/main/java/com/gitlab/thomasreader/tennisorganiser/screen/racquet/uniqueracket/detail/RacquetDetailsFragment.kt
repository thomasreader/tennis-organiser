package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.detail

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R

import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetDetailsBinding
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.details.RacquetModelDetailsScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.edit.EditRacquetScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.stringhistory.RacquetStringHistoryScreen
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class RacquetDetailsFragment: KeyedFragment(R.layout.racquet_details) {
    private val binding by bindView(RacquetDetailsBinding::bind)
    private lateinit var viewModel: RacquetDetailsViewModel
    private val infoAdapter: RacquetDetailsAdapter = RacquetDetailsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.inflateMenu(R.menu.racquet_detail_menu)
            toolbar.inflateMenu(R.menu.edit_delete_menu)
            toolbar.title = getString(R.string.racquet_details)

            racquetInfoRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = infoAdapter
            }
        }
    }

    private fun initViewModel() {
        viewModel = viewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            RacquetDetailsEvent.Deleted -> backstack.goBack()
                        }
                    }
                }
                launch {
                    viewModel.uiState.first { state ->
                        if (state != null) {
                            binding.toolbar.subtitle = state.racquetIdBrandModel.toString(requireContext())
                            binding.toolbar.setOnMenuItemClickListener { menuItem ->
                                when (menuItem.itemId) {
                                    R.id.edit_action -> {
                                        backstack.goTo(EditRacquetScreen(state.racquetId))
                                    }
                                    R.id.delete_action -> {
                                        confirmDeleteAlert()
                                    }
                                    R.id.restring_racquet_action -> {
                                        backstack.goTo(RacquetStringHistoryScreen(state.racquetId))
                                    }
                                    R.id.racquet_detail_action -> {
                                        backstack.goTo(RacquetModelDetailsScreen(state.modelId))
                                    }
                                    else -> false
                                }
                                true
                            }
                            return@first true
                        } else {
                            return@first false
                        }
                    }
                }
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            infoAdapter.submitList(state.details)
                        }
                    }
                }
            }
        }
    }

    private fun confirmDeleteAlert() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_racquet)
            .setMessage(R.string.delete_racquet_message)
            .setPositiveButton(R.string.delete) { _, _ ->
                viewModel.deleteRacquet()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}