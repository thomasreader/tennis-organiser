package com.gitlab.thomasreader.tennisorganiser.core.data

import android.content.Context
import android.text.format.DateUtils
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

interface ContextStringProvider {
    fun toString(context: Context): String

    companion object {
        val VERSION_PROVIDER: ContextStringProvider get() = VersionProvider
    }
}

private object VersionProvider : ContextStringProvider {
    override fun toString(context: Context): String {
        return context.packageManager.getPackageInfo(context.packageName, 0).versionName
    }
}

data class TimeProvider(
    val hour: Int,
    val minutes: Int
) {
    override fun toString(): String {
        val date = Calendar.getInstance().apply {
            set(0, 0, 0, this@TimeProvider.hour, this@TimeProvider.minutes)
        }.time
        return SimpleDateFormat.getTimeInstance(DateFormat.SHORT).format(date)
    }
}

data class DateTimeProvider(
    val time: Long,
    val flags: Int = 0
) : ContextStringProvider {
    override fun toString(context: Context): String {
        return DateUtils.formatDateTime(
            context,
            time,
            flags
        )
    }
}

data class DateTimeRangeProvider(
    val start: Long,
    val end: Long
) : ContextStringProvider {
    override fun toString(context: Context): String {
        return DateUtils.formatDateRange(
            context,
            start,
            end,
            0
        )
    }
}

data class RelativeDateTimeStringProvider(
    val date: Long
) : ContextStringProvider {
    override fun toString(context: Context): String {
        return DateUtils.getRelativeDateTimeString(
            context,
            date,
            DateUtils.DAY_IN_MILLIS,
            DateUtils.WEEK_IN_MILLIS,
            0
        ).toString()
    }
}

data class ElapsedTimeProvider(
    val seconds: Long
) {
    override fun toString(): String {
        return DateUtils.formatElapsedTime(this.seconds)
    }
}