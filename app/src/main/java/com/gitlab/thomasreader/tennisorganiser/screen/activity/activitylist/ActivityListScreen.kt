package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class ActivityListScreen: AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = ActivityListFragment()
}