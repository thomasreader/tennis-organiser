package com.gitlab.thomasreader.tennisorganiser.domain.string

import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.SelectLatestStringJobByRacquetId


fun SelectLatestStringJobByRacquetId.toDomain(
    tensionUnit: DroidUnit<Mass>
): StringBedBasic {
    return StringBedBasic(
        databaseModel = this,
        mainTensionString = this.mains_tension.toSymbolString(
            tensionUnit,
            Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)
        ),
        crossTensionString = this.cross_tension.toSymbolString(
            tensionUnit,
            Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)
        ),
        mainThicknessString = this.main_thickness.toSymbolString(
            DroidUnits.LengthUnits.MILLIMETRE,
            Constants.DisplayOptions.Format.stringThicknessFormatter()
        ),
        crossThicknessString = this.cross_thickness.toSymbolString(
            DroidUnits.LengthUnits.MILLIMETRE,
            Constants.DisplayOptions.Format.stringThicknessFormatter()
        )
    )
}

class StringBedBasic(
    private val databaseModel: SelectLatestStringJobByRacquetId,
    val mainTensionString: String,
    val crossTensionString: String,
    val mainThicknessString: String,
    val crossThicknessString: String,
) {
    val id: Long get() = databaseModel.id
    val strungAt: Long get() = databaseModel.strung_at!!
    val mainsTension: Quantity<Mass> get() = databaseModel.mains_tension
    val crossTension: Quantity<Mass> get() = databaseModel.cross_tension
    val mainBrand: String get() = databaseModel.main_brand
    val mainModel: String get() = databaseModel.main_model
    val mainThickness: Quantity<Length> get() = databaseModel.main_thickness
    val crossBrand: String get() = databaseModel.cross_brand
    val crossModel: String get() = databaseModel.cross_model
    val crossThickness: Quantity<Length> get() = databaseModel.cross_thickness

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StringBedBasic

        if (databaseModel != other.databaseModel) return false

        return true
    }

    override fun hashCode(): Int {
        return databaseModel.hashCode()
    }
}