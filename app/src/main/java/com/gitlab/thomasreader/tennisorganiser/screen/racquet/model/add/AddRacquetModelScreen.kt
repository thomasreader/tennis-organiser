package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.add

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class AddRacquetModelScreen: AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = AddRacquetModelFragment()
    override fun hideBottomNavigation(): Boolean = true
}