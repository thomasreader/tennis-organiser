package com.gitlab.thomasreader.tennisorganiser.core.dependency

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.sqlite.db.SupportSQLiteDatabase
import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.QuantityType
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.application.App
import com.gitlab.thomasreader.tennisorganiser.application.MainActivity
import com.gitlab.thomasreader.tennisorganiser.application.MainActivityViewModel
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AbstractSavedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AbstractViewModel
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.data.*
import com.squareup.sqldelight.ColumnAdapter
import com.squareup.sqldelight.EnumColumnAdapter
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import com.zhuinden.simplestackextensions.fragments.KeyedFragment

class Dependencies(
    app: App
) {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
    private val dataStore: DataStore<Preferences> = app.applicationContext.dataStore

    private val databaseName get() = "tennis.db"
    private val androidSqliteDriver: AndroidSqliteDriver by lazy {
        AndroidSqliteDriver(
            schema = TennisDatabase.Schema,
            context = app.applicationContext,
            name = databaseName,
            callback = object : AndroidSqliteDriver.Callback(TennisDatabase.Schema) {
                override fun onConfigure(db: SupportSQLiteDatabase) {
                    super.onConfigure(db)
                    db.setForeignKeyConstraintsEnabled(true)
                }
            }
        )
    }
    private val activityAdapter: Activity.Adapter get() {
        return Activity.Adapter(
            courtSurfaceAdapter(), matchTypeAdapter(), matchResultAdapter()
        )
    }
    private val matchRulesAdapter: Match_set_rule.Adapter get() {
        return Match_set_rule.Adapter(EnumColumnAdapter())
    }
    private val quantityAdapter: ColumnAdapter<Quantity<Nothing>, Double> = object : ColumnAdapter<Quantity<Nothing>, Double> {
        override fun decode(databaseValue: Double): Quantity<Nothing> {
            return Quantity(databaseValue)
        }

        override fun encode(value: Quantity<Nothing>): Double {
            return value.value
        }
    }
    @Suppress("UNCHECKED_CAST")
    private inline fun <reified T: QuantityType> qAdapter() = quantityAdapter as ColumnAdapter<Quantity<T>, Double>

    private val database by lazy {
        TennisDatabase(
            driver = androidSqliteDriver,
            activityAdapter = activityAdapter,
            racquetAdapter = Racquet.Adapter(
                weightAdapter = qAdapter(),
                balance_pointAdapter = qAdapter()
            ), racquet_modelAdapter = Racquet_model.Adapter(
                head_sizeAdapter = qAdapter(),
                lengthAdapter = qAdapter(),
                weightAdapter = qAdapter(),
                balance_pointAdapter = qAdapter(),
                width_beamAdapter = qAdapter(),
                width_tipAdapter = qAdapter(),
                width_shaftAdapter = qAdapter(),
                tension_lowAdapter = qAdapter(),
                tension_highAdapter = qAdapter()
            ), string_bedAdapter = String_bed.Adapter(
                mains_tensionAdapter = qAdapter(),
                cross_tensionAdapter = qAdapter()
            ), string_variantAdapter = String_variant.Adapter(
                thicknessAdapter = qAdapter()
            ), match_set_ruleAdapter = matchRulesAdapter
        )
    }

    @Suppress("UNUSED")
    fun dataStore(app: App): DataStore<Preferences> = dataStore

    fun mainActivityViewModel(mainActivity: MainActivity): MainActivityViewModel {
        return ViewModelProvider(mainActivity, MainActivityViewModelFactory(dataStore))
            .get(MainActivityViewModel::class.java)
    }

    fun viewModelFactory(fragment: KeyedFragment): ViewModelProvider.Factory =
        KeyedDbDataViewModelFactory(fragment, this.database, this.dataStore)

    fun savedViewModelFactory(fragment: KeyedFragment): AbstractSavedStateViewModelFactory =
        KeyedSavedViewModelFactory(fragment, this.database, this.dataStore)

    inline fun <reified T: KeyedViewModel<K>, K: DefaultFragmentKey> provideViewModel(
        keyedFragment: KeyedFragment
    ): T {
        return ViewModelProvider(
            keyedFragment,
            when (T::class.java) {
                AbstractViewModel::class.java -> viewModelFactory(keyedFragment)
                AbstractSavedViewModel::class.java -> savedViewModelFactory(keyedFragment)
                else -> TODO()
            },
        ).get(T::class.java)
    }
}

private class MainActivityViewModelFactory(
    val dataStore: DataStore<Preferences>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass
            .getConstructor(DataStore::class.java)
            .newInstance(dataStore)
    }
}

private class KeyedDbDataViewModelFactory(
    val fragment: KeyedFragment,
    val database: TennisDatabase,
    val dataStore: DataStore<Preferences>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(
            DefaultFragmentKey::class.java,
            TennisDatabase::class.java,
            DataStore::class.java
        ).newInstance(fragment.getKey(), database, dataStore)
    }
}

private class KeyedSavedViewModelFactory(
    val fragment: KeyedFragment,
    val database: TennisDatabase,
    val dataStore: DataStore<Preferences>
): AbstractSavedStateViewModelFactory(fragment, null) {
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return modelClass.getConstructor(
            DefaultFragmentKey::class.java,
            TennisDatabase::class.java,
            DataStore::class.java,
            SavedStateHandle::class.java
        ).newInstance(fragment.getKey(), database, dataStore, handle)
    }
}