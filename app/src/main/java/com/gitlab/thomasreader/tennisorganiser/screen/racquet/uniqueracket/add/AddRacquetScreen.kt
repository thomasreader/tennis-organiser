package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class AddRacquetScreen(
    val modelId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = AddRacquetFragment()
    override fun hideBottomNavigation(): Boolean = true
}