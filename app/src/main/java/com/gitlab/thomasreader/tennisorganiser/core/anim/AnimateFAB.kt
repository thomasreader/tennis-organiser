package com.gitlab.thomasreader.tennisorganiser.core.anim

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.view.View
import androidx.core.animation.doOnEnd
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

fun RecyclerView.animateFABOnScroll(fab: FloatingActionButton) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        var fabAnimation: ValueAnimator? = null

        private fun createNewAnimator(isAppearing: Boolean) {
            val endScale = if (isAppearing) 1f else 0f
            fabAnimation?.cancel()
            fabAnimation = ObjectAnimator.ofPropertyValuesHolder(
                fab,
                PropertyValuesHolder.ofFloat(View.SCALE_X, endScale),
                PropertyValuesHolder.ofFloat(View.SCALE_Y, endScale)
            ).apply {
                doOnEnd {
                    fabAnimation = null
                }
                duration = 200L
                start()
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            when (newState) {
                RecyclerView.SCROLL_STATE_IDLE -> {
                    if (fab.scaleX != 1f) {
                        fabAnimation?.cancel()
                        createNewAnimator(true)
//                        fabAnimation = ObjectAnimator.ofPropertyValuesHolder(
//                            fab,
//                            PropertyValuesHolder.ofFloat(View.SCALE_X, 1f),
//                            PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f)
//                        ).apply {
//                            doOnEnd {
//                                fabAnimation = null
//                            }
//                            duration = 200L
//                            start()
//                        }
                    }
                }
                RecyclerView.SCROLL_STATE_DRAGGING -> {
                    if (fab.scaleX != 0f) {
                        fabAnimation?.cancel()
                        createNewAnimator(false)
//                        fabAnimation = ObjectAnimator.ofPropertyValuesHolder(
//                            fab,
//                            PropertyValuesHolder.ofFloat(View.SCALE_X, 0f),
//                            PropertyValuesHolder.ofFloat(View.SCALE_Y, 0f)
//                        ).apply {
//                            doOnEnd {
//                                fabAnimation = null
//                            }
//                            duration = 200L
//                            start()
//                        }
                    }
                }
            }
        }
    })
}