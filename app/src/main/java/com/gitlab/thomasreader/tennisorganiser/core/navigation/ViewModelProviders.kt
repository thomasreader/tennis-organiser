package com.gitlab.thomasreader.tennisorganiser.core.navigation

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.gitlab.thomasreader.tennisorganiser.core.extension.mainActivity
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import com.zhuinden.simplestackextensions.fragments.KeyedFragment

inline fun <reified T: KeyedViewModel<K>, K: DefaultFragmentKey> KeyedFragment.viewModel(): T {
    return ViewModelProvider(
        this,
        mainActivity.app.dependencies.viewModelFactory(
            this
        )
    ).get(T::class.java)
}

inline fun <reified T: KeyedViewModel<K>, K: DefaultFragmentKey> KeyedFragment.savedViewModel(): T {
    return ViewModelProvider(
        this,
        mainActivity.app.dependencies.savedViewModelFactory(
            this
        )
    ).get(T::class.java)
}

val Fragment.dependencies get() = this.mainActivity.app.dependencies

inline fun <reified T: AbstractViewModel<K>, K: DefaultFragmentKey> KeyedFragment.vm(): T {
    val factory = when (T::class.java) {
        AbstractSavedViewModel::class.java -> dependencies.savedViewModelFactory(this)
        AbstractViewModel::class.java -> dependencies.viewModelFactory(this)
        else -> TODO()
    }
    return ViewModelProvider(this, factory).get(T::class.java)
}

inline fun <reified T: KeyedViewModel<K>, K: DefaultFragmentKey> KeyedFragment.viewModel2(): T {
    return ViewModelProvider(
        this,
        when (T::class.java) {
            AbstractViewModel::class.java -> dependencies.viewModelFactory(this)
            AbstractSavedViewModel::class.java -> dependencies.savedViewModelFactory(this)
            else -> TODO()
        },
    ).get(T::class.java)
}