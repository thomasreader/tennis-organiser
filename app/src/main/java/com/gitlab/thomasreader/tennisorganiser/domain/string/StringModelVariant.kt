package com.gitlab.thomasreader.tennisorganiser.domain.string

import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.data.SelectAllModelVariant
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits

fun List<SelectAllModelVariant>.toDomain(): List<StringModelVariant> {
    val thicknessFormat = Constants.DisplayOptions.Format.stringThicknessFormatter()
    return this.map {
        StringModelVariant(
            databaseModel = it,
            thicknessString = it.thickness.toSymbolString(DroidUnits.LengthUnits.MILLIMETRE, thicknessFormat)
        )
    }
}

data class StringModelVariant(
    private val databaseModel: SelectAllModelVariant,
    val thicknessString: String
) {
    val description: DroidTextProvider = DroidTextProvider.stringId(
        R.string.string_variant_brand_model_thickness,
        this.brand,
        this.model,
        this.thicknessString
    )
    val variant_id: Long get() = this.databaseModel.variant_id
    val brand: String get() = this.databaseModel.brand
    val model: String get() = this.databaseModel.model
    val composition: String? get() = this.databaseModel.composition
}