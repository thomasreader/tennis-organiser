package com.gitlab.thomasreader.tennisorganiser.core.extension

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.application.MainActivity

val Fragment.mainActivity: MainActivity get() = requireActivity() as MainActivity