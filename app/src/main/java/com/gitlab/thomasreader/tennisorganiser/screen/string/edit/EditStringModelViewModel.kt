package com.gitlab.thomasreader.tennisorganiser.screen.string.edit

import android.database.sqlite.SQLiteException
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModel
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

sealed interface EditStringModelEvent {
    object Saved: EditStringModelEvent
    object UniqueConstraintFailed: EditStringModelEvent {
        val msg = DroidTextProvider.stringId(R.string.duplicate_string_model_err)
    }
}

data class EditStringModelState(
    val stringModel: StringModel,
    val brands: List<String>,
    val compositions: List<String>,
    val saveEnabled: Boolean,
    val brandError: DroidTextProvider? = null,
    val modelError: DroidTextProvider? = null
)

class EditStringModelViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<EditStringModelScreen>(_key) {
    private val _events = Channel<EditStringModelEvent>(Channel.BUFFERED)
    val events: Flow<EditStringModelEvent> = _events.receiveAsFlow()

    var isInitiated: Boolean = savedStateHandle["initiated"] ?: false
        set(value) {
            savedStateHandle["initiated"] = true
            field = value
        }

    private val brandError: MutableStateFlow<DroidTextProvider?> = MutableStateFlow(null)
    private val modelError: MutableStateFlow<DroidTextProvider?> = MutableStateFlow(null)
    private val saveDisabledOverride: MutableStateFlow<Boolean> = MutableStateFlow(false)

    @Suppress("UNCHECKED_CAST")
    val uiState = combine(
        database.stringModelQueries.selectModelById(key.stringModelId, ::StringModel)
            .asFlow()
            .mapToOne(Dispatchers.IO),

        database.miscQueries.selectAllBrands()
            .asFlow()
            .mapToList(Dispatchers.IO),

        database.stringModelQueries.selectCompositions()
            .asFlow()
            .mapToList(Dispatchers.IO),

        brandError,
        modelError,
        saveDisabledOverride
    ) { args->
        val stringModel = args[0] as StringModel
        val brands = args[1] as List<String>
        val compositions = args[2] as List<String>
        val brandErr = args[3] as DroidTextProvider?
        val modelErr = args[4] as DroidTextProvider?
        val saveDisabledOver = args[5] as Boolean

        val saveDisabled = saveDisabledOver || brandErr != null || modelErr != null

        EditStringModelState(
            stringModel = stringModel,
            brands = brands,
            compositions = compositions,
            saveEnabled = !saveDisabled,
            brandError = brandErr,
            modelError = modelErr
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun validateBrand(brand: String?) {
        brandError.value = if (brand.isNullOrBlank()) DroidTextProvider.REQUIRED else null
    }

    fun validateModel(model: String?) {
        modelError.value = if (model.isNullOrBlank()) DroidTextProvider.REQUIRED else null
    }

    fun updateStringModel(
        brand: String?,
        model: String?,
        composition: String?
    ) {
        val newBrand = brand?.ifBlank { null }
        val newModel = model?.ifBlank { null }
        val newComposition = composition?.ifBlank { null }

        if (newBrand != null && newModel != null && !saveDisabledOverride.value) {
            saveDisabledOverride.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                try {
                    database.stringModelQueries.updateModel(
                        brand = newBrand,
                        model = newModel,
                        composition = newComposition,
                        id = key.stringModelId
                    )
                    _events.send(EditStringModelEvent.Saved)
                } catch (sqlException: SQLiteException) {
                    // presume unique constraint failed
                    _events.send(EditStringModelEvent.UniqueConstraintFailed)
                    saveDisabledOverride.value = false
                }
            }
        }
    }
}