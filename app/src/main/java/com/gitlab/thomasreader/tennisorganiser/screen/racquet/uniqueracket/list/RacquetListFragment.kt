package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll

import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetListBinding
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.list.AddRacquetModelListScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.detail.RacquetDetailsScreen
import com.gitlab.thomasreader.tennisorganiser.screen.settings.SettingsScreen
import com.gitlab.thomasreader.tennisorganiser.util.TennisBallLongClickListener
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RacquetListFragment : KeyedFragment(R.layout.racquet_list) {
    private var _binding: RacquetListBinding? = null
    private val binding get() = _binding!!
    private val racquetListAdapter: RacquetListAdapter = RacquetListAdapter {
        backstack.goTo(RacquetDetailsScreen(it.id))
    }
    private lateinit var viewModel: RacquetListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = RacquetListBinding.bind(view)
        initViewModel()

        with(binding) {
            toolbar.inflateMenu(R.menu.settings_menu)
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.settingsMenu -> {
                        backstack.goTo(SettingsScreen())
                        true
                    }
                    else -> false
                }
            }

            addRacquetFab.setOnClickListener {
                backstack.goTo(AddRacquetModelListScreen())
            }
            addRacquetFab.setOnLongClickListener(TennisBallLongClickListener())

            racquetListRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = racquetListAdapter
                animateFABOnScroll(addRacquetFab)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViewModel() {
        viewModel = viewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.racquetListState.collect { state ->
                    if (state != null) {
                        racquetListAdapter.submitList(state)
                    }
                }
            }
        }
    }
}