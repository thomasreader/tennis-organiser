package com.gitlab.thomasreader.tennisorganiser.screen.string.stringlist

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModel
import com.gitlab.thomasreader.tennisorganiser.databinding.StringRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.core.extension.visibleWhen

class StringListAdapter(
    val onClickListener: (StringModel) -> Unit
): ListAdapter<StringModel, StringListAdapter.ViewHolder>(StringDiffCallBack()) {
    init {
        this.stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, StringRecyclerRowBinding::inflate))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(this.currentList[position])
    }

    inner class ViewHolder(
        binding: StringRecyclerRowBinding
    ): BindingViewHolder<StringRecyclerRowBinding>(binding), View.OnClickListener {
        init {
            this.itemView.setOnClickListener(this)
        }

        fun bind(string: StringModel) {
            val resources = itemView.context.resources
            this.binding.stringBrandModel.text = string.brandModelRes.toString(resources)
            this.binding.stringComposition.visibleWhen(string.composition != null)
            this.binding.stringComposition.text = string.composition
        }

        override fun onClick(v: View) {
            onClickListener(currentList[bindingAdapterPosition])
        }
    }

    private class StringDiffCallBack : DiffUtil.ItemCallback<StringModel>() {
        override fun areItemsTheSame(oldItem: StringModel, newItem: StringModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StringModel, newItem: StringModel): Boolean {
            return oldItem == newItem
        }
    }
}