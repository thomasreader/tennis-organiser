package com.gitlab.thomasreader.tennisorganiser.screen.string.stringdetails

import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class StringDetailsScreen(
    val stringModelId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment() = StringDetailsFragment()
}

