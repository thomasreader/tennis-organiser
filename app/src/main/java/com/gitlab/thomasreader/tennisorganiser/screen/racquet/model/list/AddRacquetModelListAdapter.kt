package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class AddRacquetModelListAdapter(
    val onClickListener: (BasicRacquetModel) -> Unit,
    val onLongClickListener: (BasicRacquetModel) -> Unit
): ListAdapter<BasicRacquetModel, AddRacquetModelListAdapter.ViewHolder>(RacquetModelDiffCallback()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(
        itemView: View
    ): RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {
        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun bind(model: BasicRacquetModel) {
            (itemView as TextView).text = model.brandModelRes.toString(itemView.resources)
        }

        override fun onClick(v: View) {
            onClickListener(currentList[bindingAdapterPosition])
        }

        override fun onLongClick(v: View): Boolean {
            onLongClickListener(currentList[bindingAdapterPosition])
            return true
        }
    }

    private class RacquetModelDiffCallback: DiffUtil.ItemCallback<BasicRacquetModel>() {
        override fun areItemsTheSame(
            oldItem: BasicRacquetModel,
            newItem: BasicRacquetModel
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: BasicRacquetModel,
            newItem: BasicRacquetModel
        ): Boolean {
            return oldItem == newItem
        }
    }
}