package com.gitlab.thomasreader.tennisorganiser.core.widget

import android.content.Context
import android.util.AttributeSet
import com.gitlab.thomasreader.tennisorganiser.core.extension.addUpNavigationIfNeeded
import com.google.android.material.appbar.MaterialToolbar

class SimpleStackMaterialToolbar : MaterialToolbar {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onFinishInflate() {
        super.onFinishInflate()
        this.addUpNavigationIfNeeded()
    }
}