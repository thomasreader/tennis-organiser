package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitydetails

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class ActivityMatchDetailsScreen(
    val activityId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = ActivityDetailsFragment()
}