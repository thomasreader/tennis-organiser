package com.gitlab.thomasreader.tennisorganiser.screen.settings

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.tennisorganiser.R

import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.databinding.SettingsBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.SimpleEditTextBinding
import com.gitlab.thomasreader.tennisorganiser.screen.settings.about.AboutScreen
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SettingsFragment: KeyedFragment(R.layout.settings) {
    private val binding: SettingsBinding by bindView(SettingsBinding::bind)
    private lateinit var viewModel: SettingsViewModel
    private var activeDialog: Dialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initSettingsClickListeners()

        with (binding) {
            about.setOnClickListener {
                backstack.goTo(AboutScreen())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activeDialog?.dismiss()
        activeDialog = null
    }

    private fun initViewModel() {
        viewModel = savedViewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    if (state != null) {
                        fun TextView.updateDroidRes(droidTextProvider: DroidTextProvider?) {
                            val tag = R.id.text_view_droid_res
                            val currentRes = this.getTag(tag)
                            if (currentRes != droidTextProvider) {
                                this.setTag(tag, droidTextProvider)
                                this.text = droidTextProvider?.toString(resources)
                            }
                        }
                        binding.darkModeDesc.updateDroidRes(state.settings[SettingOptions.DARK])
                        binding.userNameDesc.updateDroidRes(state.settings[SettingOptions.USERNAME])
                        binding.racquetLengthDesc.updateDroidRes(state.settings[SettingOptions.RACQUET_LENGTH])
                        binding.racquetWeightDesc.updateDroidRes(state.settings[SettingOptions.RACQUET_WEIGHT])
                        binding.racquetHeadSizeDesc.updateDroidRes(state.settings[SettingOptions.RACQUET_HEAD_SIZE])
                        binding.racquetBalancePointDesc.updateDroidRes(state.settings[SettingOptions.RACQUET_BALANCE_POINT])
                        binding.gripSizeDesc.updateDroidRes(state.settings[SettingOptions.RACQUET_GRIP_SIZE])
                        binding.stringTensionDesc.updateDroidRes(state.settings[SettingOptions.STRING_TENSION])

                        when (state.openDialog) {
                            is OpenSettingsDialog.InputDialog -> {
                                val editTextView = SimpleEditTextBinding.inflate(layoutInflater, null, false)
                                editTextView.editTextInput.doOnTextChanged { text, _, _, _ ->
                                    viewModel.userName = text?.toString()
                                }
                                editTextView.editTextInput.setText(
                                    viewModel.userName ?: state.openDialog.text?.toString(resources)
                                )

                                activeDialog?.dismiss()
                                activeDialog = AlertDialog.Builder(requireContext())
                                    .setTitle(state.openDialog.title)
                                    .setView(editTextView.root)
                                    .setOnCancelListener { viewModel.setDialog(null) }
                                    .setPositiveButton(android.R.string.ok) { _, _ ->
                                        state.openDialog.onSubmit(viewModel, editTextView.editTextInput.text?.toString())
                                        viewModel.setDialog(null)
                                    }
                                    .setNegativeButton(android.R.string.cancel) { _, _ ->
                                        viewModel.setDialog(null)
                                    }
                                    .show()
                            }
                            is OpenSettingsDialog.SelectSingleDialog -> {
                                activeDialog?.dismiss()
                                val arrayAdapter = ArrayAdapter<String>(
                                    requireContext(),
                                    android.R.layout.simple_list_item_single_choice,
                                    state.openDialog.options.map { it.toString(resources) }
                                )
                                activeDialog = AlertDialog.Builder(requireContext())
                                    .setTitle(state.openDialog.title)
                                    .setSingleChoiceItems(arrayAdapter, state.openDialog.selectedIndex) { dialog, which ->
                                        state.openDialog.onSubmit(viewModel, which)
                                        viewModel.setDialog(null)
                                    }
                                    .setOnCancelListener {
                                        viewModel.setDialog(null)
                                    }
                                    .show()
                            }
                            null -> {
                                activeDialog?.dismiss()
                                activeDialog = null
                                viewModel.setDialog(null)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun initSettingsClickListeners() {
        with (binding) {
            fun View.OnClickListener.addTo(title: View, desc: View) {
                title.setOnClickListener(this)
                desc.setOnClickListener(this)
            }

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.DARK)
            }.addTo(darkModeTitle, darkModeDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.USERNAME)
            }.addTo(userNameTitle, userNameDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.RACQUET_LENGTH)
            }.addTo(racquetLengthTitle, racquetLengthDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.RACQUET_WEIGHT)
            }.addTo(racquetWeightTitle, racquetWeightDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.RACQUET_HEAD_SIZE)
            }.addTo(racquetHeadSizeTitle, racquetHeadSizeDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.RACQUET_BALANCE_POINT)
            }.addTo(racquetBalancePointTitle, racquetBalancePointDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.RACQUET_GRIP_SIZE)
            }.addTo(gripSizeTitle, gripSizeDesc)

            View.OnClickListener {
                viewModel.setDialog(SettingOptions.STRING_TENSION)
            }.addTo(stringTensionTitle, stringTensionDesc)
        }
    }
}