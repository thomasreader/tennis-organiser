package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DataStoreKeys
import com.gitlab.thomasreader.tennisorganiser.core.data.getMapped
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.RacquetList
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.toRacquetList
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import logcat.logcat

class RacquetListViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
) : KeyedViewModel<RacquetListScreen>(_key) {
    val racquetListState: StateFlow<List<RacquetList>?> = combine(
        database.racquetQueries.selectRacquetListInfo().asFlow().mapToList(Dispatchers.IO),
        dataStore.data
    ) { racquets, preferences ->
        val racquetWeightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
        val racquetHeadSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_HEAD_SIZE)
        val stringTensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)

        racquets.toRacquetList(racquetWeightUnit, racquetHeadSizeUnit, stringTensionUnit)
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

}