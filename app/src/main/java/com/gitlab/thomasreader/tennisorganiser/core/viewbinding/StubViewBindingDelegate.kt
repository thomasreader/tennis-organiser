package com.gitlab.thomasreader.tennisorganiser.core.viewbinding

import android.view.View
import android.view.ViewStub
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class StubViewBindingDelegate<T: ViewBinding>(
    private val lazyStub: Lazy<ViewStub>,
    private val binder: (View) -> T,
    private val onBound: (T) -> Unit
): ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        val binding = this.binding
        if (binding != null) {
            return binding
        }

        val lifeCycle = thisRef.viewLifecycleOwner.lifecycle
        if (!lifeCycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            throw IllegalStateException("Attempted to retrieve binding when View is destroyed")
        } else {
            lifeCycle.addObserver(this)
            return this.binder(lazyStub.value.inflate()).also {
                this.binding = it
                onBound(it)
            }
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        owner.lifecycle.removeObserver(this)
        this.binding = null
    }
}