package com.gitlab.thomasreader.tennisorganiser.domain

data class SoftwareLibrary(
    val name: String,
    val author: String,
    val url: String,
    val license: SoftwareLicense
)

enum class SoftwareLicense(
    val description: String,
    val url: String
) {
    MIT("MIT License", "https://mit-license.org"),
    APACHE_2_0("Apache License Version 2.0", "https://www.apache.org/licenses/LICENSE-2.0.html"),
    GPL_2_0("GNU General Public License v2.0", "https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"),
    GPL_3_0("GNU General Public License v3.0", "https://www.gnu.org/licenses/gpl-3.0.en.html");
}