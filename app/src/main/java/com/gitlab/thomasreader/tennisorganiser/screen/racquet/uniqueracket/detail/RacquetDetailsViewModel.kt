package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.detail

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.nullStringRes
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.toDomain
import com.gitlab.thomasreader.tennisorganiser.domain.string.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.squareup.sqldelight.runtime.coroutines.mapToOneOrNull
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat
import java.time.Duration

data class RacquetDetailRow(
    val header: DroidTextProvider,
    val info: ContextStringProvider?
)

data class RacquetDetailsState(
    val racquetIdBrandModel: DroidTextProvider,
    val details: List<RacquetDetailRow>,
    val racquetId: Long,
    val modelId: Long
)

sealed interface RacquetDetailsEvent {
    object Deleted : RacquetDetailsEvent
}

class RacquetDetailsViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
) : KeyedViewModel<RacquetDetailsScreen>(_key) {
    private val _events = Channel<RacquetDetailsEvent>(Channel.BUFFERED)
    val events = _events.receiveAsFlow()

    val uiState: StateFlow<RacquetDetailsState?> = combine(
        database.racquetQueries.selectModelFromId(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        database.racquetQueries.selectRacquetById(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        database.stringBedQueries.selectLatestStringJobByRacquetId(key.racquetId)
            .asFlow()
            .mapToOneOrNull(Dispatchers.IO),
        dataStore.data
    ) { model, racquet, stringBed, preferences ->
        val racquetLengthUnit = preferences.getMapped(DataStoreKeys.RACQUET_LENGTH)
        val racquetWeightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
        val racquetHeadSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_HEAD_SIZE)
        val racquetBalancePointUnit = preferences.getMapped(DataStoreKeys.RACQUET_BALANCE_POINT)
        val stringTensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)

        val domainModel = model.toDomain(
            lengthUnit = racquetLengthUnit,
            weightUnit = racquetWeightUnit,
            headSizeUnit = racquetHeadSizeUnit,
            balanceUnit = racquetBalancePointUnit,
            tensionUnit = stringTensionUnit
        )
        val domainRacquet = racquet.toDomain(
            weightUnit = racquetWeightUnit,
            balanceUnit = racquetBalancePointUnit
        )
        val domainStringBed = stringBed?.toDomain(stringTensionUnit)

        val stringsHeader: DroidTextProvider
        val stringsInfo: DroidTextProvider
        when (domainStringBed != null) {
            true -> {
                val stringAgeDays = Duration
                    .ofMillis(System.currentTimeMillis() - domainStringBed.strungAt)
                    .toDays()
                    .toInt()
                stringsHeader = DroidTextProvider.stringId(
                    R.string.strings_mains_crosses_dated,
                    DroidTextProvider.pluralId(R.plurals.strung_day_format, stringAgeDays, stringAgeDays)
                )
                with (domainStringBed) {
                    stringsInfo = when (mainBrand == crossBrand && mainModel == crossModel && mainThicknessString == crossThicknessString && mainTensionString == crossTensionString) {
                        true -> DroidTextProvider.stringId(
                            R.string.strung_racquet_format,
                            "$mainBrand $mainModel $mainThicknessString",
                            mainTensionString
                        )
                        false -> DroidTextProvider.stringId(
                            R.string.hybrid_strung_racquet_format,
                            "$mainBrand $mainModel $mainThicknessString",
                            mainTensionString,
                            "$crossBrand $crossModel $crossThicknessString",
                            crossTensionString
                        )
                    }
                }
            }
            false -> {
                stringsHeader = DroidTextProvider.stringId(R.string.strings_mains_crosses)
                stringsInfo = DroidTextProvider.stringId(R.string.unstrung_racquet)
            }
        }

        val info = listOf(
            RacquetDetailRow(stringsHeader, stringsInfo),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.weight), domainRacquet.weight?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.balance_point), domainRacquet.balancePoint?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.swing_weight), domainRacquet.swingWeight?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.notes), domainRacquet.notes?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.head_size), domainModel.headSizeString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.length), domainModel.lengthString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.stiffness), domainModel.stiffness?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.beam_width), domainModel.beamWidthRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_pattern), domainModel.stringPatternRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_tension), domainModel.tensionRangeRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.mains_skip), domainModel.skipRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_pieces), domainModel.stringPieces?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.shared_holes), domainModel.sharedHoles?.toString()?.toDroidTextResource())
        )

        RacquetDetailsState(
            racquetIdBrandModel = DroidTextProvider.stringId(
                R.string.racquet_identifier_brand_model,
                domainRacquet.identifier,
                domainModel.brand,
                domainModel.model
            ),
            details = info,
            racquetId = key.racquetId,
            modelId = domainModel.id,
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .flowOn(Dispatchers.Default)
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun deleteRacquet() {
        viewModelScope.launch(Dispatchers.IO) {
            database.racquetQueries.deleteRacquet(key.racquetId)
            _events.send(RacquetDetailsEvent.Deleted)
        }
    }
}