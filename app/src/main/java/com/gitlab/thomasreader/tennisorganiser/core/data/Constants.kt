package com.gitlab.thomasreader.tennisorganiser.core.data

import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.domain.SoftwareLibrary
import com.gitlab.thomasreader.tennisorganiser.domain.SoftwareLicense
import java.text.DecimalFormat

interface Constants {
    companion object {
        @JvmField
        val DROID_OPEN_SOURCE_PROJECT = "The Android Open Source Project"

        @JvmField
        val LIBRARIES: List<SoftwareLibrary> = listOf(
            SoftwareLibrary(
                name = "AppCompat",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.appcompat/appcompat/1.3.1",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "ConstraintLayout",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.constraintlayout/constraintlayout/2.1.0",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "Core Kotlin Extensions",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.core/core-ktx/1.3.2",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "Kuantity",
                author = "Thomas Reader",
                url = "https://gitlab.com/thomasreader/kuantity",
                license = SoftwareLicense.MIT
            ),
            SoftwareLibrary(
                name = "Lifecycle Extensions",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-extensions",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "Logcat",
                author = "Square, Inc.",
                url = "https://github.com/square/logcat",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "Material Components",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/com.google.android.material/material/1.4.0",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "OpenJDK Desugaring",
                author = "Google",
                url = "https://mvnrepository.com/artifact/com.android.tools/desugar_jdk_libs/1.1.5",
                license = SoftwareLicense.GPL_2_0
            ),
            SoftwareLibrary(
                name = "Preferences DataStore",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.datastore/datastore-preferences/1.0.0",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "RecyclerView",
                author = DROID_OPEN_SOURCE_PROJECT,
                url = "https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview/1.2.0",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "Simple Stack",
                author = "Zhuinden",
                url = "https://github.com/Zhuinden/simple-stack",
                license = SoftwareLicense.APACHE_2_0
            ),
            SoftwareLibrary(
                name = "SQLDelight",
                author = "CashApp",
                url = "https://cashapp.github.io/sqldelight/",
                license = SoftwareLicense.APACHE_2_0
            )
        )
    }
    interface DisplayOptions {
        companion object {
            @JvmField
            val DARK_MODE: List<Int> =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    listOf(
                            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM,
                            AppCompatDelegate.MODE_NIGHT_NO,
                            AppCompatDelegate.MODE_NIGHT_YES
                    )
                } else {
                    listOf(
                        AppCompatDelegate.MODE_NIGHT_NO,
                        AppCompatDelegate.MODE_NIGHT_YES
                    )
                }

            @JvmField
            val DARK_MODE_RES: List<Int> = DARK_MODE.map { darkMode ->
                when (darkMode) {
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM -> R.string.follow_system
                    AppCompatDelegate.MODE_NIGHT_NO -> R.string.light_mode
                    AppCompatDelegate.MODE_NIGHT_YES -> R.string.dark_mode
                    else -> throw IllegalStateException("Illegal dark mode: $darkMode")
                }
            }

            @JvmField
            val RACQUET_LENGTH_UNITS = listOf(
                DroidUnits.LengthUnits.CENTIMETRE,
                DroidUnits.LengthUnits.INCH
            )

            @JvmField
            val RACQUET_WEIGHT_UNITS = listOf(
                DroidUnits.MassUnits.GRAM,
                DroidUnits.MassUnits.OUNCE
            )

            @JvmField
            val RACQUET_HEAD_SIZE_UNITS = listOf(
                DroidUnits.AreaUnits.SQUARE_CENTIMETRE,
                DroidUnits.AreaUnits.SQUARE_INCH
            )

            @JvmField
            val RACQUET_BALANCE_UNITS = listOf(
                DroidUnits.LengthUnits.CENTIMETRE,
                DroidUnits.LengthUnits.INCH,
                DroidUnits.LengthUnits.BALANCE_POINT
            )

            @JvmField
            val RACQUET_GRIP_SIZE_UNITS = listOf(
                DroidUnits.LengthUnits.CENTIMETRE,
                DroidUnits.LengthUnits.INCH,
                DroidUnits.LengthUnits.GRIP_SIZE
            )

            @JvmField
            val STRING_TENSION_UNITS = listOf(
                DroidUnits.MassUnits.KILOGRAM,
                DroidUnits.MassUnits.POUND
            )
        }
        object Format {
            private val NO_DECIMAL_FORMAT = DecimalFormat("0")
            private val ONE_DECIMAL_OPTIONAL_FORMAT = DecimalFormat("0.#")
            private val TWO_DECIMAL_FORMAT = DecimalFormat("0.00")

            fun lengthFormatter(unit: DroidUnit<Length>) = ONE_DECIMAL_OPTIONAL_FORMAT

            fun weightFormatter(unit: DroidUnit<Mass>) = when (unit) {
                DroidUnits.MassUnits.GRAM -> NO_DECIMAL_FORMAT
                DroidUnits.MassUnits.OUNCE -> ONE_DECIMAL_OPTIONAL_FORMAT
                else -> throw IllegalStateException()
            }

            fun headSizeFormatter(unit: DroidUnit<Area>) = NO_DECIMAL_FORMAT

            fun balancePointFormatter(unit: DroidUnit<Length>) = ONE_DECIMAL_OPTIONAL_FORMAT

            //fun gripSizeFormatter(unit: DroidUnit<Length>) = ONE_DECIMAL_OPTIONAL_FORMAT

            fun stringTensionFormatter(unit: DroidUnit<Mass>) = when (unit) {
                DroidUnits.MassUnits.KILOGRAM -> ONE_DECIMAL_OPTIONAL_FORMAT
                DroidUnits.MassUnits.POUND -> NO_DECIMAL_FORMAT
                else -> throw IllegalStateException()
            }

            fun stringThicknessFormatter() = TWO_DECIMAL_FORMAT

            fun beamWidthFormatter() = NO_DECIMAL_FORMAT
        }
    }
}