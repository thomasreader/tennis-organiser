package com.gitlab.thomasreader.tennisorganiser.core.savestate

import androidx.lifecycle.SavedStateHandle
import kotlinx.coroutines.flow.MutableStateFlow

fun <T> SavedStateHandle.getStateFlow(key: String): MutableStateFlow<T?> {
    return getStateFlow(key, null)
}

fun <T> SavedStateHandle.getStateFlow(key: String, initialValue: T): MutableStateFlow<T> {
    val value: T = this.get<T>(key)?: initialValue
    val internalFlow = MutableStateFlow(value)
    return SavedStateHandleStateFlow<T>(key, this, internalFlow)
}

private class SavedStateHandleStateFlow<T>(
    private val key: String,
    private val handle: SavedStateHandle,
    private val internalFlow: MutableStateFlow<T>
): MutableStateFlow<T> by internalFlow {
    override var value: T
        get() = internalFlow.value
        set(value) {
            handle[key] = value
            internalFlow.value = value
        }
}