package com.gitlab.thomasreader.tennisorganiser.screen.string.stringdetails

import android.animation.ValueAnimator
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.doOnEnd
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.core.extension.invisible
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.core.extension.visibleWhen
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.StringVariantRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringVariant

class StringDetailsAdapter(
    val onLongClickListener: (StringVariant) -> Unit
): ListAdapter<StringVariant, StringDetailsAdapter.ViewHolder>(StringVariantDiffCallback()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, StringVariantRecyclerRowBinding::inflate))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNullOrEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            try {
                val (old, new) = payloads[0] as Pair<StringVariant, StringVariant>
                holder.update(old, new)
            } catch (castException: ClassCastException) {
                super.onBindViewHolder(holder, position, payloads)
            }
        }
    }

    inner class ViewHolder(
        binding: StringVariantRecyclerRowBinding
    ): BindingViewHolder<StringVariantRecyclerRowBinding>(binding), View.OnLongClickListener {
        init {
            itemView.setOnLongClickListener(this)
        }

        override fun onLongClick(v: View): Boolean {
            onLongClickListener(currentList[bindingAdapterPosition])
            return true
        }

        fun bind(string: StringVariant) {
            with (binding) {
                stringWidth.text = string.thicknessWithUnit

                when (string.rating != null) {
                    true -> {
                        stringRatingText.text = string.rating
                        stringRatingText.visible()
                        stringRating.visible()
                    }
                    false -> {
                        stringRatingText.invisible()
                        stringRating.invisible()
                    }
                }

                stringNotes.visibleWhen(string.notes != null)
                stringNotes.text = string.notes
            }
        }

        fun update(old: StringVariant, new: StringVariant) {
            if (old.thicknessWithUnit != new.thicknessWithUnit) {
                binding.stringWidth.text = new.thicknessWithUnit
            }
            if (old.rating != new.rating) {
                when (new.rating != null) {
                    true -> {
                        binding.stringRatingText.text = new.rating
                        binding.stringRatingText.visible()
                        binding.stringRating.visible()
                        if (old.rating == null) {
                            ValueAnimator.ofFloat(binding.stringRatingText.alpha, 1f).apply {
                                addUpdateListener {
                                    val alpha = it.animatedValue as Float
                                    binding.stringRatingText.alpha = alpha
                                    binding.stringRating.alpha = alpha
                                }
                                duration = 400
                                start()
                            }
                        }
                    }
                    false -> {
                        ValueAnimator.ofFloat(binding.stringRatingText.alpha, 0f).apply {
                            addUpdateListener {
                                val alpha = it.animatedValue as Float
                                binding.stringRatingText.alpha = alpha
                                binding.stringRating.alpha = alpha
                            }
                            doOnEnd {
                                binding.stringRatingText.invisible()
                                binding.stringRating.invisible()
                            }
                            duration = 400
                            start()
                        }
                    }
                }
            }
            if (old.notes != new.notes) {
                binding.stringNotes.text = new.notes
            }
        }
    }

    private class StringVariantDiffCallback : DiffUtil.ItemCallback<StringVariant>() {
        override fun areItemsTheSame(oldItem: StringVariant, newItem: StringVariant): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StringVariant, newItem: StringVariant): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: StringVariant, newItem: StringVariant): Pair<StringVariant, StringVariant> {
            return oldItem to newItem
        }
    }
}