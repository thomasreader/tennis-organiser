package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitydetails

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.TagChipBinding
import com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert.TagAdapter

class TagChipAdapter: ListAdapter<String, TagChipAdapter.ViewHolder>(TagAdapter.StringDiffer()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, TagChipBinding::inflate))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(
        binding: TagChipBinding
    ): BindingViewHolder<TagChipBinding>(binding) {
        fun bind(tag: String) {
            this.binding.tagChip.text = tag
        }
    }
}