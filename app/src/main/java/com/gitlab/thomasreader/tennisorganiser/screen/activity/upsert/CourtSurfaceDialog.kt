package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import com.gitlab.thomasreader.tennisorganiser.data.CourtSurface
import com.gitlab.thomasreader.tennisorganiser.domain.activity.stringResource

class ArrayAdapterMap<T>(
    context: Context,
    @LayoutRes val layout: Int,
    values: Array<T>,
    @IdRes val textViewId: Int? = null,
    val mapper: (T) -> CharSequence
): ArrayAdapter<T>(context, layout, values) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View = convertView ?: LayoutInflater.from(context).inflate(layout, parent, false)
        val text: TextView? = if (view is TextView) view else view.findViewById(textViewId!!)

        val item = getItem(position)
        item?.let {
            text?.let {
                if (item is CharSequence) {
                    text.text = item
                } else {
                    text.text = mapper(item)
                }
            }
        }

        return view
    }
}

class CourtSurfaceDialog: DialogFragment() {
    var onPositiveButtonClickListener: ((CourtSurface) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val adapter = ArrayAdapterMap(
            requireContext(),
            android.R.layout.simple_list_item_1,
            CourtSurface.values()
        ) { courtSurface ->
            getString(courtSurface.stringResource)
        }
        val onClickListener = DialogInterface.OnClickListener { dialog, which ->
            adapter.getItem(which)?.let { surface ->
                onPositiveButtonClickListener?.let {
                    it(surface)
                }
                dialog.dismiss()
            }
        }
        return AlertDialog.Builder(requireContext())
            .setSingleChoiceItems(
                adapter,
                0,
                onClickListener
            )
            .show()
    }
}