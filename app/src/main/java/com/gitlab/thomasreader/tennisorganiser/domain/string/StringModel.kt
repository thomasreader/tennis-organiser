package com.gitlab.thomasreader.tennisorganiser.domain.string

import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider

data class StringModel(
    val id: Long,
    val brand: String,
    val model: String,
    val composition: String?
) {
    val brandModelRes = DroidTextProvider.stringId(R.string.brand_model_join, brand, model)
}