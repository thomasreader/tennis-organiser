package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.details

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DataStoreKeys
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.data.getMapped
import com.gitlab.thomasreader.tennisorganiser.core.data.toDroidTextResource
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.toDomain
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.detail.RacquetDetailRow
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

sealed interface RacquetModelDetailsEvent {
    object Deleted: RacquetModelDetailsEvent
}

data class RacquetModelDetailsState(
    val brandModelRes: DroidTextProvider,
    val details: List<RacquetDetailRow>
)

class RacquetModelDetailsViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
): KeyedViewModel<RacquetModelDetailsScreen>(_key) {
    private val _events = Channel<RacquetModelDetailsEvent>(Channel.BUFFERED)
    val events: Flow<RacquetModelDetailsEvent> = _events.receiveAsFlow()

    val uiState: StateFlow<RacquetModelDetailsState?> = combine(
        database.racquetModelQueries.selectModelById(key.modelId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        dataStore.data
    ) { racquetModel, preferences ->
        val racquetLengthUnit = preferences.getMapped(DataStoreKeys.RACQUET_LENGTH)
        val racquetWeightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
        val racquetHeadSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_HEAD_SIZE)
        val racquetBalancePointUnit = preferences.getMapped(DataStoreKeys.RACQUET_BALANCE_POINT)
        val stringTensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)

        val domainModel = racquetModel.toDomain(
            lengthUnit = racquetLengthUnit,
            weightUnit = racquetWeightUnit,
            headSizeUnit = racquetHeadSizeUnit,
            balanceUnit = racquetBalancePointUnit,
            tensionUnit = stringTensionUnit
        )

        val details = listOf(
            RacquetDetailRow(DroidTextProvider.stringId(R.string.head_size), domainModel.headSizeString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.length), domainModel.lengthString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.weight), domainModel.weightString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.balance_point), domainModel.balanceString?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.swing_weight), domainModel.swingWeight?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.stiffness), domainModel.stiffness?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.beam_width), domainModel.beamWidthRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_pattern), domainModel.stringPatternRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_tension), domainModel.tensionRangeRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.mains_skip), domainModel.skipRes),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.string_pieces), domainModel.stringPieces?.toString()?.toDroidTextResource()),
            RacquetDetailRow(DroidTextProvider.stringId(R.string.shared_holes), domainModel.sharedHoles?.toString()?.toDroidTextResource())
        )

        return@combine RacquetModelDetailsState(
            brandModelRes = DroidTextProvider.stringId(
                R.string.brand_model_join,
                domainModel.brand,
                domainModel.model
            ),
            details = details
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun deleteRacquetModel() {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            database.racquetModelQueries.deleteModel(key.modelId)
            _events.send(RacquetModelDetailsEvent.Deleted)
        }
    }
}