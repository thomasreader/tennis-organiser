package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist

import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.SearchViewBinding
import com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert.TagAdapter

class SearchAdapter(
    val searchListener: (String) -> Unit
): ListAdapter<String, SearchAdapter.ViewHolder>(TagAdapter.StringDiffer()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, SearchViewBinding::inflate))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        println(getItem(position))
        holder.bind(getItem(position))
    }

    inner class ViewHolder(
        binding: SearchViewBinding
    ): BindingViewHolder<SearchViewBinding>(binding), TextWatcher {
        init {
            this.binding.searchInput.addTextChangedListener(this)
        }

        fun bind(search: String) {
            this.binding.searchInput.setText(search)
        }

        fun update(oldSearch: String, newSearch: String) {
            if (!this.binding.searchInput.hasFocus()) {
                if (oldSearch != newSearch) {
                    this.binding.searchInput.setText(newSearch)
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            s?.let { searchListener(it.toString()) }
        }

        override fun afterTextChanged(s: Editable?) = Unit
    }
}