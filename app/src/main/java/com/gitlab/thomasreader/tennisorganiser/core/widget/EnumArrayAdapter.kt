package com.gitlab.thomasreader.tennisorganiser.core.widget

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes

class EnumArrayAdapter<T: Enum<*>>(
    context: Context,
    @LayoutRes private val layoutRes: Int,
    private val enums: List<T>,
    private val toStringBlock: (T) -> String
): ArrayAdapter<T>(context, layoutRes, enums) {
    private val enumCharSequenceMap = enums.associateWith { toStringBlock(it) }
    private val charSequenceEnumMap = enumCharSequenceMap.map { it.value to it.key }.toMap()

    fun enumFromCharSequenceValue(string: String): T? {
        return charSequenceEnumMap[string]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textViewRow = (convertView ?: LayoutInflater.from(context)
            .inflate(layoutRes, parent, false)) as TextView
        val enum = enums[position]
        textViewRow.text = enumCharSequenceMap[enum]
        return textViewRow
    }

    override fun getFilter(): Filter {
        return object: Filter() {
            override fun convertResultToString(resultValue: Any?): CharSequence {
                return toStringBlock(resultValue as T)
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults? {
                return null
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) = Unit
        }
    }
}