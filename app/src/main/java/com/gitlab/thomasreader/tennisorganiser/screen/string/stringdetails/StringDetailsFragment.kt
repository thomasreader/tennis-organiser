package com.gitlab.thomasreader.tennisorganiser.screen.string.stringdetails

import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll

import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.StringDetailsBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.StringVariantAddBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.screen.string.edit.EditStringModelScreen
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.lang.NumberFormatException

class StringDetailsFragment: KeyedFragment(R.layout.string_details) {
    private val binding: StringDetailsBinding by bindView(StringDetailsBinding::bind)
    private val stringDetailsAdapter: StringDetailsAdapter = StringDetailsAdapter { variant ->
        viewModel.openEditDialog(variant)
    }
    lateinit var viewModel: StringDetailsViewModel
    private var addDialog: AlertDialog? = null
    private var editDialog: AlertDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.edit_action -> {
                        backstack.goTo(EditStringModelScreen(getKey<StringDetailsScreen>().stringModelId))
                    }
                    R.id.delete_action -> {
                        confirmDeleteModelAlert()
                    }
                    else -> return@setOnMenuItemClickListener false
                }
                true
            }

            addStringVariantFab.setOnClickListener {
                viewModel.openAddDialog()
            }

            stringVariantList.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = stringDetailsAdapter
                animateFABOnScroll(addStringVariantFab)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        addDialog?.dismiss()
        addDialog = null
        editDialog?.dismiss()
        editDialog = null
    }

    private fun initViewModel() {
        viewModel = savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.stringDetailsState.collect { state ->
                        if (state != null) {
                            with (state.stringModel) {
                                binding.toolbar.title = brandModelRes.toString(resources)
                                if (composition != null) {
                                    binding.toolbar.subtitle = composition
                                }
                                stringDetailsAdapter.submitList(state.stringVariants)
                                println(state)
                            }

                        }
                    }
                }
            }
            launch {
                viewModel.events.collect { event ->
                    when (event) {
                        is StringDetailEvent.Deleted -> {
                            Toast.makeText(
                                requireActivity(),
                                resources.getText(R.string.deleted_format, event.msg.toString(resources)),
                                Toast.LENGTH_SHORT
                            ).show()
                            backstack.goBack()
                        }
                        StringDetailEvent.AddedVariant -> {
                            addDialog?.dismiss()
                            addDialog = null
                        }
                        StringDetailEvent.DeletedVariant -> {
                            editDialog?.dismiss()
                            editDialog = null
                        }
                        StringDetailEvent.EditedVariant -> {
                            editDialog?.dismiss()
                            editDialog = null
                        }
                        is StringDetailEvent.UniqueConstraintFailedVariant -> {
                            Toast.makeText(
                                requireContext(),
                                event.msg.toString(resources),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
            }
            launch {
                viewModel.isDialogOpenFlow.collect { dialogType ->
                    when (dialogType) {
                        is StringDetailDialogType.Add -> {
                            editDialog?.dismiss()
                            editDialog = null
                            if (addDialog == null) {
                                addStringVariant()
                            }
                        }
                        is StringDetailDialogType.Edit -> {
                            addDialog?.dismiss()
                            addDialog = null
                            if (editDialog == null) {
                                editStringVariant(
                                    dialogType.variantId,
                                    dialogType.thickness,
                                    dialogType.rating,
                                    dialogType.notes
                                )
                            }
                        }
                        null -> {
                            addDialog?.dismiss()
                            addDialog = null
                            editDialog?.dismiss()
                            editDialog = null
                        }
                    }
                }
            }
        }
    }

    private fun confirmDeleteAlert(stringVariantId: Long) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_string_variant)
            .setMessage(R.string.delete_string_variant_message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                viewModel.deleteVariant(stringVariantId)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun confirmDeleteModelAlert() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_string_model)
            .setMessage(R.string.delete_string_model_message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                viewModel.deleteStringModel()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun addStringVariant() {
        val addStringView = StringVariantAddBinding.inflate(layoutInflater, null, false)

        addStringView.stringRatingInput.filters = arrayOf(
            InputFilter { source, _, _, dest, dStart, dEnd ->
                try {
                    val f: Float = (dest.subSequence(0, dStart).toString() + source + dest.subSequence(dEnd, dest.length)).toFloat()
                    if (f in 0f..5f) return@InputFilter null
                } catch (_: NumberFormatException) {}
                ""
            }
        )

        val job: Job = viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.dialogState.collect { state ->
                    println(state?.toString(resources))
                    addStringView.stringThicknessLayout.error = state?.toString(resources)
                    addDialog?.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = state == null
                }
            }
        }

        addStringView.stringThickness.setText(viewModel.thickness)
        addStringView.stringRatingInput.setText(viewModel.rating)
        addStringView.stringNotes.setText(viewModel.notes)

        addStringView.stringThickness.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.thickness = it
            }
        }

        addStringView.stringRatingInput.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.rating = it
            }
        }

        addStringView.stringNotes.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.notes = it
            }
        }

        addDialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.add_string_variant)
            .setView(addStringView.root)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                viewModel.closeDialog()
            }
            .setOnCancelListener {
                viewModel.closeDialog()
            }
            .setOnDismissListener { job.cancel() }
            .create()
            .apply {
                setOnShowListener {
                    val button = getButton(AlertDialog.BUTTON_POSITIVE)
                    button.setOnClickListener {
                        val thickness = addStringView.stringThickness.text?.toString()?.toDoubleOrNull()
                        val rating = addStringView.stringRatingInput.text?.toString()?.toFloatOrNull()
                        val notes = addStringView.stringNotes.text?.toString()
                        viewModel.addVariant(thickness, rating, notes)
                    }
                }
                show()
            }
    }

    private fun editStringVariant(variantId: Long, thickness: String, rating: String?, notes: String?) {
        val addStringView = StringVariantAddBinding.inflate(layoutInflater, null, false)

        addStringView.stringRatingInput.filters = arrayOf(
            InputFilter { source, _, _, dest, dStart, dEnd ->
                try {
                    val f: Float = (dest.subSequence(0, dStart).toString() + source + dest.subSequence(dEnd, dest.length)).toFloat()
                    if (f in 0f..5f) return@InputFilter null
                } catch (_: NumberFormatException) {}
                ""
            }
        )

        val job: Job = viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.dialogState.collect { state ->
                    addStringView.stringThicknessLayout.error = state?.toString(resources)
                    editDialog?.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = state == null
                }
            }
        }

        addStringView.stringThickness.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.thickness = it
            }
        }

        addStringView.stringRatingInput.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.rating = it
            }
        }

        addStringView.stringNotes.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.let {
                viewModel.notes = it
            }
        }

        addStringView.stringThickness.setText(viewModel.thickness ?: thickness)
        addStringView.stringRatingInput.setText(viewModel.rating ?: rating)
        addStringView.stringNotes.setText(viewModel.notes ?: notes)

        editDialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.edit_string_variant)
            .setView(addStringView.root)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                viewModel.closeDialog()
            }
            .setOnCancelListener {
                viewModel.closeDialog()
            }
            .setNeutralButton(resources.getText(R.string.delete)) { _, _ ->
                confirmDeleteAlert(variantId)
                viewModel.closeDialog()
            }
            .setOnDismissListener { job.cancel() }
            .create()
            .apply {
                setOnShowListener {
                    val button = getButton(AlertDialog.BUTTON_POSITIVE)
                    button.setOnClickListener {
                        val thickness = addStringView.stringThickness.text?.toString()?.toDoubleOrNull()
                        val rating = addStringView.stringRatingInput.text?.toString()?.toFloatOrNull()
                        val notes = addStringView.stringNotes.text?.toString()
                        viewModel.editVariant(variantId, thickness, rating, notes)
                    }
                }
                show()
            }
    }
}