package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.restring

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.tennisorganiser.R

import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.databinding.StringBedAddBinding
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModelVariant
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomAbbreviation
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomArrayAdapters
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class RestringRacquetFragment: KeyedFragment(R.layout.string_bed_add) {
    private val binding by bindView(StringBedAddBinding::bind)
    private lateinit var viewModel: RestringRacquetViewModel
    private var isInit: Boolean = false
    private var dialog: Dialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)

        with (binding) {
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        restringRacquet()
                        true
                    }
                    else -> false
                }
            }
            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }

            viewModel.setMainsTension(mainsTensionInput.text?.toString()?.toDoubleOrNull())
            viewModel.setCrossTension(crossTensionInput.text?.toString()?.toDoubleOrNull())

            mainsTensionInput.doOnTextChanged { text, _, _, _ ->
                viewModel.setMainsTension(text?.toString()?.toDoubleOrNull())
            }
            crossTensionInput.doOnTextChanged { text, _, _, _ ->
                viewModel.setCrossTension(text?.toString()?.toDoubleOrNull())
            }

            stringBedDateInput.setOnClickListener {
                val today = Calendar.getInstance()
                dialog = DatePickerDialog(
                    requireContext(),
                    DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        val cal = Calendar.getInstance().apply { set(year, month, dayOfMonth) }
                        viewModel.stringDate.value = cal.timeInMillis
                        dialog?.dismiss()
                        dialog = null
                    },
                    today.get(Calendar.YEAR),
                    today.get(Calendar.MONTH),
                    today.get(Calendar.DAY_OF_MONTH)
                ).apply {
                    setTitle("Select stringing date")
                    datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), DatePicker.OnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
                        println("$dayOfMonth / ${monthOfYear + 1} / $year")
                    })
                    datePicker.maxDate = Calendar.getInstance().timeInMillis
                    show()
                }
            }

            val tensionAdapter = UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.DESC,
                Constants.DisplayOptions.STRING_TENSION_UNITS
            )
            tensionUnitInput.setAdapter(tensionAdapter)
            tensionUnitInput.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                viewModel.stringUnitIx.value = position
            }
        }
    }

     override fun onDestroyView() {
         super.onDestroyView()
         dialog?.dismiss()
         dialog = null
    }

    private fun initViewModel() {
        viewModel = savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            if (!isInit) {
                                isInit = true

                                binding.toolbar.subtitle = state.racquetInfo.toString(requireContext())

                                val variantAdapter = StringVariantAdapter(
                                    requireContext(),
                                    state.stringVariants
                                )

                                binding.mainsVariantInput.setOnClickListener {
                                    dialog = AlertDialog.Builder(requireContext())
                                        .setTitle("Select string mains")
                                        .setSingleChoiceItems(
                                            variantAdapter,
                                            0,
                                            DialogInterface.OnClickListener { dialog, which ->
                                                val mains = variantAdapter.getItem(which)
                                                viewModel.updateMainsString(mains)
                                                dialog.dismiss()
                                                this@RestringRacquetFragment.dialog = null
                                            }).show()
                                }

                                binding.crossVariantInput.setOnClickListener {
                                    dialog = AlertDialog.Builder(requireContext())
                                        .setTitle("Select string crosses")
                                        .setSingleChoiceItems(
                                            variantAdapter,
                                            0,
                                            DialogInterface.OnClickListener { dialog, which ->
                                                val mains = variantAdapter.getItem(which)
                                                viewModel.updateCrossString(mains)
                                                dialog.dismiss()
                                                this@RestringRacquetFragment.dialog = null
                                            }).show()
                                }
                            }
                            binding.mainsVariantInput.setText(state.mainsString?.description?.toString(resources))
                            binding.crossVariantInput.setText(state.crossString?.description?.toString(resources))
                            binding.stringBedDateInput.setText(state.formattedDate)
                            binding.stringBedDateLayout.updateErrorVisibility(
                                state.dateErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.mainsVariantLayout.updateErrorVisibility(
                                state.mainsErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.crossVariantLayout.updateErrorVisibility(
                                state.crossErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.mainsTensionLayout.updateErrorVisibility(
                                state.mainTensionErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.crossTensionLayout.updateErrorVisibility(
                                state.crossTensionErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.tensionUnitLayout.updateErrorVisibility(
                                state.unitErr,
                                DroidTextProvider.REQUIRED
                            )
                            binding.toolbar.menu.findItem(R.id.save_action)?.isEnabled = state.saveEnabled
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            RestringRacquetEvent.Saved -> backstack.goBack()
                        }
                    }
                }
            }
        }
    }

    private fun assertGoBack() {
        dialog = AlertDialog.Builder(requireContext())
            .setTitle("Discard entry?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun restringRacquet() {
        val mainsTension = binding.mainsTensionInput.text?.toString()?.toDoubleOrNull()
        val crossTension = binding.crossTensionInput.text?.toString()?.toDoubleOrNull()
        val notes = binding.stringBedNotesInput.text?.toString()

        viewModel.restringRacquet(
            mainsTension = mainsTension,
            crossTension = crossTension,
            notes = notes
        )
    }
}

private class StringVariantAdapter(
    context: Context,
    val stringVariants: List<StringModelVariant>
): ArrayAdapter<StringModelVariant>(
    context,
    0,
    stringVariants
) {
    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        val textView = (convertView ?: LayoutInflater.from(context).inflate(
            android.R.layout.simple_list_item_1,
            parent,
            false
        )) as TextView
        textView.text = stringVariants[position].description.toString(context)
        return textView
    }
}