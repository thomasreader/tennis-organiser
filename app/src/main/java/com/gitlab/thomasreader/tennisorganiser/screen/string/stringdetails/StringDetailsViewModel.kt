package com.gitlab.thomasreader.tennisorganiser.screen.string.stringdetails

import android.os.Parcelable
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.times
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModel
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringVariant
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.domain.string.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize
import logcat.logcat
import java.sql.SQLException
import java.text.DecimalFormat

sealed interface StringDetailDialogType : Parcelable {
    @Parcelize
    object Add: StringDetailDialogType
    @Parcelize
    data class Edit(
        val variantId: Long,
        val thickness: String,
        val rating: String?,
        val notes: String?
    ): StringDetailDialogType
}

data class StringDetailsState(
    val stringModel: StringModel,
    val stringVariants: List<StringVariant>
)

sealed interface StringDetailEvent {
    class Deleted(stringModel: StringModel): StringDetailEvent {
        val msg: DroidTextProvider = DroidTextProvider.stringId(R.string.deleted_string_model_format, stringModel.brand, stringModel.model)
    }
    object DeletedVariant: StringDetailEvent
    object EditedVariant: StringDetailEvent
    object AddedVariant: StringDetailEvent
    object UniqueConstraintFailedVariant: StringDetailEvent {
        val msg = DroidTextProvider.stringId(R.string.duplicate_string_model_err)
    }
}

class StringDetailsViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<StringDetailsScreen>(_key) {
    private val _events = Channel<StringDetailEvent>(Channel.BUFFERED)
    val events = _events.receiveAsFlow()

    val stringDetailsState: StateFlow<StringDetailsState?> = combine(
        database.stringModelQueries.selectModelById(key.stringModelId, ::StringModel)
            .asFlow()
            .mapToOne(Dispatchers.IO),

        database.stringVariantQueries.selectVariantsByModelIdAsc(key.stringModelId)
            .asFlow()
            .mapToList(Dispatchers.IO)
            .map { variants ->
                val ratingFormat = DecimalFormat("#.#")
                val thicknessFormat = DecimalFormat("#.00")
                variants.map { it.toDomain(ratingFormat, thicknessFormat) }
            }

    ) { model, variants -> StringDetailsState(model, variants) }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    private var dialogType: StringDetailDialogType? = savedStateHandle["dialog_type"]
        set(value) {
            savedStateHandle["dialog_type"] = value
            field = value
            _isDialogOpenFlow.value = value
        }

    var thickness: String? = savedStateHandle["thickness"]
        set(value) {
            savedStateHandle["thickness"] = value
            field = value
            thicknessError.value = if (value.isNullOrBlank()) DroidTextProvider.REQUIRED else null
        }

    var rating: String? = savedStateHandle["rating"]
        set(value) {
            savedStateHandle["rating"] = value
            field = value
        }

    var notes: String? = savedStateHandle["notes"]
        set(value) {
            savedStateHandle["notes"] = value
            field = value
        }

    private val _isDialogOpenFlow = MutableStateFlow(dialogType)
    val isDialogOpenFlow: StateFlow<StringDetailDialogType?> = _isDialogOpenFlow

    private val thicknessError: MutableStateFlow<DroidTextProvider?> = MutableStateFlow(
        DroidTextProvider.REQUIRED)
    val dialogState: StateFlow<DroidTextProvider?> = thicknessError

    fun openAddDialog() {
        dialogType = StringDetailDialogType.Add
    }

    fun openEditDialog(stringVariant: StringVariant) {
        dialogType = StringDetailDialogType.Edit(
            stringVariant.id,
            stringVariant.thickness,
            stringVariant.rating,
            stringVariant.notes
        )
    }

    fun closeDialog() {
        dialogType = null
        thickness = null
        rating = null
        notes = null
    }

    fun deleteVariant(stringVariantId: Long) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            database.stringVariantQueries.deleteVariant(stringVariantId)
            _events.send(StringDetailEvent.DeletedVariant)
        }
    }

    fun editVariant(stringVariantId: Long, thickness: Double?, rating: Float?, notes: String?) {
        if (thickness != null) {
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                try {
                    database.stringVariantQueries.updateVariant(
                        thickness * DroidUnits.LengthUnits.MILLIMETRE,
                        rating,
                        notes?.ifEmpty { null },
                        stringVariantId
                    )
                    _events.send(StringDetailEvent.EditedVariant)
                } catch (sqlException: SQLException) {
                    _events.send(StringDetailEvent.UniqueConstraintFailedVariant)
                }
            }
        }
    }

    fun addVariant(thickness: Double?, rating: Float?, notes: String?) {
        if (thickness != null) {
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                try {
                    database.stringVariantQueries.insertVariant(
                        key.stringModelId,
                        thickness * DroidUnits.LengthUnits.MILLIMETRE,
                        rating,
                        notes?.ifBlank { null })
                    _events.send(StringDetailEvent.AddedVariant)
                } catch (sqlException: SQLException) {
                    _events.send(StringDetailEvent.UniqueConstraintFailedVariant)
                }
            }
        }

    }

    fun deleteStringModel() {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            val stringModel = when (stringDetailsState.value != null) {
                true -> stringDetailsState.value!!.stringModel
                false -> database.stringModelQueries.selectModelById(key.stringModelId, ::StringModel).executeAsOne()
            }
            database.stringModelQueries.deleteModel(key.stringModelId)
            _events.send(StringDetailEvent.Deleted(stringModel))
        }
    }
}

