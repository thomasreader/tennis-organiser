package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityListBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.screen.activity.activitydetails.ActivityMatchDetailsScreen
import com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert.UpsertActivityScreen
import com.gitlab.thomasreader.tennisorganiser.screen.settings.SettingsScreen
import com.gitlab.thomasreader.tennisorganiser.util.TennisBallLongClickListener
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ActivityListFragment: KeyedFragment(R.layout.activity_list) {
    private val binding by bindView(ActivityListBinding::bind)
    private val activityListAdapter: ActivityListAdapter = ActivityListAdapter { tennisActivity ->
        backstack.goTo(ActivityMatchDetailsScreen(tennisActivity.id))
    }
    private val searchAdapter = SearchAdapter {
        viewModel.search(it)
    }
    private lateinit var viewModel: ActivityListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.inflateMenu(R.menu.settings_menu)
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.settingsMenu -> {
                        backstack.goTo(SettingsScreen())
                        true
                    }
                    else -> false
                }
            }

            addActivityFab.setOnClickListener {
                backstack.goTo(UpsertActivityScreen())
            }
            addActivityFab.setOnLongClickListener(TennisBallLongClickListener())

            println(viewModel.search.value)
            searchAdapter.submitList(listOf(viewModel.search.value))

            activityListRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = ConcatAdapter(
                    searchAdapter,
                    activityListAdapter
                )
                animateFABOnScroll(addActivityFab)
            }
        }
    }

    private fun initViewModel() {
        viewModel = savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    if (state != null) {
                        activityListAdapter.submitList(state)
                    }
                }
            }
        }
    }
}