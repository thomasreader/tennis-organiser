package com.gitlab.thomasreader.tennisorganiser.application

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.DecelerateInterpolator
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.lifecycle.*
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentStateChanger
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityMainBinding
import com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist.ActivityListScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list.RacquetListScreen
import com.gitlab.thomasreader.tennisorganiser.screen.string.stringlist.StringListScreen
import com.zhuinden.simplestack.History
import com.zhuinden.simplestack.SimpleStateChanger
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestack.navigator.Navigator
import com.zhuinden.simplestackextensions.navigatorktx.backstack
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    val app: App get() = this.application as App
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var fragmentStateChanger: AnimatedFragmentStateChanger
    private val navigationHandler = SimpleStateChanger.NavigationHandler { stateChange ->
        fragmentStateChanger.handleStateChange(stateChange)
        viewModel.setBottomNavVisibility(stateChange.topNewKey<AnimatedFragmentKey>().hideBottomNavigation())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)
        initViewModel()

        fragmentStateChanger = AnimatedFragmentStateChanger(supportFragmentManager, R.id.container)
        Navigator
            .configure()
            .setStateChanger(SimpleStateChanger(this.navigationHandler))
            .install(this, binding.container, History.of(ActivityListScreen()))

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!Navigator.onBackPressed(this@MainActivity)) {
                    this@MainActivity.finish()
                }
            }
        })

//        supportFragmentManager.findFragmentByTag(backstack.top<DefaultFragmentKey>().fragmentTag)?.let {
//            if (it is ScopedServices.HandlesBack) {
//                if (it.onBackEvent()) return
//            }
//            if (!Navigator.onBackPressed(this)) super.onBackPressed()
//        }

        binding.bottomNav.setOnItemSelectedListener { menuItem ->
            val destination: AnimatedFragmentKey? = when (menuItem.itemId) {
                R.id.page_activities -> {
                    ActivityListScreen()
                }
                R.id.page_racquets -> {
                    RacquetListScreen()
                }
                R.id.page_strings -> {
                    StringListScreen()
                }
                else -> null
            }

            destination?.let { screen ->
                val prevBackstack = backstack.getHistory<AnimatedFragmentKey>()

                if (prevBackstack.size == 1 && prevBackstack[0].fragmentTag == screen.fragmentTag) {
                    false
                } else {
                    if (prevBackstack[0].fragmentTag == screen.fragmentTag) {
                        backstack.jumpToRoot(StateChange.REPLACE)
                    } else {
                        backstack.setHistory(listOf(screen), StateChange.REPLACE)
                    }
                    true
                }
            } ?: false
        }
    }

    private fun initViewModel() {
        viewModel = app.dependencies.mainActivityViewModel(this)

        lifecycleScope.launch {
            launch {
                viewModel.darkMode.collect { darkMode ->
                    AppCompatDelegate.setDefaultNightMode(darkMode)
                }
            }

            launch {
                viewModel.hiddenBottomNavigation.collectIndexed { index, hidden ->
                    // if activity created first time or recreated on config change so don't animate change
                    if (index == 0) {
                        // draw listener else bottom nav may not be measured
                        val drawListener = object : ViewTreeObserver.OnPreDrawListener {
                            override fun onPreDraw(): Boolean {
                                binding.bottomNav.viewTreeObserver.removeOnPreDrawListener(this)
                                hideBottomNavigation(viewModel.hiddenBottomNavigation.value)
                                return true
                            }
                        }
                        binding.bottomNav.viewTreeObserver.addOnPreDrawListener(drawListener)
                    } else {
                        animateBottomNavigation(hidden)
                    }
                }
            }
        }
    }

    private fun hideBottomNavigation(hidden: Boolean) {
        binding.bottomNav.let { bottomNav ->
            val layoutParams = (bottomNav.layoutParams as ConstraintLayout.LayoutParams)

            when (hidden) {
                false -> layoutParams.setMargins(0, 0, 0, 0)
                true -> layoutParams.setMargins(0, 0, 0, -bottomNav.height)
            }
            bottomNav.layoutParams = layoutParams
        }
    }

    private fun animateBottomNavigation(hidden: Boolean) {
        binding.bottomNav.let { bottomNav ->

            val layoutParams = (bottomNav.layoutParams as ConstraintLayout.LayoutParams)
            val bottomNavHeight = bottomNav.height
            val bottomNavAlpha = bottomNav.alpha

            AnimatorSet().apply {
                when (hidden) {
                    false -> {
                        if (layoutParams.bottomMargin != 0) {
                            play(ValueAnimator.ofInt(layoutParams.bottomMargin, 0).apply {
                                addUpdateListener {
                                    val animatedMargin: Int = it.animatedValue as Int
                                    bottomNav.updateLayoutParams<ConstraintLayout.LayoutParams> {
                                        updateMargins(bottom = animatedMargin)
                                    }
                                }
                            })
                            play(ObjectAnimator.ofFloat(bottomNav, View.ALPHA, bottomNavAlpha, bottomNavAlpha, 1f))
                        }
                    }
                    true -> {
                        if (layoutParams.bottomMargin != -bottomNavHeight) {
                            play(
                                ValueAnimator.ofInt(layoutParams.bottomMargin, -bottomNavHeight)
                                    .apply {
                                        addUpdateListener {
                                            val animatedMargin: Int = it.animatedValue as Int
                                            bottomNav.updateLayoutParams<ConstraintLayout.LayoutParams> {
                                                updateMargins(bottom = animatedMargin)
                                            }
                                        }
                                    })
                            play(ObjectAnimator.ofFloat(bottomNav, View.ALPHA, bottomNavAlpha, 0f, 0f))
                        }
                    }
                }
                duration = 800L
                interpolator = DecelerateInterpolator()
                start()
            }
        }
    }

}