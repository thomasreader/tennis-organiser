package com.gitlab.thomasreader.tennisorganiser.core.anim

import android.animation.TimeInterpolator
import kotlin.math.pow

class TennisBallBounceInterpolator: TimeInterpolator {
    override fun getInterpolation(input: Float): Float {
        return when {
            input < 0.172845f -> 33.4724f * input * input
            input < 0.4292156f -> 33.4724f * (input - 0.172845f - 0.256370f / 2).pow(2) + (1f - 0.55f)
            input < 0.6193451 -> 33.4724f * (input - 0.172845f - 0.256370f - 0.1901295f / 2).pow(2) + (1f - 0.55f.pow(2))
            input < 0.7603489f -> 33.4724f * (input - 0.172845f - 0.256370f - 0.1901295f - 0.1410038f / 2).pow(2) + (1f - 0.55f.pow(3))
            input < 0.8649201f -> 33.4724f * (input - 0.172845f - 0.256370f - 0.1901295f - 0.1410038f - 0.1045712f / 2).pow(2) + (1f - 0.55f.pow(4))
            input < 0.9424722f -> 33.4724f * (input - 0.172845f - 0.256370f - 0.1901295f - 0.1410038f - 0.1045712f - 0.0775520f / 2).pow(2) + (1f - 0.55f.pow(5))
            else -> 33.4724f * (input - 0.172845f - 0.256370f - 0.1901295f - 0.1410038f - 0.1045712f - 0.0775520f - 0.0575141f / 2).pow(2) + (1f - 0.55f.pow(6))
        }
    }
}