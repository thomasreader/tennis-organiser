package com.gitlab.thomasreader.tennisorganiser.core.navigation

import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey

interface KeyProvider<T: DefaultFragmentKey> {
    val key: T
}