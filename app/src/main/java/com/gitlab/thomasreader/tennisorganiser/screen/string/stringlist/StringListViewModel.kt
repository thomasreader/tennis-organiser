package com.gitlab.thomasreader.tennisorganiser.screen.string.stringlist

import android.database.sqlite.SQLiteException
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModel
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import logcat.logcat

sealed interface AddStringModelEvent {
    object Saved: AddStringModelEvent
    object UniqueConstraintFailed: AddStringModelEvent {
        val msg = DroidTextProvider.stringId(R.string.duplicate_string_model_err)
    }
}

data class AddStringDialogState(
    val brands: List<String>,
    val compositions: List<String>,
    val brandError: DroidTextProvider?,
    val modelError: DroidTextProvider?,
    val saveEnabled: Boolean
)

class StringListViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<StringListScreen>(_key) {
    private val _events = Channel<AddStringModelEvent>(Channel.BUFFERED)
    val events: Flow<AddStringModelEvent> = _events.receiveAsFlow()

    val uiState: StateFlow<List<StringModel>?> = database.stringModelQueries.selectAllModelsAsc(::StringModel)
        .asFlow()
        .mapToList(Dispatchers.IO)
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    var brand: String? = savedStateHandle["brand"]
        set(value) {
            savedStateHandle["brand"] = value
            field = value
            brandError.value = if (value.isNullOrBlank()) DroidTextProvider.REQUIRED else null
        }

    var model: String? = savedStateHandle["model"]
        set(value) {
            savedStateHandle["model"] = value
            field = value
            modelError.value = if (value.isNullOrBlank()) DroidTextProvider.REQUIRED else null
        }

    var composition: String? = savedStateHandle["composition"]
        set(value) {
            savedStateHandle["composition"] = value
            field = value
        }

    private var isDialogOpen: Boolean = savedStateHandle["is_dialog_open"] ?: false
        set(value) {
            savedStateHandle["is_dialog_open"] = value
            field = value
        }

    private val _isDialogOpenFlow = MutableStateFlow(isDialogOpen)
    val isDialogOpenFlow: StateFlow<Boolean> = _isDialogOpenFlow

    private val brandError: MutableStateFlow<DroidTextProvider?> = MutableStateFlow(DroidTextProvider.REQUIRED)
    private val modelError: MutableStateFlow<DroidTextProvider?> = MutableStateFlow(DroidTextProvider.REQUIRED)
    private val saveDisabledOverride = MutableStateFlow(false)

    val dialogState = combine(
        database.miscQueries.selectAllBrands().asFlow().mapToList(Dispatchers.IO),
        database.stringModelQueries.selectCompositions().asFlow().mapToList(Dispatchers.IO),
        brandError,
        modelError,
        saveDisabledOverride
    ) { brands, compositions, brandErr, modelErr, saveDisabledOver ->
        val saveDisabled = saveDisabledOver || brandErr != null || modelErr != null
        AddStringDialogState(
            brands = brands,
            compositions = compositions,
            brandError = brandErr,
            modelError = modelErr,
            saveEnabled = !saveDisabled
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(3000), null)

    fun openDialog() {
        _isDialogOpenFlow.value = true
    }

    fun closeDialog() {
        brand = null
        model = null
        composition = null
        _isDialogOpenFlow.value = false
    }

    fun addStringModel(brand: String?, model: String?, composition: String?) {
        val newBrand = brand?.ifBlank { null }
        val newModel = model?.ifBlank { null }
        val newComposition = composition?.ifBlank { null }

        if (newBrand != null && newModel != null) {
            saveDisabledOverride.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                try {
                    database.stringModelQueries.insertModel(
                        brand = newBrand,
                        model = newModel,
                        composition = newComposition
                    )
                    _events.send(AddStringModelEvent.Saved)
                } catch (sqlException: SQLiteException) {
                    // presume unique constraint failed
                    _events.send(AddStringModelEvent.UniqueConstraintFailed)
                } finally {
                    saveDisabledOverride.value = false
                }
            }
        }
    }
}