package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.os.Parcelable
import android.util.SparseArray
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.core.widget.EnumArrayAdapter
import com.gitlab.thomasreader.tennisorganiser.data.SetType
import com.gitlab.thomasreader.tennisorganiser.databinding.ScoreDialogBinding
import com.gitlab.thomasreader.tennisorganiser.domain.activity.Set
import com.gitlab.thomasreader.tennisorganiser.domain.activity.SetRule
import com.gitlab.thomasreader.tennisorganiser.domain.activity.stringResource

class SetScoreDialog: DialogFragment() {
    var onClickListener: ((SetRule, Set) -> Unit)? = null
    private var binding: ScoreDialogBinding? = null

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val sparseArray = SparseArray<Parcelable>()
        binding!!.root.saveHierarchyState(sparseArray)
        outState.putSparseParcelableArray("sparse_array", sparseArray)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val setNumber = arguments?.getInt("set_number") ?: 1
        val setRule = arguments?.getParcelable<SetRule>("set_rule")
        val setScore = arguments?.getParcelable<Set>("set_score")

        binding = ScoreDialogBinding.inflate(layoutInflater)
        savedInstanceState?.let { bundle ->
            bundle.getSparseParcelableArray<Parcelable>("sparse_array")?.let {
                binding!!.root.restoreHierarchyState(it)
            }
        }

        when (setRule) {
            is SetRule.AdvantageSet -> {
                binding!!.gamesFirstToInput.setText(setRule.gamesFirstTo)
                binding!!.gamesWinByInput.setText(setRule.gamesWinBy)
                binding!!.setTypeInput.setText(setRule.setType.stringResource)
            }
            is SetRule.TieBreak -> {
                binding!!.tieBreakFirstToInput.setText(setRule.tieBreakFirstTo)
                binding!!.tieBreakWinByInput.setText(setRule.tieBreakWinBy)
                binding!!.setTypeInput.setText(setRule.setType.stringResource)
            }
            is SetRule.TieBreakSet -> {
                binding!!.gamesFirstToInput.setText(setRule.gamesFirstTo)
                binding!!.gamesWinByInput.setText(setRule.gamesWinBy)
                binding!!.gamesTieBreakStartsInput.setText(setRule.gameTieBreakCommences)
                binding!!.tieBreakFirstToInput.setText(setRule.tieBreakFirstTo)
                binding!!.tieBreakWinByInput.setText(setRule.tieBreakWinBy)
                binding!!.setTypeInput.setText(setRule.setType.stringResource)
            }
            null -> {
                binding!!.gamesFirstToInput.setText("6")
                binding!!.gamesWinByInput.setText("2")
                binding!!.gamesTieBreakStartsInput.setText("6")
                binding!!.tieBreakFirstToInput.setText("7")
                binding!!.tieBreakWinByInput.setText("2")
                binding!!.setTypeInput.setText(SetType.TIEBREAK_SET.stringResource)
            }
        }

        val setTypeAdapter = EnumArrayAdapter<SetType>(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            SetType.values().toList()
        ) {
            getString(it.stringResource)
        }

        binding!!.setTypeInput.setAdapter(
            setTypeAdapter
        )


        binding!!.setTypeInput.doOnTextChanged { text, _, _, _ ->
            val setType = text?.let {
                (binding!!.setTypeInput.adapter as EnumArrayAdapter<SetType>)
                    .enumFromCharSequenceValue(it.toString())
            }
            when (setType) {
                SetType.TIEBREAK -> {
                    binding!!.gamesFirstToLayout.gone()
                    binding!!.gamesWinByLayout.gone()
                    binding!!.gamesTieBreakStartsLayout.gone()
                    binding!!.gamesLayout.gone()
                    binding!!.opponentGamesLayout.gone()
                    binding!!.pointsLayout.visible()
                    binding!!.opponentPointsLayout.visible()
                    binding!!.tieBreakFirstToLayout.visible()
                    binding!!.tieBreakWinByLayout.visible()
                }
                SetType.TIEBREAK_SET -> {
                    binding!!.gamesFirstToLayout.visible()
                    binding!!.gamesWinByLayout.visible()
                    binding!!.gamesTieBreakStartsLayout.visible()
                    binding!!.gamesLayout.visible()
                    binding!!.opponentGamesLayout.visible()
                    binding!!.pointsLayout.visible()
                    binding!!.opponentPointsLayout.visible()
                    binding!!.tieBreakFirstToLayout.visible()
                    binding!!.tieBreakWinByLayout.visible()
                }
                SetType.ADVANTAGE_SET -> {
                    binding!!.gamesFirstToLayout.visible()
                    binding!!.gamesWinByLayout.visible()
                    binding!!.gamesTieBreakStartsLayout.gone()
                    binding!!.gamesLayout.visible()
                    binding!!.opponentGamesLayout.visible()
                    binding!!.pointsLayout.gone()
                    binding!!.opponentPointsLayout.gone()
                    binding!!.tieBreakFirstToLayout.gone()
                    binding!!.tieBreakWinByLayout.gone()
                }
                null -> Unit
            }
        }

        return AlertDialog.Builder(requireContext())
            .setTitle("Set $setNumber")
            .setView(binding!!.root)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                if (onClickListener != null) {
                    val setType = (binding!!.setTypeInput.adapter as EnumArrayAdapter<SetType>)
                        .enumFromCharSequenceValue(binding!!.setTypeInput.text.toString())
                    val setRule = when (setType) {
                        SetType.TIEBREAK -> SetRule.TieBreak(
                            tieBreakFirstTo = binding!!.tieBreakFirstToInput.text!!.toString()
                                .toInt(),
                            tieBreakWinBy = binding!!.tieBreakWinByInput.text!!.toString().toInt()
                        )
                        SetType.TIEBREAK_SET -> SetRule.TieBreakSet(
                            gamesFirstTo = binding!!.gamesFirstToInput.text!!.toString().toInt(),
                            gamesWinBy = binding!!.gamesWinByInput.text!!.toString().toInt(),
                            gameTieBreakCommences = binding!!.gamesTieBreakStartsInput.text!!.toString()
                                .toInt(),
                            tieBreakFirstTo = binding!!.tieBreakFirstToInput.text!!.toString()
                                .toInt(),
                            tieBreakWinBy = binding!!.tieBreakWinByInput.text!!.toString().toInt()
                        )
                        SetType.ADVANTAGE_SET -> SetRule.AdvantageSet(
                            gamesFirstTo = binding!!.gamesFirstToInput.text!!.toString().toInt(),
                            gamesWinBy = binding!!.gamesWinByInput.text!!.toString().toInt()
                        )
                        null -> TODO()
                    }
                    val setScore = when (setType) {
                        SetType.TIEBREAK -> Set.TieBreak(
                            points = binding!!.pointsInput.text!!.toString().toInt(),
                            opponentPoints = binding!!.opponentPointsInput.text!!.toString().toInt()
                        )
                        SetType.TIEBREAK_SET -> Set.TieBreakSet(
                            games = binding!!.gamesInput.text!!.toString().toInt(),
                            opponentGames = binding!!.opponentGamesInput.text!!.toString().toInt(),
                            points = binding?.pointsInput?.text?.toString()?.toIntOrNull(),
                            opponentPoints = binding?.opponentPointsInput?.text?.toString()
                                ?.toIntOrNull()
                        )
                        SetType.ADVANTAGE_SET -> Set.Advantage(
                            games = binding!!.gamesInput.text!!.toString().toInt(),
                            opponentGames = binding!!.opponentGamesInput.text!!.toString().toInt()
                        )
                    }
                    onClickListener?.let { it(setRule, setScore) }
                }
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}

fun SetScoreDialog(setNumber: Int, setRule: SetRule?, setScore: Set?): SetScoreDialog {
    return SetScoreDialog().apply {
        arguments = Bundle().apply {
            putInt("set_number", setNumber)
            putParcelable("set_rule", setRule)
            putParcelable("set_score", setScore)
        }
    }
}