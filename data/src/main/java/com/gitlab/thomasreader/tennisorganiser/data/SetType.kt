package com.gitlab.thomasreader.tennisorganiser.data

enum class SetType {
    TIEBREAK,
    TIEBREAK_SET,
    ADVANTAGE_SET
}