package com.gitlab.thomasreader.tennisorganiser.screen.string.stringlist

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class StringListScreen: AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = StringListFragment()
}