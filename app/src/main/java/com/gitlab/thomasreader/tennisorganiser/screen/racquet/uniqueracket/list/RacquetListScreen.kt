package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class RacquetListScreen : AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = RacquetListFragment()
}