package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.edit

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class EditRacquetScreen(
    val racquetId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = EditRacquetFragment()
    override fun hideBottomNavigation(): Boolean = true
}