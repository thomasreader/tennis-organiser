package com.gitlab.thomasreader.tennisorganiser.core.savestate

import android.os.Parcelable
import com.zhuinden.simplestack.Bundleable
import com.zhuinden.statebundle.StateBundle
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.io.Serializable

fun main() {
    val one = Ok(StateBundle())
    val two = Ok(StateBundle().apply {
        putSerializable("brand", "Yonex")
        putSerializable("model", "VCore 100")
    })
    val three = Ok(StateBundle().apply {
        putSerializable("model", "VCore 98")
    })
    val four = Ok(StateBundle().apply {
        putSerializable("brand", "Yonex")
    })

    var job: Job? = null
    job = CoroutineScope(SupervisorJob() + Dispatchers.Default).launch {
        launch {
            one.brand.collect {
                println("One: $it")
            }
        }
        launch {
            one.model.collect {
                println("One: $it")
            }
        }
        launch {
            two.brand.collect {
                println("Two: $it")
            }
        }
        launch {
            two.model.collect {
                println("Two: $it")
            }
        }
        launch {
            three.brand.collect {
                println("Three: $it")
            }
        }
        launch {
            three.model.collect {
                println("Three: $it")
            }
        }
        launch {
            four.brand.collect {
                println("Four: $it")
            }
        }
        launch {
            four.model.collect {
                println("Four: $it")
            }
        }
        launch {
            one.brand.value = "Head"
            one.model.value = "LiquidMetal Fire"
            delay(1000)
            two.brand.value = "Head"
            two.model.value = "LiquidMetal Fire"
            delay(1000)
            three.brand.value = "Head"
            three.model.value = "LiquidMetal Fire"
            delay(1000)
            four.brand.value = "Head"
            four.model.value = "LiquidMetal Fire"
            delay(1000)
            job?.cancel()
            job = null
        }
    }

    while (job != null) {
        Thread.sleep(2000)
    }
}

class Ok(
    private val bundle: StateBundle
) {
    val brand = bundle.getStateFlow<String>("brand")
    val model = bundle.getStateFlow<String>("model", "VCore 95")
    val ok = bundle.getStateFlow<String?>("ok", null)

    val brand2 = bundle.getStateFlowMapped("brand2", StateBundle::putString)
    val model2 = bundle.getStateFlowMapped("model", "Vcore 95", StateBundle::putString)
    val ok2: MutableStateFlow<Boolean?> = bundle.getStateFlowMapped("ok2", null, StateBundle::putSerializable)
}

private class BundleStateFlow<T>(
    private val key: String,
    private val stateBundle: StateBundle?,
    private val internalFlow: MutableStateFlow<T>
): MutableStateFlow<T> by internalFlow {
    override var value: T
        get() = internalFlow.value
        set(value) {
            if (value is Parcelable) {
                stateBundle?.putParcelable(key, value)
            } else if (value is Serializable) {
                stateBundle?.putSerializable(key, value)
            }
            internalFlow.value = value
        }
}

fun <T> StateBundle.getStateFlow(key: String): MutableStateFlow<T?> {
    return getStateFlow(key, null)
}

fun <T> StateBundle.getStateFlow(key: String, initialValue: T): MutableStateFlow<T> {
    var value: T = try {
        this[key]?.let { it as T? } ?: initialValue
    } catch (e: Exception) {
        initialValue
    }
    val flow = MutableStateFlow(value)
    return BundleStateFlow(key, this, flow)
}

private class BundleStateFlowMapped<T>(
    private val key: String,
    private val stateBundle: StateBundle?,
    private val inMapper: StateBundle.(key: String, value: T) -> StateBundle,
    private val internalFlow: MutableStateFlow<T>
): MutableStateFlow<T> by internalFlow {
    override var value: T
        get() = internalFlow.value
        set(value) {
            stateBundle?.let {
                inMapper(it, key, value)
            }
            internalFlow.value = value
        }
}

fun <T> StateBundle.getStateFlowMapped(key: String, mapper: StateBundle.(key: String, value: T?) -> StateBundle): MutableStateFlow<T?> {
    return getStateFlowMapped(key, null, mapper)
}

fun <T> StateBundle.getStateFlowMapped(key: String, initialValue: T, mapper: StateBundle.(key: String, value: T) -> StateBundle): MutableStateFlow<T> {
    var value: T = try {
        this[key]?.let { it as T? } ?: initialValue
    } catch (e: Exception) {
        initialValue
    }
    val flow = MutableStateFlow(value)
    return BundleStateFlowMapped(key, this, mapper, flow)
}

abstract class BundleableViewModel: Bundleable {
    protected lateinit var bundle: StateBundle

    override fun toBundle(): StateBundle = this.bundle

    override fun fromBundle(bundle: StateBundle?) {
        this.bundle = bundle ?: StateBundle()
        this.onBindBundle(this.bundle)
    }

    abstract fun onBindBundle(bundle: StateBundle)
}

class Whatever: BundleableViewModel() {
    val brand = bundle.getStateFlow<String>("brand")
    val model = bundle.getStateFlow<String>("model", "VCore 95")
    val ok = bundle.getStateFlow<String?>("ok", null)

    val brand2 = bundle.getStateFlowMapped("brand2", StateBundle::putString)
    val model2 = bundle.getStateFlowMapped("model", "Vcore 95", StateBundle::putString)
    val ok2: MutableStateFlow<Boolean?> = bundle.getStateFlowMapped("ok2", null, StateBundle::putSerializable)

    val brandError = brand.map {
        it?.let { (it != "Yonex") }
    }

    val errors = combine(brand, model) { _brand, _model ->
        (_brand?.let { it != "Yonex" } ?: false) to (_model != "VCore 95")
    }

    lateinit var b: MutableStateFlow<String>
        private set

    override fun onBindBundle(bundle: StateBundle) {
        b = bundle.getStateFlow("bob", "Hi")
    }
}