package com.gitlab.thomasreader.tennisorganiser.screen.settings

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class SettingsScreen: AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = SettingsFragment()
}