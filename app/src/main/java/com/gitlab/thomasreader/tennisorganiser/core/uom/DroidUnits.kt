package com.gitlab.thomasreader.tennisorganiser.core.uom

import com.gitlab.thomasreader.kuantity.quantity.*
import com.gitlab.thomasreader.kuantity.unit.prefix.MetricPrefix.*
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.data.droidUnit


interface DroidUnits {
    object LengthUnits {
        @JvmField
        val CENTIMETRE = droidUnit<Length>(
            name = "Centimetre",
            nameId = R.string.centimetre,
            symbol = "cm",
            converter = CENTI.converter
        )

        @JvmField
        val MILLIMETRE = droidUnit<Length>(
            name = "Millimetre",
            nameId = R.string.millimetre,
            symbol = "mm",
            converter = MILLI.converter
        )

        @JvmField
        val INCH = droidUnit<Length>(
            name = "Inch",
            nameId = R.string.inch,
            symbol = "in",
            factor = 0.0254
        )

        @JvmField
        val FEET = droidUnit<Length>(
            name = "Feet",
            nameId = R.string.feet,
            symbol = "ft",
            factor = 0.3048
        )

        @JvmField
        val BALANCE_POINT = droidUnit<Length>(
            name = "Balance point",
            nameId = R.string.balance_point_unit,
            symbol = "pt",
            factor = 0.003175
        )

        @JvmField
        val GRIP_SIZE = droidUnit<Length>(
            name = "Grip size",
            nameId = R.string.grip_size_unit,
            symbol = "L",
            factor = 0.003175
        )

        fun toBalancePoint(racquetLength: Quantity<Length>, balancePoint: Quantity<Length>): Int {
            val midPoint = racquetLength / 2.0
            // head heavy +ve, even balance 0, head light -ve
            val offset = balancePoint - midPoint
            return offset.convertToInt(BALANCE_POINT)
        }

        fun fromBalancePoint(racquetLength: Quantity<Length>, balancePoint: Int): Quantity<Length> {
            val offset = BALANCE_POINT * balancePoint
            val midPoint = racquetLength / 2.0
            return midPoint + offset
        }

        fun toGripSize(quantity: Quantity<Length>): Int {
            // grip size starts at 4 inches (0.1016 metres)
            val sizeWithOffset = quantity - Quantity(0.1016)
            // grip size increments every 1/8 inch
            val gripSize = sizeWithOffset.value % 0.003175
            return gripSize.toInt()
        }

        fun fromGripSize(gripSize: Int): Quantity<Length> {
            val gripSizeM = 0.003175 * gripSize
            return Quantity<Length>(gripSizeM) + Quantity(0.1016)
        }

        fun getUnit(name: String): DroidUnit<Length> {
            return when (name) {
                CENTIMETRE.name -> CENTIMETRE
                MILLIMETRE.name -> MILLIMETRE
                INCH.name -> INCH
                FEET.name -> FEET
                BALANCE_POINT.name -> BALANCE_POINT
                GRIP_SIZE.name -> GRIP_SIZE
                else -> throw NullPointerException()
            }
        }
    }

    object MassUnits {
        @JvmField
        val KILOGRAM = droidUnit<Mass>(
            name = "Kilogram",
            nameId = R.string.kilogram,
            symbol = "kg",
            factor = 1.0
        )

        @JvmField
        val GRAM = droidUnit<Mass>(
            name = "Gram",
            nameId = R.string.gram,
            symbol = "g",
            factor = 0.001
        )

        @JvmField
        val POUND = droidUnit<Mass>(
            name = "Pound",
            nameId = R.string.pound,
            symbol = "lb",
            factor = 0.45359237
        )

        @JvmField
        val OUNCE = droidUnit<Mass>(
            name = "Ounce",
            nameId = R.string.ounce,
            symbol = "oz",
            factor = 0.028349523125
        )

        fun getUnit(name: String): DroidUnit<Mass> {
            return when (name) {
                KILOGRAM.name -> KILOGRAM
                GRAM.name -> GRAM
                POUND.name -> POUND
                OUNCE.name -> OUNCE
                else -> throw NullPointerException()
            }
        }
    }

    object AreaUnits {
        @JvmField
        val SQUARE_CENTIMETRE = droidUnit<Area>(
            name = "Square centimetre",
            nameId = R.string.square_centimetre,
            symbol = "cm²",
            converter = CENTI.converter.pow(2)
        )

        @JvmField
        val SQUARE_INCH = droidUnit<Area>(
            name = "Square inch",
            nameId = R.string.square_inch,
            symbol = "in²",
            factor = 0.00064516
        )

        fun getUnit(name: String): DroidUnit<Area> {
            return when (name) {
                SQUARE_CENTIMETRE.name -> SQUARE_CENTIMETRE
                SQUARE_INCH.name -> SQUARE_INCH
                else -> throw NullPointerException()
            }
        }
    }
}