package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AbstractSavedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.navigation.nonNullSavedState
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedState
import com.gitlab.thomasreader.tennisorganiser.data.*
import com.gitlab.thomasreader.tennisorganiser.domain.activity.*
import com.gitlab.thomasreader.tennisorganiser.domain.activity.Set
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat
import java.time.*
import java.time.temporal.ChronoUnit

sealed interface UpsertActivityEvent {
    object Saved : UpsertActivityEvent
}

data class UpsertActivityState(
    val saveEnabled: Boolean,
    val names: List<String>,
    val venues: List<String>,
    val racquets: List<Pair<Long, DroidTextProvider>>,
    val tags: List<String>,
    val title: String?,
    val description: String?,
    val courtSurface: CourtSurface?,
    val venue: String?,
    val date: DateTimeProvider?,
    val dateError: Boolean,
    val time: TimeProvider?,
    val timeError: Boolean,
    val duration: ElapsedTimeProvider?,
    val durationError: Boolean,
    val racquet: Pair<Long, DroidTextProvider>?,
    val selectedTags: List<String>,
    val isMatch: Boolean,
    val matchType: MatchType?,
    val result: MatchResult?,
    val resultError: Boolean,
    val seed: String?,
    val partnerName: String?,
    val partnerRating: String?,
    val opponentSeed: String?,
    val opponentOneName: String?,
    val opponentOneRating: String?,
    val opponentTwoName: String?,
    val opponentTwoRating: String?,
    val numSets: Int,
    val playerName: DroidTextProvider,
    val setOneRule: SetRule?,
    val setTwoRule: SetRule?,
    val setThreeRule: SetRule?,
    val setFourRule: SetRule?,
    val setFiveRule: SetRule?,
    val setOneScore: Set?,
    val setTwoScore: Set?,
    val setThreeScore: Set?,
    val setFourScore: Set?,
    val setFiveScore: Set?,
    val matchTypeError: Boolean,
    val setOneError: Boolean,
    val setTwoError: Boolean,
    val setThreeError: Boolean,
    val setFourError: Boolean,
    val setFiveError: Boolean,
)

private data class UpsertActivitySavedValues(
    val title: String?,
    val description: String?,
    val courtSurface: CourtSurface?,
    val venue: String?,
    val date: Long?,
    val time: Pair<Int, Int>?,
    val duration: Long?,
    val racquetId: Long?,
    val tags: List<String>,
    val isMatch: Boolean,
    val matchType: MatchType?,
    val result: MatchResult?,
    val seed: Int?,
    val partnerName: String?,
    val partnerRating: String?,
    val opponentSeed: Int?,
    val opponentOneName: String?,
    val opponentOneRating: String?,
    val opponentTwoName: String?,
    val opponentTwoRating: String?,
    val numSets: Int,
    val setOneRule: SetRule?,
    val setTwoRule: SetRule?,
    val setThreeRule: SetRule?,
    val setFourRule: SetRule?,
    val setFiveRule: SetRule?,
    val setOneScore: Set?,
    val setTwoScore: Set?,
    val setThreeScore: Set?,
    val setFourScore: Set?,
    val setFiveScore: Set?
)

class UpsertActivityViewModel(
    _key: DefaultFragmentKey,
    database: TennisDatabase,
    dataStore: DataStore<Preferences>,
    savedStateHandle: SavedStateHandle
) : AbstractSavedViewModel<UpsertActivityScreen>(_key, database, dataStore, savedStateHandle) {
    private val _events = Channel<UpsertActivityEvent>(Channel.BUFFERED)
    val events: Flow<UpsertActivityEvent> = _events.receiveAsFlow()

    private var isInit by nonNullSavedState("initiated", !key.isEdit)
    private var title: String? by savedState("title")
    private var description: String? by savedState("description")
    private var courtSurface: CourtSurface? by savedState("court_surface")
    private var venue: String? by savedState("venue")
    private var date: Long? by savedState("date")
    private var time: Pair<Int, Int>? by savedState("time")
    private var duration: Long? by savedState("duration")
    private var racquetId: Long? by savedState("racquet_id")
    private var isMatch: Boolean by nonNullSavedState("is_match", false)
    private var matchType: MatchType? by savedState("match_type", MatchType.DOUBLES_MEN)
    private var result: MatchResult? by savedState("result", MatchResult.WIN)
    private var seed: Int? by savedState("seed")
    private var partnerName: String? by savedState("partner_name")
    private var partnerRating: String? by savedState("partner_rating")
    private var opponentSeed: Int? by savedState("opponent_seed")
    private var opponentOneName: String? by savedState("opponent_one_name")
    private var opponentOneRating: String? by savedState("opponent_one_rating")
    private var opponentTwoName: String? by savedState("opponent_two_name")
    private var opponentTwoRating: String? by savedState("opponent_two_rating")
    private var tags: List<String> by nonNullSavedState("tags", listOf())
    private var numSets: Int by nonNullSavedState("num_sets", 1)
    private var setOneRule: SetRule? by savedState("set_one_rule")
    private var setTwoRule: SetRule? by savedState("set_two_rule")
    private var setThreeRule: SetRule? by savedState("set_three_rule")
    private var setFourRule: SetRule? by savedState("set_four_rule")
    private var setFiveRule: SetRule? by savedState("set_five_rule")
    private var setOneScore: Set? by savedState("set_one_score")
    private var setTwoScore: Set? by savedState("set_two_score")
    private var setThreeScore: Set? by savedState("set_three_score")
    private var setFourScore: Set? by savedState("set_four_score")
    private var setFiveScore: Set? by savedState("set_five_score")

    private val isSaving: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val savedValues = MutableStateFlow(this.generateValues)

    private val generateValues
        get(): UpsertActivitySavedValues {
            return UpsertActivitySavedValues(
                title = this.title,
                description = this.description,
                courtSurface = this.courtSurface,
                venue = this.venue,
                date = this.date,
                time = this.time,
                duration = this.duration,
                racquetId = this.racquetId,
                tags = this.tags,
                isMatch = this.isMatch,
                matchType = this.matchType,
                result = this.result,
                seed = this.seed,
                partnerName = this.partnerName,
                partnerRating = this.partnerRating,
                opponentSeed = this.opponentSeed,
                opponentOneName = this.opponentOneName,
                opponentOneRating = this.opponentOneRating,
                opponentTwoName = this.opponentTwoName,
                opponentTwoRating = this.opponentTwoRating,
                numSets = this.numSets,
                setOneRule = this.setOneRule,
                setTwoRule = this.setTwoRule,
                setThreeRule = this.setThreeRule,
                setFourRule = this.setFourRule,
                setFiveRule = this.setFiveRule,
                setOneScore = this.setOneScore,
                setTwoScore = this.setTwoScore,
                setThreeScore = this.setThreeScore,
                setFourScore = this.setFourScore,
                setFiveScore = this.setFiveScore
            )
        }

    private fun updateValues() {
        this.savedValues.value = this.generateValues
    }

    init {
        fetchInitialValues()
    }

    private fun fetchInitialValues() {
        if (!isInit && key.isEdit) {
            isInit = true
            viewModelScope.launch(Dispatchers.IO) {
                val activity = database.activityQueries.selectActivityById(key.activityId!!)
                    .executeAsOne()

                val racquet = activity.racquet_id?.let { racquetId ->
                    database.racquetQueries.selectRacquetById(racquetId).executeAsOne()
                }

                val tagsList = database.activityQueries.selectTagsByActivityId(key.activityId)
                    .executeAsList()

                val instant = Instant.ofEpochMilli(activity.started_at)
                val ldt = LocalDateTime.ofInstant(instant, ZoneOffset.systemDefault())

                val setRules = database.matchSetRuleQueries.selectSetRulesByIdAsc(key.activityId)
                    .executeAsList()

                val setResults =
                    database.matchSetResultQueries.selectSetResultsById(key.activityId)
                        .executeAsList()

                title = activity.title
                description = activity.description
                courtSurface = activity.surface
                venue = activity.venue
                date = ldt.toLocalDate().toEpochDay() * 86400000L
                time = ldt.toLocalTime().let { it.hour to it.minute }
                duration = activity.duration
                racquetId = racquet?.id
                tags = tagsList
                isMatch = activity.is_match
                matchType = activity.match_type
                result = activity.result
                seed = activity.seed
                partnerName = activity.partner_name
                partnerRating = activity.partner_rating
                opponentSeed = activity.opponent_seed
                opponentOneName = activity.opponent_one_name
                opponentOneRating = activity.opponent_one_rating
                opponentTwoName = activity.opponent_two_name
                opponentTwoRating = activity.opponent_two_rating
                setRules.forEachIndexed { index, matchSetRule ->
                    when (index) {
                        0 -> setOneRule = matchSetRule.toSetRule
                        1 -> setTwoRule = matchSetRule.toSetRule
                        2 -> setThreeRule = matchSetRule.toSetRule
                        3 -> setFourRule = matchSetRule.toSetRule
                        4 -> setFiveRule = matchSetRule.toSetRule
                    }
                }
                setResults
                    .map { it.toSet }
                    .forEachIndexed { index, matchSetResult ->
                    when (index) {
                        0 -> setOneScore = matchSetResult
                        1 -> setTwoScore = matchSetResult
                        2 -> setThreeScore = matchSetResult
                        3 -> setFourScore = matchSetResult
                        4 -> setFiveScore = matchSetResult
                    }
                }
                updateValues()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    val uiState = combine(
        savedValues,
        database.miscQueries.selectActivityVenues()
            .asFlow()
            .mapToList(Dispatchers.IO)
            .map { it.map { it.venue }.filterNotNull() },
        database.racquetQueries.selectSimpleRacquetsAsc()
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.playerQueries.selectPlayerNames()
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.activityQueries.selectActivityTags()
            .asFlow()
            .mapToList(Dispatchers.IO),
        isSaving,
        dataStore.data
    ) { args ->
        val savedValues = args[0] as UpsertActivitySavedValues
        val venues = args[1] as List<String>
        val racquets = args[2] as List<SelectSimpleRacquetsAsc>
        val playerNames = args[3] as List<String>
        val filteredTags = (args[4] as List<String>) - savedValues.tags
        val saving = args[5] as Boolean
        val preferences = args[6] as Preferences

        val dateErr = savedValues.date == null
        val timeErr = savedValues.time == null
        val durationErr = savedValues.duration == null || savedValues.duration == 0L

        val matchTypeErr = savedValues.matchType == null
        val resultErr = savedValues.result == null
        val setOneErr = savedValues.setOneRule == null || savedValues.setOneScore == null
        val setTwoErr = savedValues.numSets > 1 && (savedValues.setTwoRule == null || savedValues.setTwoScore == null)
        val setThreeErr = savedValues.numSets > 2 && (savedValues.setThreeRule == null || savedValues.setThreeScore == null)
        val setFourErr = savedValues.numSets > 3 && (savedValues.setFourRule == null || savedValues.setFourScore == null)
        val setFiveErr = savedValues.numSets > 4 && (savedValues.setFiveRule == null || savedValues.setFiveScore == null)

        val saveDisabled = saving || dateErr || timeErr || durationErr ||
                (savedValues.isMatch && (matchTypeErr || resultErr || setOneErr || setTwoErr || setThreeErr || setFourErr || setFiveErr))
        val racquet = racquets.associateBy { it.id }[savedValues.racquetId]
        val racquetsUi = racquets.map {
            it.id to DroidTextProvider.stringId(
                R.string.racquet_identifier_brand_model,
                it.identifier,
                it.brand,
                it.model
            )
        }
        val racquetUi = racquet?.let {
            it.id to DroidTextProvider.stringId(
                R.string.racquet_identifier_brand_model,
                it.identifier,
                it.brand,
                it.model
            )
        }
        val playerName = preferences.getMapped(DataStoreKeys.USERNAME)

        with(savedValues) {
            return@combine UpsertActivityState(
                saveEnabled = !saveDisabled,
                names = playerNames,
                venues = venues,
                racquets = racquetsUi,
                tags = filteredTags,
                title = title,
                description = description,
                courtSurface = courtSurface,
                venue = venue,
                date = date?.let { DateTimeProvider(it, 0) },
                dateError = dateErr,
                time = time?.let { TimeProvider(it.first, it.second) },
                timeError = timeErr,
                duration = duration?.let { ElapsedTimeProvider(it) },
                durationError = durationErr,
                racquet = racquetUi,
                selectedTags = tags,
                isMatch = isMatch ?: false,
                matchType = matchType,
                result = result,
                resultError = resultErr,
                seed = seed?.toString(),
                partnerName = partnerName,
                partnerRating = partnerRating,
                opponentSeed = opponentSeed?.toString(),
                opponentOneName = opponentOneName,
                opponentOneRating = opponentOneRating,
                opponentTwoName = opponentTwoName,
                opponentTwoRating = opponentTwoRating,
                numSets = numSets,
                playerName = playerName,
                setOneRule = setOneRule,
                setTwoRule = setTwoRule,
                setThreeRule = setThreeRule,
                setFourRule = setFourRule,
                setFiveRule = setFiveRule,
                setOneScore = setOneScore,
                setTwoScore = setTwoScore,
                setThreeScore = setThreeScore,
                setFourScore = setFourScore,
                setFiveScore = setFiveScore,
                matchTypeError = matchTypeErr,
                setOneError = setOneErr,
                setTwoError = setTwoErr,
                setThreeError = setThreeErr,
                setFourError = setFourErr,
                setFiveError = setFiveErr
            )
        }
    }
        .flowOn(Dispatchers.Default)
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun updateTitle(title: String?) {
        this.title = title
        this.updateValues()
    }

    fun updateDescription(description: String?) {
        this.description = description
        this.updateValues()
    }

    fun updateSurface(courtSurface: CourtSurface?) {
        this.courtSurface = courtSurface
        this.updateValues()
    }

    fun updateVenue(venue: String?) {
        this.venue = venue
        this.updateValues()
    }

    fun updateDate(date: Long?) {
        this.date = date
        this.updateValues()
    }

    fun updateTime(time: Pair<Int, Int>?) {
        this.time = time
        this.updateValues()
    }

    fun updateDuration(seconds: Long?) {
        this.duration = seconds
        this.updateValues()
    }

    fun updateRacquetId(racquetId: Long?) {
        this.racquetId = racquetId
        this.updateValues()
    }

    fun updateIsMatch(isMatch: Boolean) {
        this.isMatch = isMatch
        this.updateValues()
    }

    fun updateMatchType(matchType: MatchType?) {
        this.matchType = matchType
        this.updateValues()
    }

    fun updateResult(result: MatchResult?) {
        this.result = result
        this.updateValues()
    }

    fun updateSeed(seed: Int?) {
        this.seed = seed
        this.updateValues()
    }

    fun updatePartnerName(name: String?) {
        this.partnerName = name
        this.updateValues()
    }

    fun updatePartnerRating(rating: String?) {
        this.partnerRating = rating
        this.updateValues()
    }

    fun updateOpponentSeed(seed: Int?) {
        this.opponentSeed = seed
        this.updateValues()
    }

    fun updateOpponentOneName(name: String?) {
        this.opponentOneName = name
        this.updateValues()
    }

    fun updateOpponentOneRating(rating: String?) {
        this.opponentOneRating = rating
        this.updateValues()
    }

    fun updateOpponentTwoName(name: String?) {
        this.opponentTwoName = name
        this.updateValues()
    }

    fun updateOpponentTwoRating(rating: String?) {
        this.opponentTwoRating = rating
        this.updateValues()
    }

    fun updateNumSets(sets: Int) {
        this.numSets = sets
        this.updateValues()
    }

    fun updateSetOneRule(setRule: SetRule) {
        this.setOneRule = setRule
        this.updateValues()
    }

    fun updateSetTwoRule(setRule: SetRule) {
        this.setTwoRule = setRule
        this.updateValues()
    }

    fun updateSetThreeRule(setRule: SetRule) {
        this.setThreeRule = setRule
        this.updateValues()
    }

    fun updateSetFourRule(setRule: SetRule) {
        this.setFourRule = setRule
        this.updateValues()
    }

    fun updateSetFiveRule(setRule: SetRule) {
        this.setFiveRule = setRule
        this.updateValues()
    }

    fun updateSetOneScore(setScore: Set) {
        this.setOneScore = setScore
        this.updateValues()
    }

    fun updateSetTwoScore(setScore: Set) {
        this.setTwoScore = setScore
        this.updateValues()
    }

    fun updateSetThreeScore(setScore: Set) {
        this.setThreeScore = setScore
        this.updateValues()
    }

    fun updateSetFourScore(setScore: Set) {
        this.setFourScore = setScore
        this.updateValues()
    }

    fun updateSetFiveScore(setScore: Set) {
        this.setFiveScore = setScore
        this.updateValues()
    }

    fun addTag(tag: String?): Boolean {
        if (!tag.isNullOrBlank()) {
            val currentTags = this.tags
            if (!currentTags.contains(tag)) {
                this.tags = MutableList(currentTags.size + 1) { ix ->
                    if (ix == currentTags.size) tag else currentTags[ix]
                }
                this.updateValues()
                return true
            }
        }
        return false
    }

    fun removeTag(tag: String) {
        val currentTags = this.tags
        val ixRemove = currentTags.indexOf(tag)
        if (ixRemove >= 0) {
            this.tags = MutableList(currentTags.size - 1) { ix ->
                val accessor = if (ixRemove <= ix) ix + 1 else ix
                currentTags[accessor]
            }
            this.updateValues()
        }
    }

    private fun validState(
        savedValues: UpsertActivitySavedValues
    ): Boolean {
        val validActivity = savedValues.date != null && savedValues.time != null && savedValues.duration != null && savedValues.duration > 0
        val matchTypeErr = savedValues.matchType == null
        val resultErr = savedValues.result == null
        val setOneErr = savedValues.setOneRule == null || savedValues.setOneScore == null
        val setTwoErr = savedValues.numSets > 1 && (savedValues.setTwoRule == null || savedValues.setTwoScore == null)
        val setThreeErr = savedValues.numSets > 2 && (savedValues.setThreeRule == null || savedValues.setThreeScore == null)
        val setFourErr = savedValues.numSets > 3 && (savedValues.setFourRule == null || savedValues.setFourScore == null)
        val setFiveErr = savedValues.numSets > 4 && (savedValues.setFiveRule == null || savedValues.setFiveScore == null)
        val matchError = matchTypeErr || resultErr || setOneErr || setTwoErr || setThreeErr || setFourErr || setFiveErr
        return validActivity && !savedValues.isMatch || savedValues.isMatch && !matchError
    }

    fun saveActivity(
        defaultActivityNames: Map<Int, String>,
        defaultOpponentName: String,
        defaultPartnerName: String
    ) {
        val values = savedValues.value
        if (validState(values)) {
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                with(values) {
                    val hour = time!!.first
                    val title = this.title?.ifBlank { null } ?: defaultActivityNames[when (isMatch) {
                        true -> {
                            when {
                                hour < 6 || hour > 22 -> R.string.night_match
                                hour < 12 -> R.string.morning_match
                                hour < 18 -> R.string.afternoon_match
                                else -> R.string.evening_match
                            }
                        }
                        false -> {
                            when {
                                hour < 6 || hour > 22 -> R.string.night_activity
                                hour < 12 -> R.string.morning_activity
                                hour < 18 -> R.string.afternoon_activity
                                else -> R.string.evening_activity
                            }
                        }
                    }]!!

                    val defOpponentNameTwo = when (isMatch && matchType?.isSingles == true) {
                        true -> null
                        false -> defaultOpponentName
                    }
                    val defPartnerName = when (isMatch && matchType?.isSingles == true) {
                        true -> null
                        false -> defaultPartnerName
                    }
                    val startedAt =
                        Instant.ofEpochMilli(date!!).plus(time!!.first.toLong(), ChronoUnit.HOURS)
                            .plus(time.second.toLong(), ChronoUnit.MINUTES)
                    val ldt = LocalDateTime.ofInstant(startedAt, ZoneOffset.UTC)
                    val zdt = ldt.atZone(ZoneId.systemDefault())
                    val zonedInstant = zdt.toInstant().toEpochMilli()
                    database.transaction {
                        val activityId: Long
                        if (key.isEdit) {
                            database.activityQueries.updateActivity(
                                id = key.activityId!!,
                                title = title,
                                description = description?.ifBlank { null },
                                surface_id = courtSurface,
                                venue = venue?.ifBlank { null },
                                started_at = zonedInstant,
                                duration = duration!!,
                                racquet_id = racquetId,
                                is_match = isMatch,
                                match_type = matchType,
                                seed = seed,
                                opponent_seed = opponentSeed,
                                result = result,
                                user_rating = null,
                                opponent_one_name = opponentOneName?.ifBlank { defaultOpponentName },
                                opponent_one_rating = opponentOneRating?.ifBlank { null },
                                partner_name = partnerName?.ifBlank { defPartnerName },
                                partner_rating = partnerRating?.ifBlank { null },
                                opponent_two_name = opponentTwoName?.ifBlank { defOpponentNameTwo },
                                opponent_two_rating = opponentTwoRating?.ifBlank { null }
                            )
                            database.activityQueries.deleteActivityTagsById(key.activityId)
                            activityId = key.activityId!!
                        } else {
                            database.activityQueries.insertActivity(
                                title = title,
                                description = description?.ifBlank { null },
                                surface = courtSurface,
                                venue = venue?.ifBlank { null },
                                started_at = zonedInstant,
                                duration = duration!!,
                                racquet_id = racquetId,
                                is_match = isMatch ?: false,
                                match_type = matchType,
                                seed = seed,
                                opponent_seed = opponentSeed,
                                result = result,
                                user_rating = null,
                                opponent_one_name = opponentOneName?.ifBlank { defaultOpponentName },
                                opponent_one_rating = opponentOneRating?.ifBlank { null },
                                partner_name = partnerName?.ifBlank { defPartnerName },
                                partner_rating = partnerRating?.ifBlank { null },
                                opponent_two_name = opponentTwoName?.ifBlank { defOpponentNameTwo },
                                opponent_two_rating = opponentTwoRating?.ifBlank { null }
                            )
                            activityId = database.utilQueries.selectLastInsertRowId().executeAsOne()
                        }
                        tags.forEach { tag ->
                            database.activityQueries.insertActivityTag(
                                tag = tag,
                                activityId = activityId
                            )
                        }
                        if (isMatch) {
                            val (rules, scores) = when (numSets) {
                                1 -> listOf(setOneRule!!) to listOf(setOneScore!!)
                                2 -> listOf(setOneRule!!, setTwoRule!!) to listOf(
                                    setOneScore!!,
                                    setTwoScore!!
                                )
                                3 -> listOf(setOneRule!!, setTwoRule!!, setThreeRule!!) to listOf(
                                    setOneScore!!,
                                    setTwoScore!!,
                                    setThreeScore!!
                                )
                                4 -> listOf(
                                    setOneRule!!,
                                    setTwoRule!!,
                                    setThreeRule!!,
                                    setFourRule!!
                                ) to listOf(
                                    setOneScore!!,
                                    setTwoScore!!,
                                    setThreeScore!!,
                                    setFourScore!!
                                )
                                else -> listOf(
                                    setOneRule!!,
                                    setTwoRule!!,
                                    setThreeRule!!,
                                    setFourRule!!,
                                    setFiveRule!!
                                ) to listOf(
                                    setOneScore!!,
                                    setTwoScore!!,
                                    setThreeScore!!,
                                    setFourScore!!,
                                    setFiveScore!!
                                )
                            }
                            rules
                                .map { it.toMatchSetRule }
                                .forEachIndexed { index, rule ->
                                    database.matchSetRuleQueries.insertSetRule(
                                        type = rule.type,
                                        games_rule_first_to = rule.games_rule_first_to,
                                        games_rule_win_by = rule.games_rule_win_by,
                                        game_rule_tiebreak_commences = rule.game_rule_tiebreak_commences,
                                        tiebreak_rule_first_to = rule.tiebreak_rule_first_to,
                                        tiebreak_rule_win_by = rule.tiebreak_rule_win_by
                                    )
                                    val ruleId = database.matchSetRuleQueries.selectSetRuleByRules(
                                        type = rule.type,
                                        games_rule_first_to = rule.games_rule_first_to,
                                        games_rule_win_by = rule.games_rule_win_by,
                                        game_rule_tiebreak_commences = rule.game_rule_tiebreak_commences,
                                        tiebreak_rule_first_to = rule.tiebreak_rule_first_to,
                                        tiebreak_rule_win_by = rule.tiebreak_rule_win_by
                                    ).executeAsOne().id
                                    database.matchSetRuleQueries.insertActivityMatchSetRule(
                                        activity_id = activityId,
                                        set_number = index + 1,
                                        set_rule = ruleId
                                    )
                                }

                            scores.forEachIndexed { index, set ->
                                when (set) {
                                    is Set.Advantage -> database.matchSetResultQueries.insertSetResult(
                                        activity_id = activityId,
                                        set_number = index + 1,
                                        team_games = set.games,
                                        opponent_games = set.opponentGames,
                                        team_tiebreak_points = null,
                                        opponent_tiebreak_points = null
                                    )
                                    is Set.TieBreak -> database.matchSetResultQueries.insertSetResult(
                                        activity_id = activityId,
                                        set_number = index + 1,
                                        team_games = null,
                                        opponent_games = null,
                                        team_tiebreak_points = set.points,
                                        opponent_tiebreak_points = set.opponentPoints
                                    )
                                    is Set.TieBreakSet -> database.matchSetResultQueries.insertSetResult(
                                        activity_id = activityId,
                                        set_number = index + 1,
                                        team_games = set.games,
                                        opponent_games = set.opponentGames,
                                        team_tiebreak_points = set.points,
                                        opponent_tiebreak_points = set.opponentPoints
                                    )
                                }
                            }
                        }
                    }
                    _events.send(UpsertActivityEvent.Saved)
                }
            }
        }
    }
}