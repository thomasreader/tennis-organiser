package com.gitlab.thomasreader.tennisorganiser.core.data

import android.content.res.Resources
import androidx.annotation.StringRes
import com.gitlab.thomasreader.kuantity.converter.FactorConverter
import com.gitlab.thomasreader.kuantity.converter.IdentityConverter
import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.quantity.*
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasureImpl
import com.gitlab.thomasreader.tennisorganiser.R
import java.text.NumberFormat

interface DroidUnit<T: QuantityType>: UnitOfMeasure<T> {
    val name: String
    val nameId: Int
    val descriptionRes: DroidTextProvider
}

class DroidUnitImpl<T: QuantityType>(
    override val name: String,
    @StringRes override val nameId: Int,
    symbol: String?,
    converter: UnitConverter
): UnitOfMeasureImpl<T>(symbol, converter), DroidUnit<T> {
    override val descriptionRes: DroidTextProvider =
        this.symbol?.let {
            DroidTextProvider.stringId(
                R.string.uom_name_symbol_format,
                DroidTextProvider.stringId(this.nameId),
                it
            )
        } ?: DroidTextProvider.stringId(this.nameId)

    override fun toString(): String {
        return if (symbol == null) name else "$name ($symbol)"
    }
}

fun <T: QuantityType> droidUnit(
    name: String,
    @StringRes nameId: Int,
    symbol: String?,
    factor: Double
): DroidUnit<T> {
    return when (factor == 1.0) {
        true -> DroidUnitImpl<T>(name, nameId, symbol, IdentityConverter)
        false -> DroidUnitImpl<T>(name, nameId, symbol, FactorConverter(factor))
    }
}

fun <T: QuantityType> droidUnit(
    name: String,
    @StringRes nameId: Int,
    symbol: String?,
    converter: UnitConverter
): DroidUnit<T> {
    return DroidUnitImpl<T>(name, nameId, symbol, converter)
}

fun <T: QuantityType> Quantity<T>.toNameString(
    unit: DroidUnit<T>,
    numberFormat: NumberFormat,
    resources: Resources
): String {
    val result = this convertTo unit
    val formattedResult = numberFormat.format(result)
    val name = resources.getString(unit.nameId)
    return "$formattedResult $name"
}

fun DroidUnit<*>.name(resources: Resources) = resources.getString(this.nameId)