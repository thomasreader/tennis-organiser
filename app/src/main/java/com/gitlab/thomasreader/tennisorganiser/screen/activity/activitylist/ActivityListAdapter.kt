package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityMatchRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityNonmatchRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.domain.activity.*
import com.gitlab.thomasreader.tennisorganiser.data.MatchResult

class ActivityListAdapter(
    val onClickListener: (TennisActivity) -> Unit
): ListAdapter<Pair<TennisActivity, MatchScore?>, ActivityListAdapter.ActivityViewHolder<out ViewBinding>>(ActivityDiffCallBack()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    @LayoutRes private val ACTIVITY_VIEW_HOLDER = R.layout.activity_nonmatch_recycler_row
    @LayoutRes private val MATCH_VIEW_HOLDER = R.layout.activity_match_recycler_row

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position].first.isMatch) {
            true -> MATCH_VIEW_HOLDER
            false -> ACTIVITY_VIEW_HOLDER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityViewHolder<*> {
        return when (viewType) {
            MATCH_VIEW_HOLDER -> MatchViewHolder(inflateBinding(parent, ActivityMatchRecyclerRowBinding::inflate))
            ACTIVITY_VIEW_HOLDER -> NonMatchViewHolder(inflateBinding(parent, ActivityNonmatchRecyclerRowBinding::inflate))
            else -> throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: ActivityViewHolder<*>, position: Int) {
        holder.bind(currentList[position])
    }

    abstract inner class ActivityViewHolder<T: ViewBinding>(
        binding: T
    ): BindingViewHolder<T>(binding), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            onClickListener(currentList[this.bindingAdapterPosition].first)
        }

        open fun bind(activityScore: Pair<TennisActivity, MatchScore?>) {
            val (activity, score) = activityScore
            val formattedDate = activity.startedAt.toString(itemView.context)
            val surface = activity.surfaceRes?.let {
                itemView.context.resources.getString(it)
            }

            when (this) {
                is NonMatchViewHolder -> {
                    with (binding) {
                        if (activity.venue != null) {
                            activityVenue.text = activity.venue
                            activityVenue.visible()
                        } else {
                            activityVenue.gone()
                        }

                        activityDate.text = formattedDate
                        activityTitle.text = activity.title
                        if (activity.description != null) {
                            activityDescription.text = activity.description
                            activityDescription.visible()
                        } else {
                            activityDescription.gone()
                        }
                        activityDuration.text = activity.duration.toString()
                        if (surface != null) {
                            activitySurface.text = surface
                            activitySurface.visible()
                            activitySurfaceLabel.visible()
                            durationSeparator.visible()
                        } else {
                            activitySurface.gone()
                            activitySurfaceLabel.gone()
                            durationSeparator.gone()
                        }
                    }
                }
                is MatchViewHolder -> {
                    with (binding) {
                        activityVenue.text = activity.venue
                        if (activity.venue != null) {
                            activityVenue.visible()
                        } else {
                            activityVenue.gone()
                        }

                        activityDate.text = formattedDate
                        activityTitle.text = activity.title
                        if (activity.description != null) {
                            activityDescription.text = activity.description
                            activityDescription.visible()
                        } else {
                            activityDescription.gone()
                        }
                        activityDuration.text = activity.duration.toString()
                        if (surface != null) {
                            activitySurface.text = surface
                            activitySurface.visible()
                            activitySurfaceLabel.visible()
                            durationSeparator.visible()
                        } else {
                            activitySurface.gone()
                            activitySurfaceLabel.gone()
                            durationSeparator.gone()
                        }
                    }
                }
            }
        }
    }

    inner class NonMatchViewHolder(
        binding: ActivityNonmatchRecyclerRowBinding
    ): ActivityViewHolder<ActivityNonmatchRecyclerRowBinding>(binding)

    inner class MatchViewHolder(
        binding: ActivityMatchRecyclerRowBinding
    ): ActivityViewHolder<ActivityMatchRecyclerRowBinding>(binding) {
        override fun bind(activityScore: Pair<TennisActivity, MatchScore?>) {
            val (activity, score) = activityScore
            super.bind(activityScore)
            val match = activity as TennisMatch

            with (binding) {
                if (match.result == MatchResult.WIN || match.result == MatchResult.RETIREMENT_WIN) {
                    scoreBoard.winner = 1
                } else if (match.result == MatchResult.LOSS || match.result == MatchResult.RETIREMENT_LOSS) {
                    scoreBoard.winner = -1
                } else {
                    scoreBoard.winner = 0
                }

                activityType.text = itemView.context.resources.getString(match.matchTypeRes)
                activityResult.text = itemView.context.resources.getString(match.result.stringResource)

                scoreBoard.seed = match.seed
                scoreBoard.opponentSeed = match.opponentSeed
                val playerName = match.userName.toString(itemView.context)
                when (match) {
                    is DoublesMatch -> {
                        scoreBoard.setDoublesPlayers(
                            playerName,
                            match.partnerName.toString(itemView.context),
                            match.opponentOneName.toString(itemView.context),
                            match.opponentTwoName.toString(itemView.context)
                        )
                    }
                    is SinglesMatch -> {
                        scoreBoard.setSinglesPlayers(
                            playerName,
                            match.opponentName.toString(itemView.context),
                        )
                    }
                }
                if (score != null) {
                    scoreBoard.setCount = score.numSets
                    var set1: Pair<String, String>? = null
                    var set2: Pair<String, String>? = null
                    var set3: Pair<String, String>? = null
                    var set4: Pair<String, String>? = null
                    var set5: Pair<String, String>? = null
                    score.sets.forEachIndexed { index, set ->
                        when (index) {
                            0 -> { set1 = set.scoreString to set.opponentScoreString }
                            1 -> { set2 = set.scoreString to set.opponentScoreString }
                            2 -> { set3 = set.scoreString to set.opponentScoreString }
                            3 -> { set4 = set.scoreString to set.opponentScoreString }
                            4 -> { set5 = set.scoreString to set.opponentScoreString }
                        }
                    }
                    scoreBoard.setScore(
                        set1, set2, set3, set4, set5
                    )
                }
            }
        }
    }

    private class ActivityDiffCallBack : DiffUtil.ItemCallback<Pair<TennisActivity, MatchScore?>>() {
        override fun areItemsTheSame(
            oldItem: Pair<TennisActivity, MatchScore?>,
            newItem: Pair<TennisActivity, MatchScore?>
        ): Boolean {
            return oldItem.first.id == newItem.first.id
        }

        override fun areContentsTheSame(
            oldItem: Pair<TennisActivity, MatchScore?>,
            newItem: Pair<TennisActivity, MatchScore?>
        ): Boolean {
            return oldItem == newItem
        }
    }
}