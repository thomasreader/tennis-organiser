package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitydetails

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.data.Activity
import com.gitlab.thomasreader.tennisorganiser.data.Match_set_rule
import com.gitlab.thomasreader.tennisorganiser.data.SelectRacquetInfo
import com.gitlab.thomasreader.tennisorganiser.data.SelectSetResultsById
import com.gitlab.thomasreader.tennisorganiser.domain.activity.TennisActivity
import com.gitlab.thomasreader.tennisorganiser.domain.activity.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.squareup.sqldelight.runtime.coroutines.mapToOneOrNull
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

data class ActivityDetailsState(
    val activity: TennisActivity,
    val tags: List<String>,
    val racquet: DroidTextProvider?,
    val rules: List<Match_set_rule>,
    val results: List<SelectSetResultsById>
)

sealed interface ActivityDetailsEvent {
    object Deleted: ActivityDetailsEvent
}

class ActivityDetailsViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
): KeyedViewModel<ActivityMatchDetailsScreen>(_key) {
    private val _events = Channel<ActivityDetailsEvent>(Channel.BUFFERED)
    val events: Flow<ActivityDetailsEvent> = _events.receiveAsFlow()

    val uiState: StateFlow<ActivityDetailsState?> = combine(
        database.activityQueries.selectActivityById(key.activityId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        database.activityQueries.selectTagsByActivityId(key.activityId)
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.racquetQueries.selectRacquetInfo(key.activityId)
            .asFlow()
            .mapToOneOrNull(Dispatchers.IO),
        database.matchSetRuleQueries.selectSetRulesByIdAsc(key.activityId)
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.matchSetResultQueries.selectSetResultsById(key.activityId)
            .asFlow()
            .mapToList(Dispatchers.IO),
        dataStore.data
    ) { args ->
        val activity = args[0] as Activity
        val tags = args[1] as List<String>
        val racquet = args[2] as SelectRacquetInfo?
        val rules = args[3] as List<Match_set_rule>
        val results = args[4] as List<SelectSetResultsById>
        val preferences = args[5] as Preferences

        val username = preferences.getMapped(DataStoreKeys.USERNAME)
        return@combine ActivityDetailsState(
            activity = activity.toDomain(username),
            tags = tags,
            racquet = racquet?.let {
                DroidTextProvider.stringId(
                    R.string.racquet_identifier_brand_model,
                    it.identifier,
                    it.brand,
                    it.model
                )
            },
            rules = rules,
            results = results
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun deleteActivity() {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            database.activityQueries.deleteActivity(key.activityId)
            _events.send(ActivityDetailsEvent.Deleted)
        }
    }
}
