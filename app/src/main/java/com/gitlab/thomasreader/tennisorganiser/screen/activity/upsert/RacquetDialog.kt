package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RacquetDialog: DialogFragment(R.layout.recycler_view_dialog) {
    private val adapter = RacquetAdapter { racquetId ->
        onClickListener?.let { it(racquetId) }
        dialog?.dismiss()
    }
    var onClickListener: ((Long) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as RecyclerView).apply {
            itemAnimator = DefaultItemAnimator()
            adapter = this@RacquetDialog.adapter
        }
        val viewModel: UpsertActivityViewModel = (parentFragment as KeyedFragment).savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    if (state != null) {
                        adapter.submitList(state.racquets)
                    }
                }
            }
        }
    }
}

class RacquetAdapter(
    val onClickListener: (Long) -> Unit
): ListAdapter<Pair<Long, DroidTextProvider>, RacquetAdapter.ViewHolder>(RacquetDiff()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)
            .let { ViewHolder(it) }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = currentList[position].second.toString(holder.itemView.context)
    }

    inner class ViewHolder(
        itemView: View
    ): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }
        val textView = itemView as TextView

        override fun onClick(v: View?) {
            onClickListener(currentList[bindingAdapterPosition].first)
        }
    }

    class RacquetDiff: DiffUtil.ItemCallback<Pair<Long, DroidTextProvider>>() {
        override fun areItemsTheSame(
            oldItem: Pair<Long, DroidTextProvider>,
            newItem: Pair<Long, DroidTextProvider>
        ): Boolean {
            return oldItem.first == newItem.first
        }

        override fun areContentsTheSame(
            oldItem: Pair<Long, DroidTextProvider>,
            newItem: Pair<Long, DroidTextProvider>
        ): Boolean {
            return oldItem == newItem
        }
    }
}