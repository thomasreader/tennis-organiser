package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class UpsertActivityScreen @JvmOverloads constructor(
    val activityId: Long? = null
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = UpsertActivityFragment()
    override fun hideBottomNavigation(): Boolean = true
    val isEdit get() = this.activityId != null
}

