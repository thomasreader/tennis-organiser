package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.stringhistory

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class RacquetStringHistoryScreen(
    val racquetId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = RacquetStringHistoryFragment()
}