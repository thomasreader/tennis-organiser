package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.restring

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class RestringRacquetScreen(
    val racquetId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = RestringRacquetFragment()
    override fun hideBottomNavigation(): Boolean = true
}