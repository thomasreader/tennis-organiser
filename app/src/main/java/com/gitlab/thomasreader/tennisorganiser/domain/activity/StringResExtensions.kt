package com.gitlab.thomasreader.tennisorganiser.domain.activity

import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.data.CourtSurface
import com.gitlab.thomasreader.tennisorganiser.data.MatchResult
import com.gitlab.thomasreader.tennisorganiser.data.MatchType
import com.gitlab.thomasreader.tennisorganiser.data.SetType

val CourtSurface.stringResource get(): Int {
    return when (this) {
        CourtSurface.GRASS -> R.string.surface_grass
        CourtSurface.ARTIFICIAL_GRASS -> R.string.surface_artificial_grass
        CourtSurface.CLAY -> R.string.surface_clay
        CourtSurface.ARTIFICIAL_CLAY -> R.string.surface_artificial_clay
        CourtSurface.HARD -> R.string.surface_hard
        CourtSurface.CARPET -> R.string.surface_carpet
    }
}

val MatchResult.stringResource get(): Int {
    return when (this) {
        MatchResult.RETIREMENT_WIN -> R.string.result_win_retirement
        MatchResult.WIN -> R.string.result_win
        MatchResult.DRAW -> R.string.result_draw
        MatchResult.LOSS -> R.string.result_loss
        MatchResult.RETIREMENT_LOSS -> R.string.result_loss_retirement
    }
} 

val MatchType.stringResource get(): Int {
    return when (this) {
        MatchType.SINGLES_MEN -> R.string.mens_singles
        MatchType.SINGLES_WOMEN -> R.string.womens_singles
        MatchType.SINGLES_OTHER -> R.string.other_singles
        MatchType.DOUBLES_MEN -> R.string.mens_doubles
        MatchType.DOUBLES_WOMEN -> R.string.womens_doubles
        MatchType.DOUBLES_MIXED -> R.string.mixed_doubles
        MatchType.DOUBLES_OTHER -> R.string.other_doubles
    }
}

val SetType.stringResource get(): Int {
    return when (this) {
        SetType.TIEBREAK -> R.string.tiebreak_set
        SetType.TIEBREAK_SET -> R.string.tiebreakset_set
        SetType.ADVANTAGE_SET -> R.string.advantage_set
    }
}