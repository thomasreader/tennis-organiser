package com.gitlab.thomasreader.tennisorganiser.screen.settings.about

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class AboutScreen: AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = AboutFragment()
}