package com.gitlab.thomasreader.tennisorganiser.domain.racquet

import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.data.GripSize

typealias RacquetDatabase = com.gitlab.thomasreader.tennisorganiser.data.Racquet

class Racquet(
    private val racquet: RacquetDatabase,
    val weight: String?,
    val balancePoint: String?
) {
    val id get() = racquet.id
    val identifier get() = racquet.identifier
    val modelId get() = racquet.model_id
    val swingWeight get() = racquet.swing_weight
    val notes get() = racquet.notes

    override fun equals(other: Any?): Boolean = racquet.equals(other)
    override fun hashCode(): Int = racquet.hashCode()
}

fun RacquetDatabase.toDomain(
    weightUnit: DroidUnit<Mass>,
    balanceUnit: DroidUnit<Length>
): Racquet {
    return Racquet(
        racquet = this,
        weight = weight?.toSymbolString(
            weightUnit,
            Constants.DisplayOptions.Format.weightFormatter(weightUnit)
        ),
        balancePoint = balance_point?.toSymbolString(
            balanceUnit,
            Constants.DisplayOptions.Format.balancePointFormatter(balanceUnit)
        )
    )
}