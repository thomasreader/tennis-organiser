# Tennis Organiser
Android application to manage tennis matches, racquets and strings.

*API 21+ (Lollypop)*

## Built With
##### [Kotlin Coroutines](https://kotlinlang.org/docs/coroutines-overview.html)
Execute code asynchronously

##### [SQLDelight](https://github.com/cashapp/sqldelight)
Persist data locally

##### [Simple-Stack](https://github.com/Zhuinden/simple-stack)
Navigation

##### [Logcat](https://github.com/square/logcat)
Logging

##### [Kuantity](https://gitlab.com/thomasreader/kuantity)
Units of measurement

##### [DataStore](https://developer.android.com/topic/libraries/architecture/datastore)
Preference storage

## To-do
* Tests
* Pictures

## License
[GNU GPLv3](https://gitlab.com/thomasreader/tennis-organiser/-/blob/master/LICENSE)