package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.stringhistory

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.databinding.StringBedRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.StringBedSameRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringBed
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding

class StringHistoryAdapter(
    val onLongClickListener: (StringBed) -> Unit
): ListAdapter<StringBed, StringHistoryAdapter.StringBedViewHolder<out ViewBinding>>(StringHistoryDiffCallBack()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    @LayoutRes private val NORMAL_STRING_BED = R.layout.string_bed_same_recycler_row
    @LayoutRes private val CUSTOM_STRING_BED = R.layout.string_bed_recycler_row

    override fun getItemViewType(position: Int): Int {
        return with (currentList[position]) {
            when (mainsId == crossId && mainTensionString == crossTensionString) {
                true -> NORMAL_STRING_BED
                false -> CUSTOM_STRING_BED
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringBedViewHolder<out ViewBinding> {
        return when (viewType) {
            NORMAL_STRING_BED -> {
                NormalStringBedViewHolder(
                    inflateBinding(parent, StringBedSameRecyclerRowBinding::inflate)
                )
            }
            CUSTOM_STRING_BED -> {
                CustomStringBedViewHolder(
                    inflateBinding(parent, StringBedRecyclerRowBinding::inflate)
                )
            }
            else -> throw IllegalStateException("Invalid StringBedViewHolder viewType onCreateViewHolder()")
        }
    }

    override fun onBindViewHolder(holder: StringBedViewHolder<*>, position: Int) {
        holder.bind(currentList[position])
    }

    abstract inner class StringBedViewHolder<T: ViewBinding>(
        binding: T
    ): BindingViewHolder<T>(binding), View.OnLongClickListener {
        init {
            binding.root.setOnLongClickListener(this)
        }

        abstract fun bind(stringBed: StringBed)

        protected fun internalBind(
            stringBed: StringBed,
            dateRangeView: TextView,
            ageView: TextView,
            notesLabelView: TextView,
            notesView: TextView
        ) {
            dateRangeView.text = stringBed.dateRangeRes.toString(binding.root.context)
            ageView.text = stringBed.elapsedRes.toString(binding.root.context)
            if (stringBed.notes != null) {
                notesView.visibility = View.VISIBLE
                notesLabelView.visibility = View.VISIBLE
                notesView.text = stringBed.notes
            } else {
                notesView.visibility = View.GONE
                notesLabelView.visibility = View.GONE
            }
        }

        override fun onLongClick(v: View): Boolean {
            onLongClickListener(currentList[bindingAdapterPosition])
            return true
        }
    }

    inner class NormalStringBedViewHolder(
        binding: StringBedSameRecyclerRowBinding
    ): StringBedViewHolder<StringBedSameRecyclerRowBinding>(binding) {

        override fun bind(stringBed: StringBed) {
            with (binding) {
                internalBind(
                    stringBed = stringBed,
                    dateRangeView = binding.stringBedDateRange,
                    ageView = binding.stringBedAge,
                    notesLabelView = binding.stringBedNotesLabel,
                    notesView = binding.stringBedNotes
                )
                mainsCrossesStrings.text = stringBed.mainsVariantInfo.toString(binding.root.context)
                mainsCrossesTension.text = stringBed.mainTensionString
            }
        }
    }

    inner class CustomStringBedViewHolder(
        binding: StringBedRecyclerRowBinding
    ): StringBedViewHolder<StringBedRecyclerRowBinding>(binding) {
        override fun bind(stringBed: StringBed) {
            with (binding) {
                internalBind(
                    stringBed = stringBed,
                    dateRangeView = binding.stringBedDateRange,
                    ageView = binding.stringBedAge,
                    notesLabelView = binding.stringBedNotesLabel,
                    notesView = binding.stringBedNotes
                )

                mainsStrings.text = stringBed.mainsVariantInfo.toString(binding.root.context)
                mainsTension.text = stringBed.mainTensionString
                crossesStrings.text = stringBed.crossVariantInfo.toString(binding.root.context)
                crossesTension.text = stringBed.crossTensionString
            }
        }
    }

    private class StringHistoryDiffCallBack: DiffUtil.ItemCallback<StringBed>() {
        override fun areItemsTheSame(oldItem: StringBed, newItem: StringBed): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StringBed, newItem: StringBed): Boolean {
            return oldItem == newItem
        }
    }
}