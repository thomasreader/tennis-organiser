package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.viewbinding.ViewBinding
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.extension.*
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.StubViewBindingDelegate
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.core.widget.EnumArrayAdapter
import com.gitlab.thomasreader.tennisorganiser.data.MatchResult
import com.gitlab.thomasreader.tennisorganiser.data.MatchType
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityUpsertBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityUpsertStubBinding
import com.gitlab.thomasreader.tennisorganiser.domain.activity.scorePair
import com.gitlab.thomasreader.tennisorganiser.domain.activity.stringResource
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

//score
enum class UpsertActivityDialog {
    COURT_SURFACE,
    DATE,
    TIME,
    DURATION,
    RACQUET,
    SET_ONE,
    SET_TWO,
    SET_THREE,
    SET_FOUR,
    SET_FIVE;
}

class UpsertActivityFragment: KeyedFragment(R.layout.activity_upsert) {
    private val binding by bindView(ActivityUpsertBinding::bind)
    private val saveButton: ActionMenuItemView get() = binding.toolbar.action(R.id.save_action)
    private val stubBinding by StubViewBindingDelegate(
        lazy { binding.matchDetailStub },
        ActivityUpsertStubBinding::bind,
        ::onStubViewCreated
    )
    private lateinit var viewModel: UpsertActivityViewModel
    private val tagAdapter: TagAdapter = TagAdapter { viewModel.removeTag(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        childFragmentManager.addFragmentOnAttachListener { _, fragment ->
            when (fragment.tag) {
                UpsertActivityDialog.COURT_SURFACE.name -> {
                    (fragment as CourtSurfaceDialog).onPositiveButtonClickListener = {
                        viewModel.updateSurface(it)
                    }
                }
                UpsertActivityDialog.DATE.name -> {
                    (fragment as MaterialDatePicker<Long>).addOnPositiveButtonClickListener { date ->
                        viewModel.updateDate(date)
                    }
                }
                UpsertActivityDialog.TIME.name -> {
                    (fragment as MaterialTimePicker).addOnPositiveButtonClickListener {
                        viewModel.updateTime(fragment.hour to fragment.minute)
                    }
                }
                UpsertActivityDialog.DURATION.name -> {
                    (fragment as DurationDialog).onClickListener = {
                        viewModel.updateDuration(it)
                    }
                }
                UpsertActivityDialog.RACQUET.name -> {
                    (fragment as RacquetDialog).onClickListener = {
                        viewModel.updateRacquetId(it)
                    }
                }
                UpsertActivityDialog.SET_ONE.name -> attachSetListener(fragment, 1)
                UpsertActivityDialog.SET_TWO.name -> attachSetListener(fragment, 2)
                UpsertActivityDialog.SET_THREE.name -> attachSetListener(fragment, 3)
                UpsertActivityDialog.SET_FOUR.name -> attachSetListener(fragment, 4)
                UpsertActivityDialog.SET_FIVE.name -> attachSetListener(fragment, 5)
            }
        }
    }

    private fun attachSetListener(fragment: Fragment, setNumber: Int) {
        (fragment as SetScoreDialog).onClickListener = { setRule, setScore ->
            when (setNumber) {
                1 -> {
                    viewModel.updateSetOneRule(setRule)
                    viewModel.updateSetOneScore(setScore)
                }
                2 -> {
                    viewModel.updateSetTwoRule(setRule)
                    viewModel.updateSetTwoScore(setScore)
                }
                3 -> {
                    viewModel.updateSetThreeRule(setRule)
                    viewModel.updateSetThreeScore(setScore)
                }
                4 -> {
                    viewModel.updateSetFourRule(setRule)
                    viewModel.updateSetFourScore(setScore)
                }
                5 -> {
                    viewModel.updateSetFiveRule(setRule)
                    viewModel.updateSetFiveScore(setScore)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)

        with (binding) {
            toolbar.setTitle(if (viewModel.key.isEdit) R.string.edit_activity else R.string.add_activity)
            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        saveActivity()
                        true
                    }
                    else -> false
                }
            }
            titleInput.doOnTextChanged { text, _, _, _ ->
                viewModel.updateTitle(text?.toString())
            }
            descriptionInput.doOnTextChanged { text, _, _, _ ->
                viewModel.updateDescription(text?.toString())
            }
            surfaceInput.setOnClickListener {
                CourtSurfaceDialog().show(childFragmentManager, UpsertActivityDialog.COURT_SURFACE.name)
            }
            venueInput.doOnTextChanged { text, _, _, _ ->
                viewModel.updateVenue(text?.toString())
            }
            racquetInput.setOnClickListener {
                RacquetDialog().show(childFragmentManager, UpsertActivityDialog.RACQUET.name)
            }
            tagInput.setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (viewModel.addTag(tagInput.text.toString())) {
                        tagInput.setText("")
                        tagInput.requestFocus()
                        return@setOnEditorActionListener true
                    }
                }
                return@setOnEditorActionListener false
            }
            tagRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = tagAdapter
                LinearSnapHelper().attachToRecyclerView(this)
            }
            isMatch.setOnCheckedChangeListener { _, isChecked ->
                animateMatchDetails(isChecked)
                viewModel.updateIsMatch(isChecked)
            }
        }
    }

    private fun onStubViewCreated(viewBinding: ActivityUpsertStubBinding) {
        viewBinding.matchTypeInput.setAdapter(
            EnumArrayAdapter(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                MatchType.values().toList()
            ) { getString(it.stringResource) }
        )
        viewBinding.matchTypeInput.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrBlank()) {
                val matchType =
                    (viewBinding.matchTypeInput.adapter as EnumArrayAdapter<MatchType>)
                        .enumFromCharSequenceValue(text.toString())
                viewModel.updateMatchType(matchType)
            }
        }

        viewBinding.resultInput.setAdapter(
            EnumArrayAdapter(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                MatchResult.values().toList()
            ) { getString(it.stringResource) }
        )
        viewBinding.resultInput.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrBlank()) {
                val result =
                    (viewBinding.resultInput.adapter as EnumArrayAdapter<MatchResult>)
                        .enumFromCharSequenceValue(text.toString())
                viewModel.updateResult(result)
            }
        }

        viewBinding.seedInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateSeed(text?.toString()?.toIntOrNull())
        }
        viewBinding.partnerNameInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updatePartnerName(text?.toString())
        }
        viewBinding.partnerRatingInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updatePartnerRating(text?.toString())
        }
        viewBinding.seedOpponentInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateOpponentSeed(text?.toString()?.toIntOrNull())
        }
        viewBinding.opponentOneInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateOpponentOneName(text?.toString())
        }
        viewBinding.opponentOneRatingInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateOpponentOneRating(text?.toString())
        }
        viewBinding.opponentTwoInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateOpponentTwoName(text?.toString())
        }
        viewBinding.opponentTwoRatingInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateOpponentTwoRating(text?.toString())
        }
        viewBinding.numberSetsInput.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                listOf("1", "2", "3", "4", "5")
            )
        )
        viewBinding.numberSetsInput.doOnTextChanged { text, _, _, _ ->
            text?.toString()?.toIntOrNull()?.let { viewModel.updateNumSets(it) }
        }
    }


    private fun animateMatchDetails(show: Boolean) {
        val animatorSet = AnimatorSet()
        val fadeAnimator = ObjectAnimator.ofFloat(stubBinding.root, View.ALPHA, if (show) 1f else 0f).apply {
            doOnStart { if (show) stubBinding.root.visible() }
            doOnEnd { if (!show) stubBinding.root.gone()  }
        }
        animatorSet.play(fadeAnimator)
        if (!show) {
            val scrollTo = binding.isMatch.bottom - binding.upsertScrollView.height
            val scrollAnimator = ValueAnimator.ofInt(binding.upsertScrollView.scrollY, scrollTo).apply {
                addUpdateListener {
                    binding.upsertScrollView.scrollY = it.animatedValue as Int
                }
                interpolator = DecelerateInterpolator()
            }
            animatorSet.play(scrollAnimator)
        }
        animatorSet.apply {
            duration = 1000L
            start()
        }
    }

    private fun assertGoBack() {
        AlertDialog.Builder(requireContext())
            .setTitle("Discard entry?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun initViewModel() {
        viewModel = savedViewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            saveButton.isEnabled = state.saveEnabled
                            binding.titleInput.updateTextIfNotFocused(state.title)
                            binding.descriptionInput.updateTextIfNotFocused(state.description)
                            binding.surfaceInput.updateTextIfNotFocused(
                                state.courtSurface?.stringResource?.let { getString(it) }
                            )
                            binding.venueInput.updateTextIfNotFocused(state.venue)
                            if (binding.venueInput.adapter == null) {
                                binding.venueInput.setAdapter(
                                    ArrayAdapter(
                                        requireContext(),
                                        android.R.layout.simple_dropdown_item_1line,
                                        state.venues
                                    )
                                )
                            }
                            binding.dateInput.setText(state.date?.toString(requireContext()))
                            binding.dateLayout.updateErrorVisibility(state.dateError, DroidTextProvider.REQUIRED)
                            binding.dateInput.setOnClickListener {
                                MaterialDatePicker.Builder.datePicker()
                                    .setSelection(state.date?.time)
                                    .build()
                                    .show(childFragmentManager, UpsertActivityDialog.DATE.name)
                            }

                            binding.timeInput.setText(state.time?.toString())
                            binding.timeLayout.updateErrorVisibility(state.timeError, DroidTextProvider.REQUIRED)
                            binding.timeInput.setOnClickListener {
                                val time = state.time
                                MaterialTimePicker.Builder().apply {
                                    setTimeFormat(TimeFormat.CLOCK_24H)
                                    time?.let {
                                        setHour(it.hour)
                                        setMinute(it.minutes)
                                    }
                                }
                                    .build()
                                    .show(childFragmentManager, UpsertActivityDialog.TIME.name)
                            }

                            binding.durationInput.setText(state.duration?.toString())
                            binding.durationLayout.updateErrorVisibility(state.durationError, DroidTextProvider.REQUIRED)
                            binding.durationInput.setOnClickListener {
                                DurationDialog(state.duration?.seconds)
                                    .show(childFragmentManager, UpsertActivityDialog.DURATION.name)
                            }

                            binding.racquetInput.updateTextIfNotFocused(state.racquet?.second?.toString(requireContext()))

                            tagAdapter.submitList(state.selectedTags)
                            val tagAutoComplete = ArrayAdapter(
                                requireContext(),
                                android.R.layout.simple_dropdown_item_1line,
                                state.tags
                            )
                            binding.tagInput.setAdapter(tagAutoComplete)

                            binding.isMatch.isChecked = state.isMatch
                            if (state.isMatch) {
                                if (stubBinding.partnerNameInput.adapter == null) {
                                    val nameAdapter = ArrayAdapter(
                                        requireContext(),
                                        android.R.layout.simple_dropdown_item_1line,
                                        state.names
                                    )
                                    stubBinding.partnerNameInput.setAdapter(nameAdapter)
                                    stubBinding.opponentOneInput.setAdapter(nameAdapter)
                                    stubBinding.opponentTwoInput.setAdapter(nameAdapter)
                                }
                                stubBinding.matchTypeInput.updateTextIfNotFocused(
                                    state.matchType?.stringResource?.let { getString(it) }
                                )
                                stubBinding.matchTypeLayout.updateErrorVisibility(state.matchTypeError, R.string.required)
                                stubBinding.resultInput.updateTextIfNotFocused(
                                    state.result?.stringResource?.let { getString(it) }
                                )
                                stubBinding.resultLayout.updateErrorVisibility(state.resultError, R.string.required)
                                stubBinding.seedInput.updateTextIfNotFocused(state.seed)
                                stubBinding.partnerNameInput.updateTextIfNotFocused(state.partnerName)
                                stubBinding.partnerRatingInput.updateTextIfNotFocused(state.partnerRating)
                                stubBinding.seedOpponentInput.updateTextIfNotFocused(state.opponentSeed)
                                stubBinding.opponentOneInput.updateTextIfNotFocused(state.opponentOneName)
                                stubBinding.opponentOneRatingInput.updateTextIfNotFocused(state.opponentOneRating)
                                stubBinding.opponentTwoInput.updateTextIfNotFocused(state.opponentTwoName)
                                stubBinding.opponentTwoRatingInput.updateTextIfNotFocused(state.opponentTwoRating)
                                stubBinding.scoreBoard.setCount = state.numSets
                                stubBinding.scoreBoard.winner = 0
                                stubBinding.scoreBoard.seed = state.seed?.toIntOrNull()
                                stubBinding.scoreBoard.opponentSeed = state.opponentSeed?.toIntOrNull()
                                val playerName = state.playerName.toString(requireContext())
                                state.matchType?.let {
                                    if (it.isSingles) {
                                        stubBinding.partnerNameLayout.gone()
                                        stubBinding.partnerRatingLayout.gone()
                                        stubBinding.opponentTwoLayout.gone()
                                        stubBinding.opponentTwoRatingLayout.gone()
                                        stubBinding.scoreBoard.setSinglesPlayers(
                                            playerName,
                                            state.opponentOneName?.ifBlank { null } ?: getString(R.string.opponent)
                                        )
                                    } else {
                                        stubBinding.partnerNameLayout.visible()
                                        stubBinding.partnerRatingLayout.visible()
                                        stubBinding.opponentTwoLayout.visible()
                                        stubBinding.opponentTwoRatingLayout.visible()
                                        stubBinding.scoreBoard.setDoublesPlayers(
                                            playerName,
                                            state.partnerName?.ifBlank { null } ?: getString(R.string.partner),
                                            state.opponentOneName?.ifBlank { null } ?: getString(R.string.opponent),
                                            state.opponentTwoName?.ifBlank { null } ?: getString(R.string.opponent)
                                        )
                                    }
                                }
                                stubBinding.scoreBoard.setScore(
                                    state.setOneScore?.scorePair,
                                    state.setTwoScore?.scorePair,
                                    state.setThreeScore?.scorePair,
                                    state.setFourScore?.scorePair,
                                    state.setFiveScore?.scorePair
                                )
                                stubBinding.scoreBoard.winner = 1
                                if (state.numSets < 2) {
                                    stubBinding.setTwoLayout.gone()
                                } else {
                                    stubBinding.setTwoLayout.visible()
                                }
                                if (state.numSets < 3) {
                                    stubBinding.setThreeLayout.gone()
                                } else {
                                    stubBinding.setThreeLayout.visible()
                                }
                                if (state.numSets < 4) {
                                    stubBinding.setFourLayout.gone()
                                } else {
                                    stubBinding.setFourLayout.visible()
                                }
                                if (state.numSets < 5) {
                                    stubBinding.setFiveLayout.gone()
                                } else {
                                    stubBinding.setFiveLayout.visible()
                                }
                                stubBinding.setOneInput.setText(state.setOneRule?.ruleDesc?.toString(requireContext()))
                                stubBinding.setTwoInput.setText(state.setTwoRule?.ruleDesc?.toString(requireContext()))
                                stubBinding.setThreeInput.setText(state.setThreeRule?.ruleDesc?.toString(requireContext()))
                                stubBinding.setFourInput.setText(state.setFourRule?.ruleDesc?.toString(requireContext()))
                                stubBinding.setFiveInput.setText(state.setFiveRule?.ruleDesc?.toString(requireContext()))

                                stubBinding.setOneLayout.updateErrorVisibility(state.setOneError, R.string.required)
                                stubBinding.setTwoLayout.updateErrorVisibility(state.setTwoError, R.string.required)
                                stubBinding.setThreeLayout.updateErrorVisibility(state.setThreeError, R.string.required)
                                stubBinding.setFourLayout.updateErrorVisibility(state.setFourError, R.string.required)
                                stubBinding.setFiveLayout.updateErrorVisibility(state.setFiveError, R.string.required)

                                stubBinding.setOneInput.setOnClickListener {
                                    SetScoreDialog(1, state.setOneRule, state.setOneScore)
                                        .show(childFragmentManager, UpsertActivityDialog.SET_ONE.name)
                                }
                                stubBinding.setTwoInput.setOnClickListener {
                                    SetScoreDialog(2, state.setTwoRule, state.setTwoScore)
                                        .show(childFragmentManager, UpsertActivityDialog.SET_TWO.name)
                                }
                                stubBinding.setThreeInput.setOnClickListener {
                                    SetScoreDialog(3, state.setThreeRule, state.setThreeScore)
                                        .show(childFragmentManager, UpsertActivityDialog.SET_THREE.name)
                                }
                                stubBinding.setFourInput.setOnClickListener {
                                    SetScoreDialog(4, state.setFourRule, state.setFourScore)
                                        .show(childFragmentManager, UpsertActivityDialog.SET_FOUR.name)
                                }
                                stubBinding.setFiveInput.setOnClickListener {
                                    SetScoreDialog(5, state.setFiveRule, state.setFiveScore)
                                        .show(childFragmentManager, UpsertActivityDialog.SET_FIVE.name)
                                }
                            }
                        }
                    }
                }

                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            is UpsertActivityEvent.Saved -> when (viewModel.key.isEdit) {
                                true -> backstack.goBack()
                                false -> backstack.jumpToRoot(StateChange.FORWARD)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun saveActivity() {
        val activityNames = mapOf(
            R.string.morning_match to getString(R.string.morning_match),
            R.string.afternoon_match to getString(R.string.afternoon_match),
            R.string.evening_match to getString(R.string.evening_match),
            R.string.night_match to getString(R.string.night_match),
            R.string.morning_activity to getString(R.string.morning_activity),
            R.string.afternoon_activity to getString(R.string.afternoon_activity),
            R.string.evening_activity to getString(R.string.evening_activity),
            R.string.night_activity to getString(R.string.night_activity),
        )

        viewModel.saveActivity(
            activityNames,
            getString(R.string.opponent),
            getString(R.string.partner)
        )
    }
}