package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add

import android.database.sqlite.SQLiteException
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.times
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.data.GripSize
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

data class AddRacquetState(
    val racquetInfoRes: DroidTextProvider,
    val idError: Boolean,
    val saveEnabled: Boolean
)

sealed interface AddRacquetEvent {
    object Saved : AddRacquetEvent
}

class AddRacquetViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<AddRacquetScreen>(_key) {
    private val _events = Channel<AddRacquetEvent>(Channel.BUFFERED)
    val events: Flow<AddRacquetEvent> = _events.receiveAsFlow()

    var weightUnitIx: Int? = savedStateHandle["weight_unit_ix"]
        set(value) {
            println("weightUnitIx: $value")
            if (field != value) {
                field = value
                savedStateHandle["weight_unit_ix"] = value
            }
        }

    var balancePointUnitIx: Int? = savedStateHandle["balance_point_unit_ix"]
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["balance_point_unit_ix"] = value
            }
        }

    private val identifierError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val isSaving: MutableStateFlow<Boolean> = MutableStateFlow(false)

    val uiState = combine(
        database.racquetModelQueries.selectBasicModelById(key.modelId)
            .asFlow()
            .mapToOne(Dispatchers.IO)
            .map {
                DroidTextProvider.stringId(
                    R.string.brand_model_join,
                    it.brand,
                    it.model
                )
            },
        identifierError,
        isSaving,
        dataStore.data
    ) { racquetInfo, idError, saving, preferences ->
        val gripSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_GRIP_SIZE)
        val saveDisabled = saving || idError
        return@combine AddRacquetState(
            racquetInfoRes = racquetInfo,
            idError = idError,
            saveEnabled = !saveDisabled
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun updateIdentifier(identifier: String?) {
        this.identifierError.value = identifier.isNullOrBlank()
    }

    fun addRacquet(
        identifier: String?,
        weight: Double?,
        swingWeight: Int?,
        balancePoint: Double?,
        notes: String?
    ) {
        if (!identifier.isNullOrBlank()) {
            val weightUnitIx = this.weightUnitIx
            val balancePointUnitIx = this.balancePointUnitIx
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                val weightUnit = weightUnitIx?.let { Constants.DisplayOptions.RACQUET_WEIGHT_UNITS[it] }
                val balancePointUnit = balancePointUnitIx?.let { Constants.DisplayOptions.RACQUET_BALANCE_UNITS[it] }

                val internalWeight = when (weight != null && weightUnit != null) {
                    true -> weight * weightUnit
                    else -> null
                }
                val internalBalancePoint =  when (balancePoint != null && balancePointUnit != null) {
                    true -> balancePoint * balancePointUnit
                    else -> null
                }
                try {
                    database.racquetQueries.insertRacquet(
                        model_id = key.modelId,
                        identifier = identifier,
                        weight = internalWeight,
                        swing_weight = swingWeight,
                        balance_point = internalBalancePoint,
                        notes = notes
                    )
                    _events.send(AddRacquetEvent.Saved)
                } catch (sqlException: SQLiteException) {
                    isSaving.value = false
                }
            }
        }
    }
}