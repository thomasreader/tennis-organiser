package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll

import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetModelListAddBinding
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.add.AddRacquetModelScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.details.RacquetModelDetailsScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add.AddRacquetScreen
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class
AddRacquetModelListFragment: KeyedFragment(R.layout.racquet_model_list_add) {
    private var _binding: RacquetModelListAddBinding? = null
    private val binding get() = _binding!!
    private val modelAdapter: AddRacquetModelListAdapter = AddRacquetModelListAdapter(
        onClickListener = { backstack.goTo(AddRacquetScreen(it.id)) },
        onLongClickListener = { backstack.goTo(RacquetModelDetailsScreen(it.id)) }
    )
    private lateinit var viewModel: RacquetModelListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = RacquetModelListAddBinding.bind(view)
        initViewModel()

        with (binding) {
            addRacquetModelFab.setOnClickListener {
                backstack.goTo(AddRacquetModelScreen())
            }

            racquetModelList.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = modelAdapter
                animateFABOnScroll(addRacquetModelFab)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViewModel() {
        viewModel = viewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    if (state != null) {
                        modelAdapter.submitList(state)
                    }
                }
            }
        }
    }
}