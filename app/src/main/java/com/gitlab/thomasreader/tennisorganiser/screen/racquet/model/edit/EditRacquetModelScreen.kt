package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.edit

import androidx.fragment.app.Fragment
import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class EditRacquetModelScreen(
    val modelId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment(): Fragment = EditRacquetModelFragment()
    override fun hideBottomNavigation(): Boolean = true
}