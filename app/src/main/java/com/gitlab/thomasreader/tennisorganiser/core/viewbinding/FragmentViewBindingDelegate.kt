package com.gitlab.thomasreader.tennisorganiser.core.viewbinding

import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <T: ViewBinding> DialogFragment.inflateView(
    crossinline inflater: (LayoutInflater) -> T
): Lazy<T> {
    return lazy { inflater(this.layoutInflater) }
}

fun <T: ViewBinding> Fragment.bindView(
    binder: (View) -> T,
    onViewDestroyedBlock: (T.() -> Unit)? = null
): ReadOnlyProperty<Fragment, T> {
    return FragmentViewBindingDelegate<T>(binder, onViewDestroyedBlock)
}

private class FragmentViewBindingDelegate<T: ViewBinding>(
    private val binder: (View) -> T,
    private val onViewDestroyedBlock: (T.() -> Unit)?
): ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        val binding = this.binding
        if (binding != null) {
            return binding
        }

        val lifeCycle = thisRef.viewLifecycleOwner.lifecycle
        if (!lifeCycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            throw IllegalStateException("Attempted to retrieve binding when View is destroyed")
        } else {
            lifeCycle.addObserver(this)
            return this.binder(thisRef.requireView()).also { this.binding = it }
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        owner.lifecycle.removeObserver(this)
        this.binding?.let { this.onViewDestroyedBlock?.invoke(it) }
        this.binding = null
    }
}