package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.detail

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.databinding.LabelInfoLayoutBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding

class RacquetDetailsAdapter : ListAdapter<RacquetDetailRow, RacquetDetailsAdapter.ViewHolder>(RacquetInfoDiffCallBack()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    lateinit var noValueString: String

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        if (!::noValueString.isInitialized) noValueString = recyclerView.context.getString(R.string.no_value)
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            inflateBinding(parent, LabelInfoLayoutBinding::inflate)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(
        binding: LabelInfoLayoutBinding
    ) : BindingViewHolder<LabelInfoLayoutBinding>(binding) {
        fun bind(row: RacquetDetailRow) {
            binding.detailLabel.text = row.header.toString(itemView.resources)
            binding.detailInfo.text = row.info?.toString(itemView.context) ?: noValueString
            println(row.info?.toString(itemView.context) ?: noValueString)
        }
    }

    private class RacquetInfoDiffCallBack : DiffUtil.ItemCallback<RacquetDetailRow>() {
        override fun areItemsTheSame(oldItem: RacquetDetailRow, newItem: RacquetDetailRow): Boolean {
            return oldItem.header == newItem.header
        }

        override fun areContentsTheSame(oldItem: RacquetDetailRow, newItem: RacquetDetailRow): Boolean {
            return oldItem == newItem
        }
    }
}