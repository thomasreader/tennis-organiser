package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.restring

import android.text.format.DateUtils
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.times
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.data.SelectRacquetInfo
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringModelVariant
import com.gitlab.thomasreader.tennisorganiser.domain.string.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

data class RestringRacquetState(
    val racquetInfo: DroidTextProvider,
    val stringVariants: List<StringModelVariant>,
    val mainsString: StringModelVariant?,
    val crossString: StringModelVariant?,
    val formattedDate: CharSequence?,
    val saveEnabled: Boolean,
    val mainTensionErr: Boolean,
    val crossTensionErr: Boolean,
    val unitErr: Boolean
) {
    val dateErr: Boolean = formattedDate == null
    val mainsErr: Boolean = mainsString == null
    val crossErr: Boolean = crossString == null
}

sealed interface RestringRacquetEvent {
    object Saved: RestringRacquetEvent
}

class RestringRacquetViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<RestringRacquetScreen>(_key) {
    private val _events = Channel<RestringRacquetEvent>(Channel.BUFFERED)
    val events: Flow<RestringRacquetEvent> = _events.receiveAsFlow()

    val mainsId = savedStateHandle.getStateFlow<Long>("mains_id")
    val crossId = savedStateHandle.getStateFlow<Long>("cross_id")
    val stringDate = savedStateHandle.getStateFlow<Long>("string_date")
    val stringUnitIx = savedStateHandle.getStateFlow<Int>("string_unit_ix")
    private val mainTensionErr: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val crossTensionErr: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val isSaving = MutableStateFlow(false)

    @Suppress("UNCHECKED_CAST")
    val uiState: StateFlow<RestringRacquetState?> = combine(
        database.stringVariantQueries.selectAllModelVariant()
            .asFlow()
            .mapToList(Dispatchers.IO)
            .map { strings ->
                val modelStrings = strings.toDomain()
                modelStrings to modelStrings.associateBy { it.variant_id } },

        database.racquetQueries.selectRacquetInfo(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO),

        mainsId,
        crossId,
        stringDate,
        mainTensionErr,
        crossTensionErr,
        stringUnitIx,
        isSaving
    ) { combined ->
        val stringsPair = combined[0] as Pair<List<StringModelVariant>, Map<Long, StringModelVariant>>
        val racquet = combined[1] as SelectRacquetInfo
        val mainsIdValue = combined[2] as Long?
        val crossIdValue = combined[3] as Long?
        val date = combined[4] as Long?
        val mainTensErr = combined[5] as Boolean
        val crossTensErr = combined[6] as Boolean
        val unitIx = combined[7] as Int?
        val isSaving = combined[8] as Boolean
        val (strings, stringsMap) = stringsPair

        val mains = stringsMap[mainsIdValue]
        val crosses = stringsMap[crossIdValue]
        val formattedDate = date?.let {
            DateUtils.getRelativeTimeSpanString(
                date,
                System.currentTimeMillis(),
                DateUtils.MINUTE_IN_MILLIS,
                DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_SHOW_YEAR
            )
        }
        val saveDisabled = isSaving || mainsIdValue == null || crossIdValue == null || date == null ||
                mainTensErr || crossTensErr || unitIx == null

        RestringRacquetState(
            racquetInfo = with (racquet) {
                DroidTextProvider.stringId(
                    R.string.racquet_identifier_brand_model,
                    identifier,
                    brand,
                    model
                )
            },
            stringVariants = strings,
            mainsString = mains,
            crossString = crosses,
            formattedDate = formattedDate,
            saveEnabled = !saveDisabled,
            mainTensionErr = mainTensErr,
            crossTensionErr = crossTensErr,
            unitErr = unitIx == null
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun updateMainsString(string: StringModelVariant?) {
        mainsId.value = string?.variant_id
    }

    fun updateCrossString(string: StringModelVariant?) {
        crossId.value = string?.variant_id
    }

    fun setCrossTension(tension: Double?) {
        crossTensionErr.value = tension == null
    }

    fun setMainsTension(tension: Double?) {
        mainTensionErr.value = tension == null
    }

    fun restringRacquet(
        mainsTension: Double?,
        crossTension: Double?,
        notes: String?
    ) {
        val mainsId = this.mainsId.value
        val crossId = this.crossId.value
        val stringDate = this.stringDate.value
        val tensionUnit = stringUnitIx.value?.let { Constants.DisplayOptions.STRING_TENSION_UNITS[it] }
        if (mainsId != null && crossId != null && stringDate != null && mainsTension != null && crossTension != null && tensionUnit != null) {
            val internalMainsTension = mainsTension * tensionUnit
            val internalCrossTension = crossTension * tensionUnit
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    database.stringBedQueries.insertStringBed(
                        racquet_id = key.racquetId,
                        mains_id = mainsId,
                        cross_id = crossId,
                        mains_tension = internalMainsTension,
                        cross_tension = internalCrossTension,
                        strung_at = stringDate,
                        notes = notes?.ifEmpty { null }
                    )
                    _events.send(RestringRacquetEvent.Saved)
                } catch (e: Exception) {
                    isSaving.value = false
                }
            }
        }
    }
}