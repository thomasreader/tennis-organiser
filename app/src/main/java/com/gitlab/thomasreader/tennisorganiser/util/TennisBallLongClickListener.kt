package com.gitlab.thomasreader.tennisorganiser.util

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.view.marginEnd
import com.gitlab.thomasreader.tennisorganiser.core.anim.TennisBallBounceInterpolator
import kotlin.random.Random

class TennisBallLongClickListener: View.OnLongClickListener {
    val upDuration = 450L
    val downDuration = (upDuration / 0.172845f).toLong()

    override fun onLongClick(view: View): Boolean {
        val maxHeight = view.y.toInt()
        val end = view.x.toInt()
        val height = Random.Default.nextInt(maxHeight * 3 / 4, maxHeight)
        val start = -end.toFloat()
        val marginEnd = view.marginEnd.toFloat()
        val sideBounces = Random.Default.nextInt(1, 4)

        val sideBounceValues = FloatArray(sideBounces * 2 + 1).apply {
            this[0] = 0f
            this[1] = start
            this[size - 1] = 0f
            if (sideBounces >= 2) {
                this[2] = marginEnd
                this[3] = start
            }
            if (sideBounces >= 3) {
                this[4] = marginEnd
                this[5] = start
            }
            if (sideBounces >= 4) {
                this[6] = marginEnd
                this[7] = start
            }
        }

        val animatorUp = AnimatorSet().apply {
            interpolator = DecelerateInterpolator()
            duration = upDuration
            play(ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, -height.toFloat()))
        }
        val animatorDown = AnimatorSet().apply {
            interpolator = TennisBallBounceInterpolator()
            duration = downDuration
            play(ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, 0f))
        }
        val animatorSidetoSide = ObjectAnimator.ofFloat(
            view, View.TRANSLATION_X,
            *sideBounceValues
        ).apply {
            interpolator = DecelerateInterpolator()
            duration = upDuration + downDuration
        }
        AnimatorSet().apply {
            playSequentially(animatorUp, animatorDown)
            play(animatorSidetoSide)
        }.start()

        return true
    }
}