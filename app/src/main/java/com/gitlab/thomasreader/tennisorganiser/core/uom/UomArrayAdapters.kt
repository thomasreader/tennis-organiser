package com.gitlab.thomasreader.tennisorganiser.core.uom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit

interface UomArrayAdapters {
    companion object {
        @JvmStatic
        fun list(context: Context, abbreviation: UomAbbreviation, units: List<DroidUnit<*>>): ArrayAdapter<DroidUnit<*>> {
            return UomArrayAdapter(
                context,
                abbreviation,
                android.R.layout.simple_list_item_single_choice,
                units
            )
        }

        @JvmStatic
        fun dropDown(context: Context, abbreviation: UomAbbreviation, units: List<DroidUnit<*>>): ArrayAdapter<DroidUnit<*>> {
            return UomArrayAdapter(
                context,
                abbreviation,
                android.R.layout.simple_dropdown_item_1line,
                units
            )
        }
    }
}

enum class UomAbbreviation {
    SYMBOL, TITLE, DESC
}

private class UomArrayAdapter(
    context: Context,
    private val abbreviation: UomAbbreviation,
    @LayoutRes private val layoutRes: Int,
    private val units: List<DroidUnit<*>>
): ArrayAdapter<DroidUnit<*>>(context, layoutRes, units) {
    fun formatUnit(uom: DroidUnit<*>): String {
        return when (abbreviation) {
            UomAbbreviation.SYMBOL -> uom.symbol ?: context.resources.getString(uom.nameId)
            UomAbbreviation.TITLE -> context.resources.getString(uom.nameId)
            UomAbbreviation.DESC -> {
                with(context.resources) {
                    getString(R.string.uom_name_symbol_format, getString(uom.nameId), uom.symbol)
                }
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textViewRow = (convertView ?: LayoutInflater.from(context)
            .inflate(layoutRes, parent, false)) as TextView
        val uom = units[position]
        textViewRow.text = formatUnit(uom)
        return textViewRow
    }

    override fun getFilter(): Filter {
        return object: Filter() {
            override fun convertResultToString(resultValue: Any?): CharSequence {
                return formatUnit(resultValue as DroidUnit<*>)
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults? {
                return null
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) = Unit
        }
    }


}