package com.gitlab.thomasreader.tennisorganiser.util

fun oneIsNull(a: Any?, b: Any?): Boolean {
    // both null = 2, one null = 0, both not null = -2
    var cumulativeNull = if (a == null) 1 else -1
    cumulativeNull += if (b == null) 1 else -1
    return cumulativeNull == 0
    //return (a == null && b != null) || (a != null && b == null)
}

fun Boolean.toInt(): Int {
    return if (this) 1 else 0
}