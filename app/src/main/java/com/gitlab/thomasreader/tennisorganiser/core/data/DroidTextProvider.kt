package com.gitlab.thomasreader.tennisorganiser.core.data

import android.content.Context
import android.content.res.Resources
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import com.gitlab.thomasreader.tennisorganiser.R

fun String.toDroidTextResource(): DroidTextProvider {
    return DroidTextProvider.text(this)
}

sealed class DroidTextProvider: ContextStringProvider {
    override fun toString(context: Context): String = this.toString(context.resources)
    abstract fun toString(resources: Resources): String

    companion object {
        fun text(text: String): DroidTextProvider = StringTextResource(text)

        fun stringId(@StringRes id: Int): DroidTextProvider = IdTextResource(id)

        fun stringId(@StringRes id: Int, vararg args: Any): DroidTextProvider {
            return IdArgsTextResource(id, args)
        }

        fun pluralId(@PluralsRes id: Int, quantity: Int): DroidTextProvider {
            return PluralTextResource(id, quantity)
        }

        fun pluralId(@PluralsRes id: Int, quantity: Int, vararg args: Any): DroidTextProvider {
            return PluralArgsTextResource(id, quantity, args)
        }

        @JvmField
        val REQUIRED: DroidTextProvider = stringId(R.string.required)
    }
}

private data class StringTextResource(
    private val text: String
) : DroidTextProvider() {
    override fun toString(resources: Resources): String = text
}

private data class IdTextResource(
    @StringRes private val id: Int
) : DroidTextProvider() {
    override fun toString(resources: Resources): String {
        return resources.getString(id)
    }
}

private data class IdArgsTextResource(
    @StringRes private val id: Int,
    private val args: Array<out Any>
) : DroidTextProvider() {
    override fun toString(context: Context): String {
        if (args.any { it is ContextStringProvider }) {
            val formattedArgs = Array<Any>(args.size) { ix ->
                val arg = args[ix]
                if (arg is ContextStringProvider) {
                    arg.toString(context)
                } else {
                    arg
                }
            }
            return context.resources.getString(id, *formattedArgs)
        } else {
            return context.resources.getString(id, *args)
        }
    }

    override fun toString(resources: Resources): String {
        if (args.any { it is DroidTextProvider }) {
            val formattedArgs = Array<Any>(args.size) { ix ->
                val arg = args[ix]
                if (arg is DroidTextProvider) {
                    arg.toString(resources)
                } else {
                    arg
                }
            }
            return resources.getString(id, *formattedArgs)
        } else {
            return resources.getString(id, *args)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as IdArgsTextResource

        if (id != other.id) return false
        if (!args.contentEquals(other.args)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + args.contentHashCode()
        return result
    }
}

private data class PluralTextResource(
    @PluralsRes private val id: Int,
    private val quantity: Int
) : DroidTextProvider() {
    override fun toString(resources: Resources): String {
        return resources.getQuantityString(id, quantity)
    }
}

private data class PluralArgsTextResource(
    @PluralsRes private val id: Int,
    private val quantity: Int,
    private val args: Array<out Any>
) : DroidTextProvider() {
    override fun toString(resources: Resources): String {
        if (args.any { it is DroidTextProvider }) {
            val formattedArgs = Array<Any>(args.size) { ix ->
                val arg = args[ix]
                if (arg is DroidTextProvider) {
                    arg.toString(resources)
                } else {
                    arg
                }
            }
            return resources.getQuantityString(id, quantity, *formattedArgs)
        } else {
            return resources.getQuantityString(id, quantity, *args)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PluralArgsTextResource

        if (id != other.id) return false
        if (quantity != other.quantity) return false
        if (!args.contentEquals(other.args)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + quantity
        result = 31 * result + args.contentHashCode()
        return result
    }


}

