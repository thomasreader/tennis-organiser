package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.edit

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.extension.action
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomAbbreviation
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomArrayAdapters
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetAddBinding
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.nullStringRes
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class EditRacquetFragment: KeyedFragment(R.layout.racquet_add) {
    private val binding by bindView(RacquetAddBinding::bind)
    private val saveButton: ActionMenuItemView get() = binding.toolbar.action(R.id.save_action)
    private lateinit var viewModel: EditRacquetViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        with (binding) {
            weightUnit.setAdapter(
                UomArrayAdapters.dropDown(
                    requireContext(),
                    UomAbbreviation.SYMBOL,
                    Constants.DisplayOptions.RACQUET_WEIGHT_UNITS
                )
            )
            weightUnit.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.weightUnitIx = position
                }

            balancePointUnitInput.setAdapter(
                UomArrayAdapters.dropDown(
                    requireContext(),
                    UomAbbreviation.SYMBOL,
                    Constants.DisplayOptions.RACQUET_BALANCE_UNITS
                )
            )
            balancePointUnitInput.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.balancePointUnitIx = position
                }

            idInput.doOnTextChanged { text, _, _, _ ->
                viewModel.updateIdentifier(text?.toString())
            }

            toolbar.title = resources.getString(R.string.edit_racquet)
            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        editRacquet()
                        true
                    }
                    else -> false
                }
            }
        }
    }

    private fun editRacquet() {
        val identifier = binding.idInput.text?.toString()
        val weight = binding.weightInput.text?.toString()?.toDoubleOrNull()
        val swingWeight = binding.swingWeightInput.text?.toString()?.toIntOrNull()
        val balancePoint = binding.balancePointInput.text?.toString()?.toDoubleOrNull()
        val notes = binding.notesInput.text?.toString()

        viewModel.editRacquet(
            identifier = identifier,
            weight = weight,
            swingWeight = swingWeight,
            balancePoint = balancePoint,
            notes = notes
        )
    }


    private fun initViewModel() {
        viewModel = savedViewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            binding.toolbar.subtitle = state.racquetInfoRes.toString(resources)
                            binding.idLayout.updateErrorVisibility(
                                state.idError,
                                DroidTextProvider.REQUIRED
                            )
                            saveButton.isEnabled = state.saveEnabled

                            if (state.initialValues != null) {
                                with(state.initialValues) {
                                    binding.idInput.setText(identifier)
                                    binding.weightInput.setText(weight)
                                    binding.weightUnit.setText(
                                        (binding.weightUnit.adapter as ArrayAdapter<*>).filter.convertResultToString(
                                            weightUnit
                                        )
                                    )
                                    binding.swingWeightInput.setText(swingWeight)
                                    binding.balancePointInput.setText(balancePoint)
                                    binding.balancePointUnitInput.setText(
                                        (binding.balancePointUnitInput.adapter as ArrayAdapter<*>).filter.convertResultToString(
                                            balancePointUnit
                                        )
                                    )
                                    binding.notesInput.setText(notes)
                                }
                            }
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            EditRacquetEvent.Saved -> backstack.goBack()
                        }
                    }
                }
            }
        }
    }

    private fun assertGoBack() {
        AlertDialog.Builder(requireContext())
            .setTitle("Discard edit?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}