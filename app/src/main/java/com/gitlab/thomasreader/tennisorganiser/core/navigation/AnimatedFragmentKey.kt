package com.gitlab.thomasreader.tennisorganiser.core.navigation

import android.transition.Fade
import android.transition.Transition
import android.transition.TransitionSet
import com.gitlab.thomasreader.tennisorganiser.R
import com.google.android.material.transition.platform.MaterialFadeThrough
import com.google.android.material.transition.platform.MaterialSharedAxis
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey

private const val toolbarTransitionLength: Long = 500
private const val contentTransitionLength: Long = 600

abstract class AnimatedFragmentKey : DefaultFragmentKey() {
    open fun hideBottomNavigation(): Boolean = false

    open fun contentEnterTransition(previous: AnimatedFragmentKey): Transition? =
        MaterialSharedAxis(MaterialSharedAxis.Z, true)
    open fun contentExitTransition(next: AnimatedFragmentKey): Transition? =
        MaterialSharedAxis(MaterialSharedAxis.Z, true)
    open fun contentPopEnterTransition(previous: AnimatedFragmentKey): Transition? =
        MaterialSharedAxis(MaterialSharedAxis.Z, false)
    open fun contentPopExitTransition(next: AnimatedFragmentKey): Transition? =
        MaterialSharedAxis(MaterialSharedAxis.Z, false)
    open fun contentReplaceEnterTransition(previous: AnimatedFragmentKey): Transition? =
        MaterialFadeThrough()
    open fun contentReplaceExitTransition(next: AnimatedFragmentKey): Transition? =
        MaterialFadeThrough()

    fun enterTransition(previous: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentEnterTransition(previous)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }

    fun exitTransition(next: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentExitTransition(next)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }

    fun popEnterTransition(previous: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentPopEnterTransition(previous)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }

    fun popExitTransition(next: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentPopExitTransition(next)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }

    fun replaceEnterTransition(previous: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentReplaceEnterTransition(previous)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }

    fun replaceExitTransition(next: AnimatedFragmentKey): Transition? = TransitionSet().apply {
        contentReplaceExitTransition(next)?.let {
            addTransition(it.apply {
                excludeTarget(R.id.toolbar, true)
                duration = contentTransitionLength
            })
        }
        addTransition(Fade().apply {
            addTarget(R.id.toolbar)
            duration = toolbarTransitionLength
        })
    }
}