package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.list

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.data.SelectBasicModels
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import logcat.logcat

typealias BasicRacquetModel = SelectBasicModels
val BasicRacquetModel.brandModelRes get() = DroidTextProvider.stringId(R.string.brand_model_join, brand, model)

class RacquetModelListViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
): KeyedViewModel<AddRacquetModelListScreen>(_key) {
    val uiState: StateFlow<List<BasicRacquetModel>?> = database.racquetModelQueries.selectBasicModels()
        .asFlow()
        .mapToList(Dispatchers.IO)
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)
}