package com.gitlab.thomasreader.tennisorganiser.domain.string

import com.gitlab.thomasreader.kuantity.quantity.format.formatSymbol
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.String_variant
import java.text.DecimalFormat

data class StringVariant(
    val id: Long,
    val modelId: Long,
    val thickness: String,
    val thicknessWithUnit: String,
    val rating: String?,
    val notes: String?
)

fun String_variant.toDomain(ratingFormat: DecimalFormat, thicknessFormat: DecimalFormat): StringVariant {
    val ratingString = this.rating?.let { ratingFormat.format(it) }
    val thickness = thicknessFormat.format(this.thickness.convertTo(DroidUnits.LengthUnits.MILLIMETRE))
    val thicknessString = DroidUnits.LengthUnits.MILLIMETRE.formatSymbol(thickness)
    return StringVariant(this.id, this.model_id, thickness, thicknessString, ratingString, this.notes)
}