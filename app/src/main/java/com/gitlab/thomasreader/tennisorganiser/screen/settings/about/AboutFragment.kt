package com.gitlab.thomasreader.tennisorganiser.screen.settings.about

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R

import com.gitlab.thomasreader.tennisorganiser.databinding.AboutBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.zhuinden.simplestackextensions.fragments.KeyedFragment

class AboutFragment: KeyedFragment(R.layout.about) {
    private val binding: AboutBinding by bindView(AboutBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with (binding) {
            val concatAdapter = ConcatAdapter(
                AboutIconInfoAdapter(),
                AboutInfoAdapter(),
                AboutHeaderAdapter(),
                AboutLibraryAdapter()
            )

            aboutRecyclerContent.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = concatAdapter
            }
        }
    }
}