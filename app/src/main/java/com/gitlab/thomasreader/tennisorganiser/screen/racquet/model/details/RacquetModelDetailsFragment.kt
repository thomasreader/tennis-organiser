package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.details

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetModelDetailsBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.edit.EditRacquetModelScreen
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list.RacquetListScreen
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RacquetModelDetailsFragment: KeyedFragment(R.layout.racquet_model_details) {
    private val binding by bindView(RacquetModelDetailsBinding::bind)
    private lateinit var viewModel: RacquetModelDetailsViewModel
    private val infoAdapter: RacquetModelDetailsAdapter = RacquetModelDetailsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.edit_action -> {
                        backstack.goTo(EditRacquetModelScreen(viewModel.key.modelId))
                    }
                    R.id.delete_action -> {
                        confirmDeleteAlert()
                    }
                    else -> false
                }
                true
            }
            toolbar.title = getString(R.string.racquet_model_details)

            racquetInfoRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = infoAdapter
            }
        }
    }

    private fun initViewModel() {
        viewModel = viewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            binding.toolbar.subtitle = state.brandModelRes.toString(requireContext())
                            infoAdapter.submitList(state.details)
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            RacquetModelDetailsEvent.Deleted -> backstack.goTo(RacquetListScreen())
                        }
                    }
                }
            }
        }
    }

    private fun confirmDeleteAlert() {
            AlertDialog.Builder(requireContext())
                .setTitle(R.string.delete_racquet_model)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    viewModel.deleteRacquetModel()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()

    }
}