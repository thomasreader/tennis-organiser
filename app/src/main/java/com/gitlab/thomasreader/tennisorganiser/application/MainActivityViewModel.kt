package com.gitlab.thomasreader.tennisorganiser.application

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.tennisorganiser.core.data.DataStoreKeys
import com.gitlab.thomasreader.tennisorganiser.core.data.get
import kotlinx.coroutines.flow.*

class MainActivityViewModel(
    private val dataStore: DataStore<Preferences>
): ViewModel() {
    val darkMode: Flow<Int> = dataStore.data
        .map { preferences -> preferences[DataStoreKeys.DARK_MODE] }
        .apply { launchIn(viewModelScope) }

    private val _hiddenBottomNavigation: MutableStateFlow<Boolean> = MutableStateFlow(false).apply {
        launchIn(viewModelScope)
    }
    val hiddenBottomNavigation: StateFlow<Boolean> = _hiddenBottomNavigation

    fun setBottomNavVisibility(hidden: Boolean) {
        val previous = _hiddenBottomNavigation.value
        if (previous != hidden) {
            _hiddenBottomNavigation.value = hidden
        }
    }
}
