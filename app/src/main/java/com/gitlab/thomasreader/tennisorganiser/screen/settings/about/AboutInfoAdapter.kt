package com.gitlab.thomasreader.tennisorganiser.screen.settings.about

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.ContextStringProvider
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.databinding.AboutInfoBinding
import com.gitlab.thomasreader.tennisorganiser.domain.SoftwareLicense
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible

class AboutInfoAdapter: RecyclerView.Adapter<AboutInfoAdapter.ViewHolder>() {
    private val aboutInformation = listOf(
        versionInfo(),
        licenseInfo(),
        sourceCodeInfo()
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, AboutInfoBinding::inflate))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(aboutInformation[position])
    }

    override fun getItemCount(): Int = aboutInformation.size

    inner class ViewHolder(binding: AboutInfoBinding): BindingViewHolder<AboutInfoBinding>(binding) {
        @SuppressLint("UseCompatLoadingForDrawables")
        fun bind(info: AboutInfo) {
            with (binding) {
                infoTitle.setText(info.title)
                val context = itemView.context
                val drawable = context.resources.getDrawable(info.icon, context.theme)
                infoImage.setImageDrawable(drawable)
                root.setOnClickListener(info.onClickListener)
                if (info.miscIcon != null) {
                    val miscDrawable = context.resources.getDrawable(info.miscIcon, context.theme)
                    infoMiscImage.setImageDrawable(miscDrawable)
                    infoMiscImage.visible()
                } else {
                    infoMiscImage.gone()
                }
                infoDesc.text = info.description.toString(context)
            }
        }
    }
}

data class AboutInfo(
    @StringRes val title: Int,
    val description: ContextStringProvider,
    @DrawableRes val icon: Int,
    val onClickListener: View.OnClickListener?
) {
    @DrawableRes val miscIcon: Int? = onClickListener?.let { R.drawable.ic_baseline_open_in_new_24 }
}

private const val projectUrl: String = "https://gitlab.com/thomasreader/tennis-organiser"

private fun versionInfo(): AboutInfo {
    return AboutInfo(
        title = R.string.version,
        description = ContextStringProvider.VERSION_PROVIDER,
        icon = R.drawable.ic_baseline_info_24,
        onClickListener = null
    )
}

private fun licenseInfo(): AboutInfo {
    return AboutInfo(
        title = R.string.license,
        description = DroidTextProvider.stringId(R.string.gpl_3),
        icon = R.drawable.ic_gnu,
    ) { view ->
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(SoftwareLicense.GPL_3_0.url))
        view.context.startActivity(intent)
    }
}

private fun sourceCodeInfo(): AboutInfo {
    return AboutInfo(
        title = R.string.source_code,
        description = DroidTextProvider.text(projectUrl),
        icon = R.drawable.ic_gitlab,
    ) { view ->
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(projectUrl))
        view.context.startActivity(intent)
    }
}