package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.edit

import android.database.sqlite.SQLiteException
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.times
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.data.GripSize
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

sealed interface EditRacquetEvent {
    object Saved: EditRacquetEvent
}

data class EditRacquetState(
    val racquetInfoRes: DroidTextProvider,
    val idError: Boolean,
    val saveEnabled: Boolean,
    val initialValues: EditRacquetInitialValues?
)

data class EditRacquetInitialValues(
    val identifier: String,
    val weight: String?,
    val weightUnit: DroidUnit<Mass>,
    val swingWeight: String?,
    val balancePoint: String?,
    val balancePointUnit: DroidUnit<Length>,
    val notes: String?
)

class EditRacquetViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<EditRacquetScreen>(_key) {
    private val _events = Channel<EditRacquetEvent>(Channel.BUFFERED)
    val events: Flow<EditRacquetEvent> = _events.receiveAsFlow()

    private val isInit = savedStateHandle.getStateFlow("initiated", false)
    var weightUnitIx: Int? = savedStateHandle["weight_unit_ix"]
        set(value) {
            println("weightUnitIx: $value")
            if (field != value) {
                field = value
                savedStateHandle["weight_unit_ix"] = value
            }
        }

    var balancePointUnitIx: Int? = savedStateHandle["balance_point_unit_ix"]
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["balance_point_unit_ix"] = value
            }
        }

    private val identifierError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val isSaving: MutableStateFlow<Boolean> = MutableStateFlow(false)

    private val initialValuesFlow = combine(
        isInit,
        database.racquetQueries.selectRacquetById(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        dataStore.data
    ) { initiated, racquet, preferences ->
        return@combine when (initiated) {
            true -> null
            false -> {
                val weightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
                val balancePointUnit = preferences.getMapped(DataStoreKeys.RACQUET_BALANCE_POINT)
                val weight = racquet.weight?.convertTo(weightUnit)?.let {
                    Constants.DisplayOptions.Format.weightFormatter(weightUnit).format(it)
                }
                val balancePoint = racquet.balance_point?.convertTo(balancePointUnit)?.let {
                    Constants.DisplayOptions.Format.balancePointFormatter(balancePointUnit).format(it)
                }

                EditRacquetInitialValues(
                    identifier = racquet.identifier,
                    weight = weight,
                    weightUnit = weightUnit,
                    swingWeight = racquet.swing_weight?.toString(),
                    balancePoint = balancePoint,
                    balancePointUnit = balancePointUnit,
                    notes = racquet.notes
                )
            }
        }
    }
        .onEach { isInit.value = true }
        .catch { logcat { it.stackTraceToString() } }

    val uiState = combine(listOf(
        database.racquetQueries.selectRacquetInfo(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO)
            .map {
                DroidTextProvider.stringId(
                    R.string.racquet_identifier_brand_model,
                    it.identifier,
                    it.brand,
                    it.model
                )
            },
        initialValuesFlow,
        identifierError,
        isSaving,
        dataStore.data)
    ) {
        val racquetInfo = it[0] as DroidTextProvider
        val initialVals = it[1] as EditRacquetInitialValues
        val idError = it[2] as Boolean
        val saving = it[3] as Boolean
        val preferences = it[4] as Preferences

        val saveDisabled = saving || idError
        if (initialVals != null) {
            weightUnitIx = Constants.DisplayOptions.RACQUET_WEIGHT_UNITS.indexOf(initialVals.weightUnit)
            balancePointUnitIx = Constants.DisplayOptions.RACQUET_BALANCE_UNITS.indexOf(initialVals.balancePointUnit)
        }
        return@combine EditRacquetState(
            racquetInfoRes = racquetInfo,
            idError = idError,
            saveEnabled = !saveDisabled,
            initialValues = initialVals
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun updateIdentifier(
        identifier: String?
    ) {
        this.identifierError.value = identifier.isNullOrBlank()
    }

    fun editRacquet(
        identifier: String?,
        weight: Double?,
        swingWeight: Int?,
        balancePoint: Double?,
        notes: String?
    ) {
        val weightUnit = this.weightUnitIx?.let { Constants.DisplayOptions.RACQUET_WEIGHT_UNITS[it] }
        val balancePointUnit = this.balancePointUnitIx?.let { Constants.DisplayOptions.RACQUET_BALANCE_UNITS[it] }

        val internalWeight = when (weight != null && weightUnit != null) {
            true -> weight * weightUnit
            else -> null
        }
        val internalBalancePoint =  when (balancePoint != null && balancePointUnit != null) {
            true -> balancePoint * balancePointUnit
            else -> null
        }
        if (!identifier.isNullOrBlank()) {
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                try {
                    database.racquetQueries.updateRacquet(
                        identifier = identifier,
                        weight = internalWeight,
                        swing_weight = swingWeight,
                        balance_point = internalBalancePoint,
                        notes = notes,
                        id = key.racquetId
                    )
                    _events.send(EditRacquetEvent.Saved)
                } catch (sqlException: SQLiteException) {
                    isSaving.value = false
                }
            }
        }
    }
}
