package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.list

import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class AddRacquetModelListScreen: AnimatedFragmentKey() {
    override fun instantiateFragment() = AddRacquetModelListFragment()
    override fun hideBottomNavigation(): Boolean = true
}