package com.gitlab.thomasreader.tennisorganiser.core.anim

import android.animation.*
import android.transition.TransitionValues
import android.transition.Visibility
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import com.google.android.material.transition.platform.FadeThroughProvider

private fun AnimatorSet.playTogetherCompat(animators: MutableList<Animator>) {

    // Fix for pre-M bug where animators with start delay are not played correctly in an
    // AnimatorSet.
    var totalDuration: Long = 0

    var i = 0
    val count: Int = animators.size
    while (i < count) {
        val animator: Animator = animators.get(i)
        totalDuration = totalDuration.coerceAtLeast(animator.startDelay + animator.duration)
        i++
    }

    val fix: Animator = ValueAnimator.ofInt(0, 0)
    fix.duration = totalDuration
    animators.add(0, fix)

    this.playTogether(animators)
}

class SlideTransition(
    val isForward: Boolean
) : Visibility() {

    override fun onAppear(
        sceneRoot: ViewGroup,
        view: View,
        startValues: TransitionValues?,
        endValues: TransitionValues?
    ): Animator? {
        return createAnimator(sceneRoot, view, true)
    }

    override fun onDisappear(
        sceneRoot: ViewGroup,
        view: View,
        startValues: TransitionValues?,
        endValues: TransitionValues?
    ): Animator? {
        return createAnimator(sceneRoot, view, false)
    }

    private fun createAnimator(sceneRoot: ViewGroup, view: View, appearing: Boolean): Animator? {
        val set = AnimatorSet()
        val animators: MutableList<Animator> = ArrayList()
        val primaryAnimator: Animator = when (appearing) {
            true -> {
                when (isForward) {
                    true ->
                        ObjectAnimator.ofPropertyValuesHolder(
                            view,
                            PropertyValuesHolder.ofFloat(
                                View.TRANSLATION_X,
                                sceneRoot.width.toFloat(),
                                0f
                            )
                        )
                    false -> {
                        val rootWidth = sceneRoot.width

                        animators.add(ObjectAnimator.ofPropertyValuesHolder(
                            view,
                            PropertyValuesHolder.ofInt(
                                "ScrollX",
                                -(rootWidth * 0.6).toInt(),
                                0
                            )
                        ))


                        ObjectAnimator.ofPropertyValuesHolder(
                            view,
                            PropertyValuesHolder.ofFloat(
                                View.TRANSLATION_X,
                                -rootWidth.toFloat(),
                                0f
                            )
                        )
                    }
                }

            }
            false -> {
                ViewCompat.setElevation(view, 0.0f)
                when (isForward) {
                    true -> {
                        val rootWidth = sceneRoot.width

                        animators.add(ObjectAnimator.ofPropertyValuesHolder(
                            view,
                            PropertyValuesHolder.ofInt(
                                "ScrollX",
                                0,
                                -(rootWidth * 0.6).toInt()
                            )
                        ))

                        ObjectAnimator.ofPropertyValuesHolder(
                            view,
                            PropertyValuesHolder.ofFloat(
                                View.TRANSLATION_X,
                                0f,
                                -rootWidth.toFloat()
                            )
                        )
                    }
                    false -> ObjectAnimator.ofPropertyValuesHolder(
                        view,
                        PropertyValuesHolder.ofFloat(
                            View.TRANSLATION_X,
                            0f,
                            sceneRoot.width.toFloat(),
                        )
                    )
                }
            }
        }
        animators.add(primaryAnimator)
        if (appearing && isForward) {
            FadeThroughProvider().createAppear(sceneRoot, view)?.let {
                animators.add(it)
            }
        }
        set.playTogetherCompat(animators)
        return set
    }
}