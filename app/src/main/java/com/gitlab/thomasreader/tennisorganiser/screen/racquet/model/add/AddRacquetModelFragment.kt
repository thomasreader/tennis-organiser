package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.add

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel

import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.extension.action
import com.gitlab.thomasreader.tennisorganiser.core.extension.onItemClickListener
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateAdapterValues
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomAbbreviation
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomArrayAdapters
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetModelAddBinding
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add.AddRacquetScreen
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AddRacquetModelFragment: KeyedFragment(R.layout.racquet_model_add) {
    private val binding by bindView(RacquetModelAddBinding::bind)
    private val saveButton: ActionMenuItemView get() = binding.toolbar.action(R.id.save_action)
    private lateinit var viewModel: AddRacquetModelViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)

        with (binding) {
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        saveRacquetModel()
                        true
                    }
                    else -> false
                }
            }

            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }

            racquetBrandInput.doOnTextChanged { text, _, _, _ ->
                viewModel.validateBrand(text?.toString())
            }
            racquetModelInput.doOnTextChanged { text, _, _, _ ->
                viewModel.validateModel(text?.toString())
            }

            viewModel.validateBrand(racquetBrandInput.text?.toString())
            viewModel.validateModel(racquetModelInput.text?.toString())

            weightUnit.setAdapter(
                UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.SYMBOL,
                Constants.DisplayOptions.RACQUET_WEIGHT_UNITS
            ))
            weightUnit.onItemClickListener<DroidUnit<Mass>>(viewModel::weightUnit::set)

            lengthUnit.setAdapter(UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.SYMBOL,
                Constants.DisplayOptions.RACQUET_LENGTH_UNITS
            ))
            lengthUnit.onItemClickListener<DroidUnit<Length>>(viewModel::lengthUnit::set)

            headSizeUnit.setAdapter(UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.SYMBOL,
                Constants.DisplayOptions.RACQUET_HEAD_SIZE_UNITS
            ))
            headSizeUnit.onItemClickListener<DroidUnit<Area>>(viewModel::headSizeUnit::set)

            balancePointUnitInput.setAdapter(UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.SYMBOL,
                Constants.DisplayOptions.RACQUET_BALANCE_UNITS
            ))
            balancePointUnitInput.onItemClickListener<DroidUnit<Length>>(viewModel::balancePointUnit::set)

            tensionUnitInput.setAdapter(UomArrayAdapters.dropDown(
                requireContext(),
                UomAbbreviation.SYMBOL,
                Constants.DisplayOptions.STRING_TENSION_UNITS
            ))
            tensionUnitInput.onItemClickListener<DroidUnit<Mass>>(viewModel::tensionUnit::set)
        }
    }

    fun assertGoBack() {
        AlertDialog.Builder(requireContext())
            .setTitle("Discard entry?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun initViewModel() {
        viewModel = savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            if (binding.racquetBrandInput.adapter == null) {
                                val adapter = ArrayAdapter<String>(
                                    requireContext(),
                                    android.R.layout.simple_dropdown_item_1line,
                                    state.brands
                                )
                                binding.racquetBrandInput.setAdapter(adapter)
                                binding.racquetBrandInput.setTag(R.id.autocomplete_current_list, state.brands)
                            } else {
                                binding.racquetBrandInput.updateAdapterValues(state.brands)
                            }
                            binding.racquetBrandLayout.updateErrorVisibility(state.brandErr, DroidTextProvider.REQUIRED)
                            binding.racquetModelLayout.updateErrorVisibility(state.modelErr, DroidTextProvider.REQUIRED)
                            saveButton.isEnabled = state.saveEnabled
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            is AddRacquetModelEvent.Saved -> {
                                backstack.replaceTop(AddRacquetScreen(event.modelId), StateChange.FORWARD)
                            }
                        }
                    }
                }
            }
        }

    }

    private fun saveRacquetModel() {
        val brand = binding.racquetBrandInput.text?.toString()
        val model = binding.racquetModelInput.text?.toString()
        val weight = binding.weightInput.text?.toString()?.toDoubleOrNull()
        val length = binding.lengthInput.text?.toString()?.toDoubleOrNull()
        val headSize = binding.headSizeInput.text?.toString()?.toDoubleOrNull()
        val balancePoint = binding.balancePointInput.text?.toString()?.toDoubleOrNull()
        val swingWeight = binding.swingWeightInput.text?.toString()?.toIntOrNull()
        val stiffness = binding.stiffnessInput.text?.toString()?.toIntOrNull()
        val tipWidth = binding.tipWidthInput.text?.toString()?.toDoubleOrNull()
        val beamWidth = binding.beamWidthInput.text?.toString()?.toDoubleOrNull()
        val shaftWidth = binding.shaftWidthInput.text?.toString()?.toDoubleOrNull()
        val numMains = binding.numberMainsInput.text?.toString()?.toIntOrNull()
        val numCrosses = binding.numberCrossInput.text?.toString()?.toIntOrNull()
        val tensionLow = binding.tensionLowInput.text?.toString()?.toDoubleOrNull()
        val tensionHigh = binding.tensionHighInput.text?.toString()?.toDoubleOrNull()
        val mainsSkipThroat = binding.mainsSkipThroatInput.text?.toString()?.toIntOrNull()
        val mainsSkipHead = binding.mainsSkipHeadInput.text?.toString()?.toIntOrNull()
        val stringPieces = binding.stringPiecesInput.text?.toString()?.toIntOrNull()
        val sharedHoles = binding.sharedHolesInput.text?.toString()?.toIntOrNull()

        viewModel.addRacquetModel(
            brand = brand,
            model = model,
            weight = weight,
            length = length,
            headSize = headSize,
            balancePoint = balancePoint,
            swingWeight = swingWeight,
            stiffness = stiffness,
            widthBeam = beamWidth,
            widthTip = tipWidth,
            widthShaft = shaftWidth,
            numMains = numMains,
            numCrosses = numCrosses,
            tensionLow = tensionLow,
            tensionHigh = tensionHigh,
            mainsSkipThroat = mainsSkipThroat,
            mainsSkipHead = mainsSkipHead,
            stringPieces = stringPieces,
            sharedHoles = sharedHoles
        )
    }
}