package com.gitlab.thomasreader.tennisorganiser.screen.settings.about

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.thomasreader.tennisorganiser.R

class AboutHeaderAdapter: RecyclerView.Adapter<AboutHeaderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.about_library_header, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = Unit

    override fun getItemCount(): Int = 1

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}