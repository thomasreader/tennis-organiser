package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.databinding.DurationInputBinding

class DurationDialog: DialogFragment() {
    var onClickListener: ((seconds: Long) -> Unit)? = null
    var binding: DurationInputBinding? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val sHours: Int? = savedInstanceState?.getInt("hours")
        val sMinutes: Int? = savedInstanceState?.getInt("minutes")
        val seconds: Long = arguments?.getLong("seconds") ?: 0
        val hours: Int
        val minutes: Int
        if (sHours == null && sMinutes == null) {
            hours = (seconds / 3600).toInt()
            minutes = ((seconds - (hours * 3600)) / 60).toInt()
        } else {
            hours = sHours ?: 0
            minutes = sMinutes ?: 0
        }

        binding = DurationInputBinding.inflate(layoutInflater)
        binding!!.hourPicker.apply {
            minValue = 0
            maxValue = 23
            wrapSelectorWheel = true
            value = hours.coerceAtMost(23)
        }
        binding!!.minutePicker.apply {
            minValue = 0
            maxValue = 59
            wrapSelectorWheel = true
            value = minutes.coerceAtMost(59)
        }

        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.duration)
            .setView(binding!!.root)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val binding = binding
                val onClickListener = this.onClickListener
                if (onClickListener != null && binding != null) {
                    onClickListener(
                        binding.hourPicker.value * 3600L + binding.minutePicker.value * 60L
                    )
                }
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding?.let {
            outState.putInt("hours", it.hourPicker.value)
            outState.putInt("minutes", it.minutePicker.value)
        }
    }
}

fun DurationDialog(seconds: Long?): DurationDialog {
    return DurationDialog().apply {
        arguments = Bundle().apply {
            if (seconds != null) {
                putLong("seconds", seconds)
            }
        }
    }
}