package com.gitlab.thomasreader.tennisorganiser.screen.settings.about

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.databinding.AboutLibraryRowBinding
import com.gitlab.thomasreader.tennisorganiser.domain.SoftwareLibrary

class AboutLibraryAdapter: RecyclerView.Adapter<AboutLibraryAdapter.ViewHolder>() {
    val libraries get() = Constants.LIBRARIES

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.about_library_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(libraries[position])
    }

    override fun getItemCount(): Int = libraries.size

    inner class ViewHolder(
        itemView: View
    ): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val binding = AboutLibraryRowBinding.bind(itemView)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(library: SoftwareLibrary) {
            with (binding) {
                libraryName.text = library.name
                libraryAuthor.text = library.author
                libraryLicense.text = library.license.description
            }
        }

        override fun onClick(view: View) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(libraries[bindingAdapterPosition].url))
            view.context.startActivity(intent)
        }
    }
}