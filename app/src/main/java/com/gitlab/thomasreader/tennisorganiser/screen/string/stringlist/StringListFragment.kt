package com.gitlab.thomasreader.tennisorganiser.screen.string.stringlist

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.*
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.anim.animateFABOnScroll
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility

import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.StringAddBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.StringListBinding
import com.gitlab.thomasreader.tennisorganiser.screen.string.stringdetails.StringDetailsScreen
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.screen.settings.SettingsScreen
import com.gitlab.thomasreader.tennisorganiser.util.TennisBallLongClickListener
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class StringListFragment: KeyedFragment(R.layout.string_list) {
    private val binding: StringListBinding by bindView(StringListBinding::bind)
    private lateinit var viewModel: StringListViewModel
    private val stringListAdapter: StringListAdapter = StringListAdapter { stringModel ->
        backstack.goTo(StringDetailsScreen(stringModel.id))
    }
    private var dialog: AlertDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with (binding) {
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.settingsMenu -> {
                        backstack.goTo(SettingsScreen())
                        true
                    }
                    else -> false
                }
            }

            addStringFab.setOnClickListener {
                viewModel.openDialog()
            }
            addStringFab.setOnLongClickListener(TennisBallLongClickListener())

            stringListRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = stringListAdapter
                animateFABOnScroll(addStringFab)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dialog?.dismiss()
        dialog = null
    }

    private fun initViewModel() {
        viewModel = savedViewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            stringListAdapter.submitList(state)
                        }
                    }
                }
                launch {
                    viewModel.isDialogOpenFlow.collect { isOpen ->
                        if (isOpen) {
                            if (dialog == null) openAddDialog()
                        } else {
                            dialog?.dismiss()
                            dialog = null
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            is AddStringModelEvent.Saved -> {
                                viewModel.closeDialog()
                            }
                            is AddStringModelEvent.UniqueConstraintFailed -> {
                                Toast.makeText(
                                    requireContext(),
                                    event.msg.toString(resources),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun openAddDialog() {
        val addStringView = StringAddBinding.inflate(layoutInflater, null, false)

        addStringView.stringBrandInput.setText(viewModel.brand)
        addStringView.stringModelInput.setText(viewModel.model)
        addStringView.stringCompositionInput.setText(viewModel.composition)

        addStringView.stringBrandInput.doOnTextChanged { text, _, _, _ ->
            val brand = text?.toString()
            if (brand != null) {
                viewModel.brand = brand
            }
        }

        addStringView.stringModelInput.doOnTextChanged { text, _, _, _ ->
            val model = text?.toString()
            if (model != null) {
                viewModel.model = model
            }
        }

        addStringView.stringCompositionInput.doOnTextChanged { text, _, _, _ ->
            val composition = text?.toString()
            if (composition != null) {
                viewModel.composition = composition
            }
        }

        var job: Job? = null

        dialog = AlertDialog.Builder(binding.root.context)
            .setTitle(R.string.add_string_model)
            .setView(addStringView.root)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                viewModel.closeDialog()
            }
            .setOnCancelListener { _ ->
                viewModel.closeDialog()
            }
            .setOnDismissListener {
                job?.cancel()
                job = null
            }
            .create()
            .apply {
                setOnShowListener { _ ->
                    val button = getButton(AlertDialog.BUTTON_POSITIVE)
                    button.setOnClickListener { _ ->
                        val brand = addStringView.stringBrandInput.text?.toString()
                        val model = addStringView.stringModelInput.text?.toString()
                        val composition = addStringView.stringCompositionInput.text?.toString()
                        viewModel.addStringModel(brand, model, composition)
                    }
                }
                show()
            }

        job = viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.dialogState.collect { state ->
                    state?.let {
                        if (addStringView.stringBrandInput.adapter == null || addStringView.stringCompositionInput.adapter == null) {
                            val brandAdapter = ArrayAdapter<String>(
                                requireContext(),
                                android.R.layout.simple_dropdown_item_1line,
                                state.brands
                            )
                            val compositionAdapter = ArrayAdapter<String>(
                                requireContext(),
                                android.R.layout.simple_dropdown_item_1line,
                                state.compositions
                            )
                            addStringView.stringBrandInput.setAdapter(brandAdapter)
                            addStringView.stringCompositionInput.setAdapter(compositionAdapter)
                        }
                        addStringView.stringBrandLayout.updateErrorVisibility(state.brandError)
                        addStringView.stringModelLayout.updateErrorVisibility(state.modelError)
                        dialog?.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = state.saveEnabled
                    }
                }
            }
        }
    }
}