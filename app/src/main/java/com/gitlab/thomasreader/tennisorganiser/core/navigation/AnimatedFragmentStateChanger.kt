package com.gitlab.thomasreader.tennisorganiser.core.navigation

import android.os.Handler
import android.os.Looper
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.zhuinden.simplestack.StateChange
import javax.annotation.Nonnull

class AnimatedFragmentStateChanger
/**
 * Constructor.
 *
 * @param fragmentManager the fragment manager
 * @param containerId     the container ID in which fragments are swapped
 */(
    private val fragmentManager: FragmentManager,
    @IdRes private val containerId: Int
) {
    private val handler = Handler(Looper.getMainLooper())

    /**
     * Decide if the fragment is currently added, but not showing.
     *
     * @param fragment the fragment
     * @return if the fragment is not showing
     */
    protected fun isNotShowing(@Nonnull fragment: Fragment): Boolean {
        return fragment.isDetached
    }

    /**
     * Start showing the fragment that is added but not showing.
     *
     * @param fragmentTransaction the fragment transaction
     * @param fragment            the fragment
     */
    protected fun startShowing(
        @Nonnull fragmentTransaction: FragmentTransaction,
        @Nonnull fragment: Fragment?
    ) {
        fragmentTransaction.attach(fragment!!) // show top fragment if already exists
    }

    /**
     * Stop showing the fragment that is added, showing, but should not be showing.
     *
     * @param fragmentTransaction the fragment transaction
     * @param fragment            the fragment
     */
    protected fun stopShowing(
        @Nonnull fragmentTransaction: FragmentTransaction,
        @Nonnull fragment: Fragment?
    ) {
        fragmentTransaction.detach(fragment!!) // destroy view of fragment not top
    }

    /**
     * Handles the transition from one state to another, swapping fragments.
     *
     * @param stateChange the state change
     */
    fun handleStateChange(@Nonnull stateChange: StateChange) {
        var didExecutePendingTransactions = false
        try {
            fragmentManager.executePendingTransactions() // two synchronous immediate fragment transactions can overlap.
            didExecutePendingTransactions = true
        } catch (e: IllegalStateException) { // executePendingTransactions() can fail, but this should "just work".
        }
        if (didExecutePendingTransactions) {
            executeFragmentTransaction(stateChange)
        } else { // failed to execute pending transactions
            if (!fragmentManager.isDestroyed) { // ignore state change if activity is dead. :(
                handler.post {
                    if (!fragmentManager.isDestroyed) { // ignore state change if activity is dead. :(
                        executeFragmentTransaction(stateChange)
                    }
                }
            }
        }
    }

    fun animatePrevFragment(
        fragment: Fragment,
        stateChange: StateChange,
        prevKey: AnimatedFragmentKey,
        newKey: AnimatedFragmentKey
    ) {
        when (stateChange.direction) {
            StateChange.FORWARD -> {
                fragment.exitTransition = prevKey.exitTransition(newKey)
            }
            StateChange.BACKWARD -> {
                fragment.exitTransition = prevKey.popExitTransition(newKey)
            }
            StateChange.REPLACE -> {
                fragment.exitTransition = prevKey.replaceExitTransition(newKey)
            }
        }
    }

    fun animateNewFragment(
        fragment: Fragment,
        stateChange: StateChange,
        prevKey: AnimatedFragmentKey?,
        newKey: AnimatedFragmentKey
    ) {
        prevKey?.let { nonNullPrevKey ->
            when (stateChange.direction) {
                StateChange.FORWARD -> {
                    fragment.enterTransition = newKey.enterTransition(nonNullPrevKey)
                }
                StateChange.BACKWARD -> {
                    fragment.enterTransition = newKey.popEnterTransition(nonNullPrevKey)
                }
                StateChange.REPLACE -> {
                    fragment.enterTransition = newKey.replaceEnterTransition(nonNullPrevKey)
                }
            }
        }
    }

    private fun executeFragmentTransaction(@Nonnull stateChange: StateChange) {
        val fragmentTransaction = fragmentManager.beginTransaction().disallowAddToBackStack()
        val previousKeys: List<AnimatedFragmentKey> = stateChange.getPreviousKeys()
        val newKeys: List<AnimatedFragmentKey> = stateChange.getNewKeys()
        for (oldKey in previousKeys) {
            val fragment = fragmentManager.findFragmentByTag(oldKey.fragmentTag)
            if (fragment != null) {
                animatePrevFragment(fragment, stateChange, oldKey, stateChange.topNewKey())
                if (!newKeys.contains(oldKey)) {
                    fragmentTransaction.remove(fragment)
                } else if (!isNotShowing(fragment)) {
                    stopShowing(fragmentTransaction, fragment)
                }
            }
        }
        for (newKey in newKeys) {
            var fragment = fragmentManager.findFragmentByTag(newKey.fragmentTag)
            if (newKey == stateChange.topNewKey()) {
                if (fragment != null) {
                    if (fragment.isRemoving) { // fragments are quirky, they die asynchronously. Ignore if they're still there.
                        val newKeyFrag = newKey.createFragment()
                        animateNewFragment(newKeyFrag, stateChange, stateChange.topPreviousKey(), newKey)
                        fragmentTransaction.replace(
                            containerId,
                            newKeyFrag,
                            newKey.fragmentTag
                        )
                    } else if (isNotShowing(fragment)) {
                        animateNewFragment(fragment, stateChange, stateChange.topPreviousKey(), newKey)
                        startShowing(fragmentTransaction, fragment)
                    }
                } else {
                    fragment = newKey.createFragment()
                    animateNewFragment(fragment, stateChange, stateChange.topPreviousKey(), newKey)
                    fragmentTransaction.add(containerId, fragment, newKey.fragmentTag)
                }
            } else {
                if (fragment != null && !isNotShowing(fragment)) {
                    stopShowing(fragmentTransaction, fragment)
                }
            }

        }
        fragmentTransaction.commitAllowingStateLoss()
    }
}