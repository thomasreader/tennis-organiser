package com.gitlab.thomasreader.tennisorganiser.domain.activity

import android.os.Parcelable
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.data.Match_set_rule
import com.gitlab.thomasreader.tennisorganiser.data.SetType
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

val SetRule.toMatchSetRule get(): Match_set_rule {
    return when (this) {
        is SetRule.AdvantageSet -> Match_set_rule(
            id = -1,
            type = this.setType,
            games_rule_first_to = this.gamesFirstTo,
            games_rule_win_by = this.gamesWinBy,
            game_rule_tiebreak_commences = null,
            tiebreak_rule_first_to = null,
            tiebreak_rule_win_by = null
        )
        is SetRule.TieBreak -> Match_set_rule(
            id = -1,
            type = this.setType,
            games_rule_first_to = null,
            games_rule_win_by = null,
            game_rule_tiebreak_commences = null,
            tiebreak_rule_first_to = this.tieBreakFirstTo,
            tiebreak_rule_win_by = this.tieBreakWinBy
        )
        is SetRule.TieBreakSet -> Match_set_rule(
            id = -1,
            type = this.setType,
            games_rule_first_to = this.gamesFirstTo,
            games_rule_win_by = this.gamesWinBy,
            game_rule_tiebreak_commences = this.gameTieBreakCommences,
            tiebreak_rule_first_to = this.tieBreakFirstTo,
            tiebreak_rule_win_by = this.tieBreakWinBy
        )
    }
}

val Match_set_rule.toSetRule get(): SetRule {
    return when (this.type) {
        SetType.TIEBREAK -> SetRule.TieBreak(
            tieBreakFirstTo = this.tiebreak_rule_first_to!!,
            tieBreakWinBy = this.tiebreak_rule_win_by!!
        )
        SetType.TIEBREAK_SET -> SetRule.TieBreakSet(
            gamesFirstTo = this.games_rule_first_to!!,
            gamesWinBy = this.games_rule_win_by!!,
            gameTieBreakCommences = this.game_rule_tiebreak_commences!!,
            tieBreakFirstTo = this.tiebreak_rule_first_to!!,
            tieBreakWinBy = this.tiebreak_rule_win_by!!
        )
        SetType.ADVANTAGE_SET -> SetRule.AdvantageSet(
            gamesFirstTo = this.games_rule_first_to!!,
            gamesWinBy = this.games_rule_win_by!!
        )
    }
}

sealed interface SetRule : Parcelable {
    val setType: SetType
    val ruleDesc: DroidTextProvider

    @Parcelize
    data class TieBreak(
        val tieBreakFirstTo: Int,
        val tieBreakWinBy: Int
    ) : SetRule {
        override val setType: SetType
            get() = SetType.TIEBREAK

        @IgnoredOnParcel
        override val ruleDesc: DroidTextProvider =
            DroidTextProvider.stringId(R.string.tiebreak_rule, tieBreakFirstTo, tieBreakWinBy)
    }

    @Parcelize
    data class TieBreakSet(
        val gamesFirstTo: Int,
        val gamesWinBy: Int,
        val gameTieBreakCommences: Int,
        val tieBreakFirstTo: Int,
        val tieBreakWinBy: Int
    ) : SetRule {
        override val setType: SetType
            get() = SetType.TIEBREAK_SET

        @IgnoredOnParcel
        override val ruleDesc: DroidTextProvider = DroidTextProvider.stringId(
            R.string.tiebreak_set_rule,
            gamesFirstTo,
            gamesWinBy,
            gameTieBreakCommences,
            tieBreakFirstTo,
            tieBreakWinBy
        )
    }

    @Parcelize
    data class AdvantageSet(
        val gamesFirstTo: Int,
        val gamesWinBy: Int
    ) : SetRule {
        override val setType: SetType
            get() = SetType.ADVANTAGE_SET

        @IgnoredOnParcel
        override val ruleDesc: DroidTextProvider =
            DroidTextProvider.stringId(R.string.advantage_rule, gamesFirstTo, gamesWinBy)
    }


}