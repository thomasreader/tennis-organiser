package com.gitlab.thomasreader.tennisorganiser.util

import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider

sealed class Resource<T> {
    abstract val data: T?

    fun toError(throwable: Throwable, message: DroidTextProvider?): Error<T> {
        return Error(throwable, message, this.data)
    }
    fun toLoading(): Loading<T> {
        if (this is Loading) {
            return this
        }
        return Loading(this.data)
    }
    fun toSuccess(data: T): Success<T> {
        return Success(data)
    }

    data class Success<T>(override val data: T): Resource<T>()
    data class Loading<T>(override val data: T? = null): Resource<T>()
    data class Error<T>(
        val throwable: Throwable,
        val message: DroidTextProvider?,
        override val data: T? = null
    ): Resource<T>()
}

