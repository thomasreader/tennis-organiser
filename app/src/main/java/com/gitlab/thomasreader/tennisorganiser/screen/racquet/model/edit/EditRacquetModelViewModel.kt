package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.edit

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.*
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import logcat.logcat
import java.sql.SQLException

sealed interface EditRacquetModelEvent {
    object Saved: EditRacquetModelEvent
}

data class EditRacquetModelState(
    val initialState: EditRacquetModelInitialValues?,
    val brandModelRes: DroidTextProvider,
    val brands: List<String>,
    val brandError: Boolean,
    val modelError: Boolean,
    val saveEnabled: Boolean
)

data class EditRacquetModelInitialValues(
    val brand: String,
    val model: String,
    val weight: String?,
    val weightUnit: DroidUnit<Mass>,
    val length: String?,
    val lengthUnit: DroidUnit<Length>,
    val headSize: String?,
    val headSizeUnit: DroidUnit<Area>,
    val balancePoint: String?,
    val balanceUnit: DroidUnit<Length>,
    val swingWeight: String?,
    val stiffness: String?,
    val tipWidth: String?,
    val beamWidth: String?,
    val shaftWidth: String?,
    val numMains: String?,
    val numCrosses: String?,
    val tensionLow: String?,
    val tensionHigh: String?,
    val tensionUnit: DroidUnit<Mass>,
    val mainsSkipThroat: String?,
    val mainsSkipHead: String?,
    val stringPieces: String?,
    val sharedHoles: String?
)

class EditRacquetModelViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<EditRacquetModelScreen>(_key) {
    private val _events = Channel<EditRacquetModelEvent>(Channel.BUFFERED)
    val events: Flow<EditRacquetModelEvent> = _events.receiveAsFlow()

    private val isInit = savedStateHandle.getStateFlow("initiated", false)

    var weightUnit: DroidUnit<Mass>? = null
        get() {
            val unitName: String? = savedStateHandle["weight_unit"]
            return unitName?.let { DroidUnits.MassUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["weight_unit"] = value?.name
            }
        }

    var lengthUnit: DroidUnit<Length>? = null
        get() {
            val unitName: String? = savedStateHandle["length_unit"]
            return unitName?.let { DroidUnits.LengthUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["length_unit"] = value?.name
            }
        }

    var headSizeUnit: DroidUnit<Area>? = null
        get() {
            val unitName: String? = savedStateHandle["head_size_unit"]
            return unitName?.let { DroidUnits.AreaUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["head_size_unit"] = value?.name
            }
        }

    var balancePointUnit: DroidUnit<Length>? = null
        get() {
            val unitName: String? = savedStateHandle["balance_point_unit"]
            return unitName?.let { DroidUnits.LengthUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["balance_point_unit"] = value?.name
            }
        }

    var tensionUnit: DroidUnit<Mass>? = null
        get() {
            val unitName: String? = savedStateHandle["tension_unit"]
            return unitName?.let { DroidUnits.MassUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["tension_unit"] = value?.name
            }
        }

    private val brandError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val modelError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val isSaving: MutableStateFlow<Boolean> = MutableStateFlow(false)

    private val initialValuesFlow = combine(
        isInit,
        database.racquetModelQueries.selectModelById(key.modelId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        dataStore.data
    ) { initiated, racquetModel, preferences ->
        return@combine when (initiated) {
            true -> null
            false -> {
                val weightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
                val lengthUnit = preferences.getMapped(DataStoreKeys.RACQUET_LENGTH)
                val headSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_HEAD_SIZE)
                val balancePointUnit = preferences.getMapped(DataStoreKeys.RACQUET_BALANCE_POINT)
                val widthUnit = DroidUnits.LengthUnits.MILLIMETRE
                val tensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)

                val weight = racquetModel.weight?.convertTo(weightUnit)?.let {
                    Constants.DisplayOptions.Format.weightFormatter(weightUnit).format(it)
                }
                val length = racquetModel.length?.convertTo(lengthUnit)?.let {
                    Constants.DisplayOptions.Format.lengthFormatter(lengthUnit).format(it)
                }
                val headSize = racquetModel.head_size?.convertTo(headSizeUnit)?.let {
                    Constants.DisplayOptions.Format.headSizeFormatter(headSizeUnit).format(it)
                }
                val balancePoint = racquetModel.balance_point?.convertTo(balancePointUnit)?.let {
                    Constants.DisplayOptions.Format.balancePointFormatter(balancePointUnit).format(it)
                }
                val widthFormat = Constants.DisplayOptions.Format.beamWidthFormatter()
                val beamWidth = racquetModel.width_beam?.convertTo(widthUnit)?.let {
                    widthFormat.format(it)
                }
                val tipWidth = racquetModel.width_tip?.convertTo(widthUnit)?.let {
                    widthFormat.format(it)
                }
                val shaftWidth = racquetModel.width_shaft?.convertTo(widthUnit)?.let {
                    widthFormat.format(it)
                }
                val tensionFormatter = Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)
                val tensionLow = racquetModel.tension_low?.convertTo(tensionUnit)?.let {
                    tensionFormatter.format(it)
                }
                val tensionHigh = racquetModel.tension_high?.convertTo(tensionUnit)?.let {
                    tensionFormatter.format(it)
                }

                return@combine EditRacquetModelInitialValues(
                    brand = racquetModel.brand,
                    model = racquetModel.model,
                    weight = weight,
                    weightUnit = weightUnit,
                    length = length,
                    lengthUnit = lengthUnit,
                    headSize = headSize,
                    headSizeUnit = headSizeUnit,
                    balancePoint = balancePoint,
                    balanceUnit = balancePointUnit,
                    swingWeight = racquetModel.swing_weight?.toString(),
                    stiffness = racquetModel.stiffness?.toString(),
                    tipWidth = tipWidth,
                    beamWidth = beamWidth,
                    shaftWidth = shaftWidth,
                    numMains = racquetModel.num_mains?.toString(),
                    numCrosses = racquetModel.num_crosses?.toString(),
                    tensionLow = tensionLow,
                    tensionHigh = tensionHigh,
                    tensionUnit = tensionUnit,
                    mainsSkipThroat = racquetModel.mains_skip_throat?.toString(),
                    mainsSkipHead = racquetModel.mains_skip_head?.toString(),
                    stringPieces = racquetModel.string_pieces?.toString(),
                    sharedHoles = racquetModel.shared_holes?.toString()

                )
            }
        }
    }
        .onEach { isInit.value = true }
        .catch { logcat { it.stackTraceToString() } }

    @Suppress("UNCHECKED_CAST")
    val uiState = combine(
        database.racquetModelQueries.selectBasicModelById(key.modelId) { _, brand, model, _ ->
            DroidTextProvider.stringId(R.string.brand_model_join, brand, model)
        }
            .asFlow()
            .mapToOne(Dispatchers.IO),
        initialValuesFlow,
        isSaving,
        brandError,
        modelError,
        database.miscQueries.selectAllBrands()
            .asFlow()
            .mapToList(Dispatchers.IO)
    ) {
        val brandModelRes = it[0] as DroidTextProvider
        val initialValues = it[1] as EditRacquetModelInitialValues?
        val isSaving = it[2] as Boolean
        val brandErr = it[3] as Boolean
        val modelErr = it[4] as Boolean
        val brands = it[5] as List<String>

        if (initialValues != null) {
            weightUnit = initialValues.weightUnit
            lengthUnit = initialValues.lengthUnit
            headSizeUnit = initialValues.headSizeUnit
            balancePointUnit = initialValues.balanceUnit
            tensionUnit = initialValues.tensionUnit
        }

        val saveDisabled = isSaving || brandErr || modelErr
        return@combine EditRacquetModelState(
            brandModelRes = brandModelRes,
            initialState = initialValues,
            brands = brands,
            brandError = brandErr,
            modelError = modelErr,
            saveEnabled = !saveDisabled
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun validateBrand(brand: String?) {
        brandError.value = brand.isNullOrBlank()
    }

    fun validateModel(model: String?) {
        modelError.value = model.isNullOrBlank()
    }

    fun editRacquetModel(
        brand: String?,
        model: String?,
        weight: Double?,
        length: Double?,
        headSize: Double?,
        balancePoint: Double?,
        swingWeight: Int?,
        stiffness: Int?,
        widthBeam: Double?,
        widthTip: Double?,
        widthShaft: Double?,
        numMains: Int?,
        numCrosses: Int?,
        tensionLow: Double?,
        tensionHigh: Double?,
        mainsSkipThroat: Int?,
        mainsSkipHead: Int?,
        stringPieces: Int?,
        sharedHoles: Int?
    ) {
        if (brand != null && model != null && !isSaving.value) {
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                val weightUnit = this@EditRacquetModelViewModel.weightUnit
                val lengthUnit = this@EditRacquetModelViewModel.lengthUnit
                val headSizeUnit = this@EditRacquetModelViewModel.headSizeUnit
                val balancePointUnit = this@EditRacquetModelViewModel.balancePointUnit
                val tensionUnit = this@EditRacquetModelViewModel.tensionUnit

                val internalWeight = when (weight != null && weightUnit != null) {
                    true -> weight * weightUnit
                    else -> null
                }
                val internalLength = when (length != null && lengthUnit != null) {
                    true -> length * lengthUnit
                    else -> null
                }
                val internalHeadSize = when (headSize != null && headSizeUnit != null) {
                    true -> headSize * headSizeUnit
                    else -> null
                }
                val internalBalancePoint = when (balancePoint != null && balancePointUnit != null) {
                    true -> balancePoint * balancePointUnit
                    else -> null
                }
                val internalTensionLow = when (tensionLow != null && tensionUnit != null) {
                    true -> tensionLow * tensionUnit
                    else -> null
                }
                val internalTensionHigh = when (tensionHigh != null && tensionUnit != null) {
                    true -> tensionHigh * tensionUnit
                    else -> null
                }
                try {
                    database.racquetModelQueries.updateModel(
                        id = key.modelId,
                        brand = brand,
                        model = model,
                        head_size = internalHeadSize,
                        length = internalLength,
                        weight = internalWeight,
                        balance_point = internalBalancePoint,
                        swing_weight = swingWeight,
                        stiffness = stiffness,
                        width_beam = widthBeam?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                        width_tip = widthTip?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                        width_shaft = widthShaft?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                        num_mains = numMains,
                        num_crosses = numCrosses,
                        tension_low = internalTensionLow,
                        tension_high = internalTensionHigh,
                        mains_skip_throat = mainsSkipThroat,
                        mains_skip_head = mainsSkipHead,
                        string_pieces = stringPieces,
                        shared_holes = sharedHoles
                    )
                    _events.send(EditRacquetModelEvent.Saved)
                } catch (sqlException: SQLException) {
                    isSaving.value = false
                }
            }
        }
    }
}