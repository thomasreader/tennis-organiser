package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitydetails

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DefaultItemAnimator
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.viewModel
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateText
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.data.SelectSetResultsById
import com.gitlab.thomasreader.tennisorganiser.databinding.ActivityDetailsBinding
import com.gitlab.thomasreader.tennisorganiser.domain.activity.*
import com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert.UpsertActivityScreen
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.random.Random

class ActivityDetailsFragment: KeyedFragment(R.layout.activity_details) {
    private val binding by bindView(ActivityDetailsBinding::bind)
    private lateinit var viewModel: ActivityDetailsViewModel
    private val tagChipAdapter = TagChipAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        with(binding) {
            toolbar.inflateMenu(R.menu.edit_delete_menu)
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.edit_action -> {
                        backstack.goTo(UpsertActivityScreen(viewModel.key.activityId))
                        true
                    }
                    R.id.delete_action -> {
                        confirmDeleteAlert()
                        true
                    }
                    else -> false
                }
            }

            binding.tagRecycler.apply {
                itemAnimator = DefaultItemAnimator()
                adapter = tagChipAdapter
            }
        }
    }

    private fun initViewModel() {
        viewModel = viewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            if (state.activity.venue != null) {
                                binding.activityVenue.text = state.activity.venue
                                binding.activityVenue.visible()
                            } else {
                                binding.activityVenue.gone()
                            }

                            binding.activityDate.text = state.activity.startedAt.toString(requireContext())
                            binding.activityTitle.text = state.activity.title
                            if (state.activity.description != null) {
                                binding.activityDescription.text = state.activity.description
                                binding.activityDescription.visible()
                            } else {
                                binding.activityDescription.gone()
                            }
                            binding.activityDuration.text = state.activity.duration.toString()
                            val surface = state.activity.surfaceRes?.let { getString(it) }
                            if (surface != null) {
                                binding.activitySurface.text = surface
                                binding.activitySurface.visible()
                                binding.activitySurfaceLabel.visible()
                                binding.durationSeparator.visible()
                            } else {
                                binding.activitySurface.gone()
                                binding.activitySurfaceLabel.gone()
                                binding.durationSeparator.gone()
                            }

                            if (state.racquet != null) {
                                binding.racquet.text = state.racquet.toString(requireContext())
                                binding.racquet.visible()
                                binding.racquetLabel.visible()
                            } else {
                                binding.racquet.gone()
                                binding.racquetLabel.gone()
                            }

                            tagChipAdapter.submitList(state.tags)
                            if (state.tags.isNotEmpty()) {
                                binding.tagLabel.visible()
                            } else {
                                binding.tagLabel.gone()
                            }

                            if (state.activity !is TennisMatch) {
                                binding.matchDetailsGroup.gone()
                            } else {
                                binding.matchDetailsGroup.visible()
                                binding.activityType.text = getString(state.activity.matchTypeRes)
                                binding.activityResult.text = getString(state.activity.result.stringResource)
                                binding.scoreBoard.winner = state.activity.result.result
                                binding.scoreBoard.seed = state.activity.seed
                                binding.scoreBoard.opponentSeed = state.activity.opponentSeed
                                binding.scoreBoard.setCount = state.rules.size
                                var set1: Pair<String, String>? = null
                                var set2: Pair<String, String>? = null
                                var set3: Pair<String, String>? = null
                                var set4: Pair<String, String>? = null
                                var set5: Pair<String, String>? = null
                                state.results
                                    .map { it?.toSet?.scorePair }
                                    .forEachIndexed { ix, result ->
                                    when (ix) {
                                        0 -> { set1 = result }
                                        1 -> { set2 = result }
                                        2 -> { set3 = result }
                                        3 -> { set4 = result }
                                        4 -> { set5 = result }
                                    }
                                }
                                binding.scoreBoard.setScore(
                                    set1, set2, set3, set4, set5
                                )

                                // TODO extra match info

                                val playerName = state.activity.userName.toString(requireContext())
                                when (state.activity) {
                                    is DoublesMatch -> {
                                        binding.scoreBoard.setDoublesPlayers(
                                            playerName,
                                            state.activity.partnerName.toString(requireContext()),
                                            state.activity.opponentOneName.toString(requireContext()),
                                            state.activity.opponentTwoName.toString(requireContext())
                                        )
                                    }
                                    is SinglesMatch -> {
                                        binding.scoreBoard.setSinglesPlayers(
                                            playerName,
                                            state.activity.opponentName.toString(requireContext()),
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            is ActivityDetailsEvent.Deleted -> {
                                backstack.goBack()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun confirmDeleteAlert() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_activity_title)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                viewModel.deleteActivity()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}