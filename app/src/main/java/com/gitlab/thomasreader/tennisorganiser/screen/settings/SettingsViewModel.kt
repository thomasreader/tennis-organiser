package com.gitlab.thomasreader.tennisorganiser.screen.settings

import androidx.annotation.StringRes
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

enum class SettingOptions(
    @StringRes val titleRes: Int
) {
    DARK(R.string.dark_mode_title),
    USERNAME(R.string.user_name_title),
    RACQUET_LENGTH(R.string.racquet_length),
    RACQUET_WEIGHT(R.string.racquet_weight),
    RACQUET_HEAD_SIZE(R.string.racquet_head_size),
    RACQUET_BALANCE_POINT(R.string.racquet_balance_point),
    RACQUET_GRIP_SIZE(R.string.racquet_grip_size),
    STRING_TENSION(R.string.string_tension)
}

data class SettingsState(
    val settings: Map<SettingOptions, DroidTextProvider>,
    val openDialog: OpenSettingsDialog?
)

sealed interface OpenSettingsDialog {
    val title: Int

    data class InputDialog(
        @StringRes override val title: Int,
        val text: DroidTextProvider?
    ): OpenSettingsDialog {
        val onSubmit = SettingsViewModel::setUsername
    }

    data class SelectSingleDialog(
        private val option: SettingOptions,
        val options: List<DroidTextProvider>,
        val selectedIndex: Int
    ): OpenSettingsDialog {
        override val title: Int = option.titleRes

        val onSubmit = when (option) {
            SettingOptions.DARK -> SettingsViewModel::setDarkMode
            SettingOptions.RACQUET_LENGTH -> SettingsViewModel::setLengthUnit
            SettingOptions.RACQUET_WEIGHT -> SettingsViewModel::setWeightUnit
            SettingOptions.RACQUET_HEAD_SIZE -> SettingsViewModel::setHeadSizeUnit
            SettingOptions.RACQUET_BALANCE_POINT -> SettingsViewModel::setBalancePointUnit
            SettingOptions.RACQUET_GRIP_SIZE -> SettingsViewModel::setGripSizeUnit
            SettingOptions.STRING_TENSION -> SettingsViewModel::setTensionUnit
            else -> throw IllegalStateException("Invalid setting option for single select dialog")
        }
    }
}

class SettingsViewModel (
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
) : KeyedViewModel<SettingsScreen>(_key) {
    private val openDialog = savedStateHandle.getStateFlow<SettingOptions?>("open_dialog")

    var userName: String? = savedStateHandle["user_name"]
        set(value) {
            savedStateHandle["user_name"] = value
            field = value
        }

    val uiState = combine(
        dataStore.data, openDialog
    ) { preferences, dialog ->
        val darkMode = preferences.getMapped(DataStoreKeys.DARK_MODE)
        val username = preferences.getMapped(DataStoreKeys.USERNAME)
        val racquetLengthUnit = preferences.getMapped(DataStoreKeys.RACQUET_LENGTH)
        val racquetWeightUnit = preferences.getMapped(DataStoreKeys.RACQUET_WEIGHT)
        val racquetHeadSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_HEAD_SIZE)
        val racquetBalancePointUnit = preferences.getMapped(DataStoreKeys.RACQUET_BALANCE_POINT)
        val racquetGripSizeUnit = preferences.getMapped(DataStoreKeys.RACQUET_GRIP_SIZE)
        val stringTensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)

        val settingsRows: Map<SettingOptions, DroidTextProvider> = mapOf(
            SettingOptions.DARK to DroidTextProvider.stringId(darkMode),
            SettingOptions.USERNAME to username,
            SettingOptions.RACQUET_LENGTH to racquetLengthUnit.descriptionRes,
            SettingOptions.RACQUET_WEIGHT to racquetWeightUnit.descriptionRes,
            SettingOptions.RACQUET_HEAD_SIZE to racquetHeadSizeUnit.descriptionRes,
            SettingOptions.RACQUET_BALANCE_POINT to racquetBalancePointUnit.descriptionRes,
            SettingOptions.RACQUET_GRIP_SIZE to racquetGripSizeUnit.descriptionRes,
            SettingOptions.STRING_TENSION to stringTensionUnit.descriptionRes
        )
        
        val openSettingsDialog: OpenSettingsDialog? = when (dialog) {
            SettingOptions.DARK -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.DARK_MODE_RES.map { DroidTextProvider.stringId(it) },
                    Constants.DisplayOptions.DARK_MODE_RES.indexOf(darkMode)
                )
            }
            SettingOptions.USERNAME -> {
                OpenSettingsDialog.InputDialog(
                    dialog.titleRes,
                    userName?.let { DroidTextProvider.text(it) } ?: username
                )
            }
            SettingOptions.RACQUET_LENGTH -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.RACQUET_LENGTH_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.RACQUET_LENGTH_UNITS.indexOf(racquetLengthUnit)
                )
            }
            SettingOptions.RACQUET_WEIGHT -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.RACQUET_WEIGHT_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.RACQUET_WEIGHT_UNITS.indexOf(racquetWeightUnit)
                )
            }
            SettingOptions.RACQUET_HEAD_SIZE -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.RACQUET_HEAD_SIZE_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.RACQUET_HEAD_SIZE_UNITS.indexOf(racquetHeadSizeUnit)
                )
            }
            SettingOptions.RACQUET_BALANCE_POINT -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.RACQUET_BALANCE_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.RACQUET_BALANCE_UNITS.indexOf(racquetBalancePointUnit)
                )
            }
            SettingOptions.RACQUET_GRIP_SIZE -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.RACQUET_GRIP_SIZE_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.RACQUET_GRIP_SIZE_UNITS.indexOf(racquetGripSizeUnit)
                )
            }
            SettingOptions.STRING_TENSION -> {
                OpenSettingsDialog.SelectSingleDialog(
                    dialog,
                    Constants.DisplayOptions.STRING_TENSION_UNITS.map { it.descriptionRes },
                    Constants.DisplayOptions.STRING_TENSION_UNITS.indexOf(stringTensionUnit)
                )
            }
            null -> null
        }
        
        SettingsState(settingsRows, openSettingsDialog)
    }
        .catch { e -> logcat { e.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun setDialog(option: SettingOptions?) {
        if (option == null) {
            userName = null
        }
        openDialog.value = option
    }

    fun setDarkMode(darkModeIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.DARK_MODE] = Constants.DisplayOptions.DARK_MODE[darkModeIndex]
            }
        }
    }

    fun setUsername(name: String?) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                name?.ifBlank { null }?.let {
                    preferences[DataStoreKeys.USERNAME] = it
                } ?: preferences.remove(DataStoreKeys.USERNAME)
            }
        }
    }

    fun setLengthUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.RACQUET_LENGTH] =
                    Constants.DisplayOptions.RACQUET_LENGTH_UNITS[uomIndex].name
            }
        }
    }

    fun setWeightUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.RACQUET_WEIGHT] =
                    Constants.DisplayOptions.RACQUET_WEIGHT_UNITS[uomIndex].name
            }
        }
    }

    fun setHeadSizeUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.RACQUET_HEAD_SIZE] =
                    Constants.DisplayOptions.RACQUET_HEAD_SIZE_UNITS[uomIndex].name
            }
        }
    }

    fun setBalancePointUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.RACQUET_BALANCE_POINT] =
                    Constants.DisplayOptions.RACQUET_BALANCE_UNITS[uomIndex].name
            }
        }
    }

    fun setGripSizeUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.RACQUET_GRIP_SIZE] =
                    Constants.DisplayOptions.RACQUET_GRIP_SIZE_UNITS[uomIndex].name
            }
        }
    }

    fun setTensionUnit(uomIndex: Int) {
        viewModelScope.launch(Dispatchers.IO + NonCancellable) {
            dataStore.edit { preferences ->
                preferences[DataStoreKeys.STRING_TENSION] =
                    Constants.DisplayOptions.STRING_TENSION_UNITS[uomIndex].name
            }
        }
    }
}