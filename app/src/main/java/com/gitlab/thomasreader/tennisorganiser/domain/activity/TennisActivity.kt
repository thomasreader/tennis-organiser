package com.gitlab.thomasreader.tennisorganiser.domain.activity

import androidx.annotation.StringRes
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.data.Activity
import com.gitlab.thomasreader.tennisorganiser.data.MatchResult

fun Activity.toDomainActivity(): TennisActivity {
    val duration = ElapsedTimeProvider(this.duration)
    val startedAt = RelativeDateTimeStringProvider(this.started_at)
    return TennisActivityImpl(
        id = id,
        title = title,
        description = description,
        surfaceRes = surface?.stringResource,
        venue = venue,
        startedAt = startedAt,
        duration = duration,
        racquetId = racquet_id
    )
}

fun Activity.toDomainMatch(userName: DroidTextProvider): TennisMatch {
    val duration = ElapsedTimeProvider(this.duration)
    val startedAt = RelativeDateTimeStringProvider(this.started_at)
    if (match_type!!.isSingles) {
        return SinglesMatch(
            id = id,
            title = title,
            description = description,
            surfaceRes = surface?.stringResource,
            venue = venue,
            startedAt = startedAt,
            duration = duration,
            racquetId = racquet_id,
            matchTypeRes = match_type!!.stringResource,
            seed = seed,
            opponentSeed = opponent_seed,
            result = result!!,
            userName = userName,
            userRating = user_rating,
            opponentName = opponent_one_name?.toDroidTextResource() ?: DroidTextProvider.stringId(R.string.opponent),
            opponentRating = opponent_one_rating
        )
    } else {
        return DoublesMatch(
            id = id,
            title = title,
            description = description,
            surfaceRes = surface?.stringResource,
            venue = venue,
            startedAt = startedAt,
            duration = duration,
            racquetId = racquet_id,
            matchTypeRes = match_type!!.stringResource,
            seed = seed,
            opponentSeed = opponent_seed,
            result = result!!,
            userName = userName,
            userRating = user_rating,
            partnerName = partner_name?.toDroidTextResource() ?: DroidTextProvider.stringId(R.string.partner),
            partnerRating = partner_rating,
            opponentOneName = opponent_one_name?.toDroidTextResource() ?: DroidTextProvider.stringId(R.string.opponent),
            opponentOneRating = opponent_one_rating,
            opponentTwoName = opponent_two_name?.toDroidTextResource() ?: DroidTextProvider.stringId(R.string.opponent),
            opponentTwoRating = opponent_two_rating
        )
    }
}

fun Activity.toDomain(userName: DroidTextProvider): TennisActivity {
        val duration = ElapsedTimeProvider(this.duration)
        val startedAt = RelativeDateTimeStringProvider(this.started_at)

        return when (is_match) {
            false -> TennisActivityImpl(
                id = id,
                title = title,
                description = description,
                surfaceRes = surface?.stringResource,
                venue = venue,
                startedAt = startedAt,
                duration = duration,
                racquetId = racquet_id
            )
            true -> {
                if (match_type!!.isSingles) {
                    SinglesMatch(
                        id = id,
                        title = title,
                        description = description,
                        surfaceRes = surface?.stringResource,
                        venue = venue,
                        startedAt = startedAt,
                        duration = duration,
                        racquetId = racquet_id,
                        matchTypeRes = match_type!!.stringResource,
                        seed = seed,
                        opponentSeed = opponent_seed,
                        result = result ?: MatchResult.RETIREMENT_LOSS,
                        userName = userName,
                        userRating = user_rating,
                        opponentName = opponent_one_name?.toDroidTextResource()
                            ?: DroidTextProvider.stringId(R.string.opponent),
                        opponentRating = opponent_one_rating
                    )
                } else {
                    DoublesMatch(
                        id = id,
                        title = title,
                        description = description,
                        surfaceRes = surface?.stringResource,
                        venue = venue,
                        startedAt = startedAt,
                        duration = duration,
                        racquetId = racquet_id,
                        matchTypeRes = match_type!!.stringResource,
                        seed = seed,
                        opponentSeed = opponent_seed,
                        result = result ?: MatchResult.RETIREMENT_LOSS,
                        userName = userName,
                        userRating = user_rating,
                        partnerName = partner_name?.toDroidTextResource()
                            ?: DroidTextProvider.stringId(R.string.partner),
                        partnerRating = partner_rating,
                        opponentOneName = opponent_one_name?.toDroidTextResource()
                            ?: DroidTextProvider.stringId(R.string.opponent),
                        opponentOneRating = opponent_one_rating,
                        opponentTwoName = opponent_two_name?.toDroidTextResource()
                            ?: DroidTextProvider.stringId(R.string.opponent),
                        opponentTwoRating = opponent_two_rating
                    )
                }
            }
        }
}

sealed interface TennisActivity {
    val id: Long
    val title: String
    val description: String?
    val surfaceRes: Int?
    val venue: String?
    val startedAt: ContextStringProvider
    val duration: ElapsedTimeProvider
    val racquetId: Long?
    val isMatch: Boolean
}

data class TennisActivityImpl(
    override val id: Long,
    override val title: String,
    override val description: String?,
    @StringRes override val surfaceRes: Int?,
    override val venue: String?,
    override val startedAt: ContextStringProvider,
    override val duration: ElapsedTimeProvider,
    override val racquetId: Long?
): TennisActivity {
    override val isMatch: Boolean
        get() = false
}

sealed interface TennisMatch: TennisActivity {
    val matchTypeRes: Int
    val seed: Int?
    val opponentSeed: Int?
    val result: MatchResult
    val userName: DroidTextProvider
    val userRating: String?
}

data class SinglesMatch(
    override val id: Long,
    override val title: String,
    override val description: String?,
    @StringRes override val surfaceRes: Int?,
    override val venue: String?,
    override val startedAt: ContextStringProvider,
    override val duration: ElapsedTimeProvider,
    override val racquetId: Long?,
    @StringRes override val matchTypeRes: Int,
    override val seed: Int?,
    override val opponentSeed: Int?,
    override val result: MatchResult,
    override val userName: DroidTextProvider,
    override val userRating: String?,
    val opponentName: DroidTextProvider,
    val opponentRating: String?
): TennisMatch {
    override val isMatch: Boolean
        get() = true
}

data class DoublesMatch(
    override val id: Long,
    override val title: String,
    override val description: String?,
    @StringRes override val surfaceRes: Int?,
    override val venue: String?,
    override val startedAt: ContextStringProvider,
    override val duration: ElapsedTimeProvider,
    override val racquetId: Long?,
    @StringRes override val matchTypeRes: Int,
    override val seed: Int?,
    override val opponentSeed: Int?,
    override val result: MatchResult,
    override val userName: DroidTextProvider,
    override val userRating: String?,
    val partnerName: DroidTextProvider,
    val partnerRating: String?,
    val opponentOneName: DroidTextProvider,
    val opponentOneRating: String?,
    val opponentTwoName: DroidTextProvider,
    val opponentTwoRating: String?
): TennisMatch {
    override val isMatch: Boolean
        get() = true
}