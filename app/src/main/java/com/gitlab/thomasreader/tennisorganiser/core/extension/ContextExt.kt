package com.gitlab.thomasreader.tennisorganiser.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.gitlab.thomasreader.tennisorganiser.core.data.ContextStringProvider

fun Context.colour(@ColorRes colorId: Int): Int {
    return ContextCompat.getColor(this, colorId)
}

@SuppressLint("UseCompatLoadingForDrawables")
fun Context.drawable(@DrawableRes drawableId: Int, theme: Resources.Theme? = null): Drawable {
    return this.resources.getDrawable(drawableId, theme)
}

fun Context.toast(@StringRes stringId: Int, short: Boolean = true) {
    Toast.makeText(
        this,
        stringId,
        if (short) Toast.LENGTH_SHORT else Toast.LENGTH_LONG
    ).show()
}

fun Context.toast(charSequence: CharSequence, short: Boolean = true) {
    Toast.makeText(
        this,
        charSequence,
        if (short) Toast.LENGTH_SHORT else Toast.LENGTH_LONG
    ).show()
}

fun Context.toast(contextStringProvider: ContextStringProvider, short: Boolean = true) {
    Toast.makeText(
        this,
        contextStringProvider.toString(this),
        if (short) Toast.LENGTH_SHORT else Toast.LENGTH_LONG
    ).show()
}