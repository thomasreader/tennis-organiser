package com.gitlab.thomasreader.tennisorganiser.core.anim

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import androidx.appcompat.widget.Toolbar
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.animateToolbarOnScroll(toolbar: Toolbar) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        var runningAnimation: ValueAnimator? = null
        var animatingDirection: Int = 0

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (recyclerView.canScrollVertically(1) || recyclerView.canScrollVertically(-1)) {
                if (!recyclerView.canScrollVertically(-1)) {
                    if (toolbar.elevation != 0f && animatingDirection != -1) {
                        runningAnimation?.cancel()
                        runningAnimation = ObjectAnimator.ofFloat(toolbar, "elevation", 0f).apply {
                            doOnStart { animatingDirection = -1 }
                            doOnEnd {
                                runningAnimation = null
                                animatingDirection = 0
                            }
                            duration = 400L
                            start()
                        }
                    }
                } else if (toolbar.elevation != 4f && animatingDirection != 1) {
                    runningAnimation?.cancel()
                    runningAnimation = ObjectAnimator.ofFloat(toolbar, "elevation", 4f).apply {
                        doOnStart { animatingDirection = 1 }
                        doOnEnd {
                            runningAnimation = null
                            animatingDirection = 0
                        }
                        duration = 400L
                        start()
                    }
                }
            }
        }
    })
}