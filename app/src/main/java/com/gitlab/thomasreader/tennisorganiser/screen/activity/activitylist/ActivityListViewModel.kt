package com.gitlab.thomasreader.tennisorganiser.screen.activity.activitylist

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DataStoreKeys
import com.gitlab.thomasreader.tennisorganiser.core.data.getMapped
import com.gitlab.thomasreader.tennisorganiser.core.savestate.getStateFlow
import com.gitlab.thomasreader.tennisorganiser.data.Activity
import com.gitlab.thomasreader.tennisorganiser.data.SetType
import com.gitlab.thomasreader.tennisorganiser.domain.activity.MatchScore
import com.gitlab.thomasreader.tennisorganiser.domain.activity.Set
import com.gitlab.thomasreader.tennisorganiser.domain.activity.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import logcat.logcat

class ActivityListViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
): KeyedViewModel<ActivityListScreen>(_key) {
    val search = savedStateHandle.getStateFlow<String>("search", "")
    private val _search = MutableStateFlow(search.value)
    @OptIn(FlowPreview::class)
    private val bufferedSearch = _search.debounce(500)

    init {
        viewModelScope.launch(Dispatchers.IO) {
            bufferedSearch.collect {
                search.value = it
            }
        }
    }

    private val searchQuery = search.map { query ->
        withContext(Dispatchers.IO) {
            if (query.isBlank()) {
                return@withContext null
            } else {
                val activities: List<Activity> = database.activityQueries.transactionWithResult {
                    database.activityQueries.selectActivityByTitleOrTag(query).executeAsList()
                }
                return@withContext activities
            }
        }
    }.flowOn(Dispatchers.IO)

    val uiState = combine(
        searchQuery,
        database.activityQueries.selectAllActivities()
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.matchSetResultQueries.selectResultsAndType()
            .asFlow()
            .mapToList(Dispatchers.IO)
            .map { setResults ->
                 setResults.groupBy { it.activity_id!! }
                     .mapValues { mapEntry ->
                         val results = mapEntry.value
                         if (results.isNotEmpty()) {
                             return@mapValues MatchScore(
                                 activityId = results[0].activity_id!!,
                                 numSets = results.size,
                                 sets = results.map { resultAndType ->
                                     when (resultAndType.type) {
                                         SetType.TIEBREAK -> Set.TieBreak(
                                             points = resultAndType.team_tiebreak_points!!,
                                             opponentPoints = resultAndType.opponent_tiebreak_points!!
                                         )
                                         SetType.TIEBREAK_SET -> Set.TieBreakSet(
                                             games = resultAndType.team_games!!,
                                             opponentGames = resultAndType.opponent_games!!,
                                             points = resultAndType.team_tiebreak_points,
                                             opponentPoints = resultAndType.opponent_tiebreak_points
                                         )
                                         SetType.ADVANTAGE_SET -> Set.Advantage(
                                             games = resultAndType.team_games!!,
                                             opponentGames = resultAndType.opponent_games!!
                                         )
                                     }
                                 }
                             )
                         } else {
                             null
                         }
                     }
            },
        dataStore.data
    ) { filtered, activitiesUris, sets, preferences ->
        val name = preferences.getMapped(DataStoreKeys.USERNAME)
        val result = filtered ?: activitiesUris
        return@combine result.map { it.toDomain(name) to sets[it.id] }
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun search(search: String) {
        if (search == "" || search.isNotBlank()) {
            this._search.value = search
        }
    }
}