package com.gitlab.thomasreader.tennisorganiser.domain.racquet

import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.SelectRacquetListInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

data class RacquetList(
    val id: Long,
    val identifier: String,
    val weight: String?,
    val swingWeight: Int?,
    val brand: String,
    val model: String,
    val headSize: String?,
//    val string_bed_id: Long?,
//    val racquet_id: Long?,
//    val mains_id: Long?,
//    val cross_id: Long?,
    val hasStringBed: Boolean,
    val mainsTension: String?,
    val crossTension: String?,
    val mainsThickness: String?,
    val mainsBrand: String?,
    val mainsModel: String?,
    val crossThickness: String?,
    val crossBrand: String?,
    val crossModel: String?
)

suspend fun List<SelectRacquetListInfo>.toRacquetList(
    weightUnit: DroidUnit<Mass>,
    headSizeUnit: DroidUnit<Area>,
    tensionUnit: DroidUnit<Mass>
): List<RacquetList> = withContext(Dispatchers.Default) {
    val weightFormatter = Constants.DisplayOptions.Format.weightFormatter(weightUnit)
    val headSizeFormatter = Constants.DisplayOptions.Format.headSizeFormatter(headSizeUnit)
    val tensionFormatter  = Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)
    val stringThicknessFormatter = Constants.DisplayOptions.Format.stringThicknessFormatter()

    this@toRacquetList.map { racquetListDb: SelectRacquetListInfo ->
        RacquetList(
            id = racquetListDb.id,
            identifier = racquetListDb.identifier,
            weight = racquetListDb.weight?.toSymbolString(weightUnit, weightFormatter),
            swingWeight = racquetListDb.swing_weight,
            brand = racquetListDb.brand,
            model = racquetListDb.model,
            headSize = racquetListDb.head_size?.toSymbolString(headSizeUnit, headSizeFormatter),
            hasStringBed = racquetListDb.string_bed_id != null,
            mainsTension = racquetListDb.mains_tension?.toSymbolString(tensionUnit, tensionFormatter),
            crossTension = racquetListDb.cross_tension?.toSymbolString(tensionUnit, tensionFormatter),
            mainsThickness = racquetListDb.mains_thickness?.toSymbolString(DroidUnits.LengthUnits.MILLIMETRE, stringThicknessFormatter),
            mainsBrand = racquetListDb.mains_brand,
            mainsModel = racquetListDb.mains_model,
            crossThickness = racquetListDb.cross_thickness?.toSymbolString(DroidUnits.LengthUnits.MILLIMETRE, stringThicknessFormatter),
            crossBrand = racquetListDb.cross_brand,
            crossModel = racquetListDb.cross_model
        )
    }
}
