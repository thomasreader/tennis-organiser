package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.Constants
import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.core.extension.action
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomAbbreviation
import com.gitlab.thomasreader.tennisorganiser.core.uom.UomArrayAdapters
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetAddBinding
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.nullStringRes
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AddRacquetFragment: KeyedFragment(R.layout.racquet_add) {
    private val binding by bindView(RacquetAddBinding::bind)
    private val saveButton: ActionMenuItemView get() = binding.toolbar.action(R.id.save_action)
    private lateinit var viewModel: AddRacquetViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)

        with (binding) {
            toolbar.title = resources.getString(R.string.add_racquet)
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        saveRacquet()
                        true
                    }
                    else -> false
                }
            }

            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }

            viewModel.updateIdentifier(idInput.text?.toString())
            idInput.doOnTextChanged { text, _, _, _ ->
                val identifier = text?.toString()
                if (identifier != null) {
                    viewModel.updateIdentifier(identifier)
                }
            }

            weightUnit.setAdapter(
                UomArrayAdapters.dropDown(
                    requireContext(),
                    UomAbbreviation.SYMBOL,
                    Constants.DisplayOptions.RACQUET_WEIGHT_UNITS
                )
            )
            weightUnit.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.weightUnitIx = position
                }

            balancePointUnitInput.setAdapter(
                UomArrayAdapters.dropDown(
                    requireContext(),
                    UomAbbreviation.SYMBOL,
                    Constants.DisplayOptions.RACQUET_BALANCE_UNITS
                )
            )
            balancePointUnitInput.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.balancePointUnitIx = position
                }
        }
    }

    private fun initViewModel() {
        viewModel = savedViewModel()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            binding.toolbar.subtitle = state.racquetInfoRes.toString(resources)
                            binding.idLayout.updateErrorVisibility(
                                state.idError,
                                DroidTextProvider.REQUIRED
                            )
                            saveButton.isEnabled = state.saveEnabled
                        }
                    }
                }
                launch {
                    viewModel.events.collect { event ->
                        when (event) {
                            is AddRacquetEvent.Saved -> backstack.jumpToRoot(StateChange.FORWARD)
                        }
                    }
                }
            }
        }
    }

    private fun assertGoBack() {
        AlertDialog.Builder(requireContext())
            .setTitle("Discard entry?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun saveRacquet() {
        val identifier = binding.idInput.text?.toString()
        val weight = binding.weightInput.text?.toString()?.toDoubleOrNull()
        val swingWeight = binding.swingWeightInput.text?.toString()?.toIntOrNull()
        val balancePoint = binding.balancePointInput.text?.toString()?.toDoubleOrNull()
        val notes = binding.notesInput.text?.toString()?.ifEmpty { null }

        viewModel.addRacquet(
            identifier = identifier,
            weight = weight,
            swingWeight = swingWeight,
            balancePoint = balancePoint,
            notes = notes
        )
    }
}