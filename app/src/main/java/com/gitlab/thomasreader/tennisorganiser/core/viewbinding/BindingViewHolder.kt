package com.gitlab.thomasreader.tennisorganiser.core.viewbinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class BindingViewHolder<T: ViewBinding>(
    val binding: T
): RecyclerView.ViewHolder(binding.root)

inline fun <T: ViewBinding> inflateBinding(
    parent: ViewGroup,
    inflater: (LayoutInflater, ViewGroup, Boolean) -> T
): T {
    return inflater(
        LayoutInflater.from(parent.context),
        parent,
        false
    )
}