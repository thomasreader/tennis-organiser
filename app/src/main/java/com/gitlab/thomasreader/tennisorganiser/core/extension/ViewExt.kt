package com.gitlab.thomasreader.tennisorganiser.core.extension

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidTextProvider
import com.gitlab.thomasreader.tennisorganiser.util.oneIsNull
import com.gitlab.thomasreader.tennisorganiser.util.toInt
import com.google.android.material.textfield.TextInputLayout
import com.zhuinden.simplestackextensions.navigatorktx.backstack

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun <T: View> T.visibleWhen(predicate: (T) -> Boolean): T = this.apply {
    if (predicate(this)) visible() else gone()
}

fun <T: View> T.visibleWhen(predicate: Boolean): T = this.apply {
    if (predicate) visible() else gone()
}

inline fun <T: View, U: Any> T.visibleWhenNotNull(
    value: U?,
    crossinline block: (T, U) -> Unit
): T = this.apply {
    when (value != null) {
        true -> {
            block(this, value)
            visible()
        }
        false -> gone()
    }
}

fun TextInputLayout.updateErrorVisibility(error: DroidTextProvider?): Boolean {
    if (oneIsNull(this.error, error)) {
        this.error = error?.toString(resources)
        return true
    } else {
        return false
    }
}

fun TextInputLayout.updateErrorVisibility(error: CharSequence?): Boolean {
    if (oneIsNull(this.error, error)) {
        this.error = error
        return true
    } else {
        return false
    }
}

fun TextInputLayout.updateErrorVisibility(show: Boolean, error: CharSequence): Boolean {
    if ((this.isErrorEnabled.toInt() + show.toInt()) == 1) {
        this.error = error
        return true
    } else {
        this.error = null
        return false
    }
}

fun TextInputLayout.updateErrorVisibility(show: Boolean, @StringRes error: Int): Boolean {
    if (!show) {
        this.error = null
        return false
    } else if (this.isErrorEnabled != show) {
        this.error = context.getString(error)
        return true
    } else {
        return false
    }
}

fun TextInputLayout.updateErrorVisibility(show: Boolean, error: DroidTextProvider): Boolean {
    if (((this.error != null).toInt() + show.toInt()) == 1) {
        this.error = if (show) error.toString(resources) else null
        return true
    } else {
        return false
    }
}

inline fun <T> TextView.updateText(text: T?, block: (T) -> CharSequence) {
    val currentPayload = this.getTag(R.id.text_view_droid_res)
    if (currentPayload != text) {
        this.text = text?.let {
            this.setTag(R.id.text_view_droid_res, text)
            block(it)
        }
    }
}

inline fun <T> TextView.updateTextIfNotFocused(text: T?, block: (T) -> CharSequence) {
    if (!this.isFocused) {
        val currentPayload = this.getTag(R.id.text_view_droid_res)
        if (currentPayload != text) {
            this.text = text?.let {
                this.setTag(R.id.text_view_droid_res, text)
                block(it)
            }
        }
    }
}

fun TextView.updateTextIfNotFocused(text: String?) {
    if (!this.isFocused) {
        val currentPayload = this.getTag(R.id.text_view_droid_res)
        if (currentPayload != text) {
            this.text = text
            this.setTag(R.id.text_view_droid_res, text)
        }
    }
}

fun Toolbar.addUpNavigationIfNeeded(): Boolean {
    val isUpEnabled = backstack.getHistory<Nothing>().size > 1
    if (isUpEnabled) {
        this.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        this.setNavigationOnClickListener { backstack.goBack() }
    }
    return isUpEnabled
}

inline fun AutoCompleteTextView.onItemIxClickListener(crossinline listener: (Int) -> Unit) {
    this.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
        listener(position)
    }
}

inline fun <T> AutoCompleteTextView.onItemClickListener(crossinline listener: (T) -> Unit) {
    this.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
        listener(adapterView.getItemAtPosition(position) as T)
    }
}

fun <T: View> Toolbar.action(@IdRes action: Int): T {
    return this.getTag(action) as T? ?: this.findViewById<T>(action).also {
        this.setTag(action, it)
    }
}

fun <T> AutoCompleteTextView.updateAdapterValues(values: List<T>) {
    val currentPayload = this.getTag(R.id.autocomplete_current_list)
    if (this.adapter != null && this.adapter is ArrayAdapter<*> && currentPayload != values) {
        this.setTag(R.id.autocomplete_current_list, values)
        with (this.adapter as ArrayAdapter<T>) {
            clear()
            addAll(values)
            notifyDataSetChanged()
        }
    }
}