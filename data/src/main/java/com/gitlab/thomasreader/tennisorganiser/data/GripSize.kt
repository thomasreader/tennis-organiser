package com.gitlab.thomasreader.tennisorganiser.data

import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Quantity

enum class GripSize(
    val diameter: Quantity<Length>
) {
    L0(Quantity(0.1016)),
    L1(Quantity(0.104775)),
    L2(Quantity(0.10795)),
    L3(Quantity(0.111125)),
    L4(Quantity(0.1143)),
    L5(Quantity(0.117475)),
    L6(Quantity(0.12065))
}