package com.gitlab.thomasreader.tennisorganiser.screen.activity.upsert

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.EntryChipBinding

class TagAdapter(
    val closeListener: (String) -> Unit
): ListAdapter<String, TagAdapter.ViewHolder>(StringDiffer()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflateBinding(parent, EntryChipBinding::inflate))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(
        binding: EntryChipBinding
    ): BindingViewHolder<EntryChipBinding>(binding), View.OnClickListener {
        init {
            this.binding.entryChip.setOnCloseIconClickListener(this)
        }

        override fun onClick(v: View) {
            closeListener(currentList[bindingAdapterPosition])
        }

        fun bind(tag: String) {
            this.binding.entryChip.text = tag
        }
    }

    class StringDiffer: DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: String, newItem: String): Any? {
            return oldItem to newItem
        }
    }
}