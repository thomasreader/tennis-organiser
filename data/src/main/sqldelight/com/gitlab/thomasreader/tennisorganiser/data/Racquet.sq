import com.gitlab.thomasreader.kuantity.quantity.Length;
import com.gitlab.thomasreader.kuantity.quantity.Mass;
import com.gitlab.thomasreader.kuantity.quantity.Quantity;
import com.gitlab.thomasreader.tennisorganiser.data.GripSize;

CREATE TABLE racquet (
    id INTEGER NOT NULL PRIMARY KEY,
    model_id INTEGER NOT NULL,
    identifier TEXT NOT NULL,
    weight REAL AS Quantity<Mass>,
    swing_weight INTEGER AS Int,
    balance_point REAL AS Quantity<Length>,
    notes TEXT,

    CONSTRAINT racquet_model_id_FK FOREIGN KEY (model_id) REFERENCES racquet_model(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE INDEX racquet_model_id_IX ON racquet(model_id);

insertRacquet:
INSERT INTO racquet(model_id, identifier, weight, swing_weight, balance_point, notes)
VALUES (?, ?, ?, ?, ?, ?);

updateRacquet:
UPDATE racquet
SET
    identifier = :identifier,
    weight = :weight,
    swing_weight = :swing_weight,
    balance_point = :balance_point,
    notes = :notes
WHERE id = :id;

deleteRacquet:
DELETE
FROM racquet
WHERE id = :id;

selectRacquets:
SELECT
    id,
    model_id,
    identifier,
    weight,
    swing_weight,
    balance_point,
    notes
FROM racquet;

selectRacquetById:
SELECT
    id,
    model_id,
    identifier,
    weight,
    swing_weight,
    balance_point,
    notes
FROM racquet
WHERE id = :id;

selectRacquetTensionRangeById:
WITH unique_racquet AS (
SELECT model_id
FROM racquet
WHERE id = ?
)
SELECT
    racquet_model.tension_low,
    racquet_model.tension_high
FROM unique_racquet
INNER JOIN racquet_model ON unique_racquet.model_id = racquet_model.id;

selectRacquetInfo:
WITH unique_racquet AS (
SELECT
    identifier,
    model_id
FROM racquet
WHERE id = :id
)
SELECT
    racquet_model.brand,
    racquet_model.model,
    unique_racquet.identifier
FROM unique_racquet
INNER JOIN racquet_model ON unique_racquet.model_id = racquet_model.id;

selectSimpleRacquetsAsc:
SELECT
    racquet.id,
    racquet.identifier,
    racquet_model.brand,
    racquet_model.model
FROM racquet
INNER JOIN racquet_model ON racquet.model_id = racquet_model.id
ORDER BY racquet_model.brand ASC, racquet_model.model ASC, racquet.identifier ASC;

selectModelFromId:
WITH racquet_model_id AS (
SELECT model_id
FROM racquet
WHERE id = :id
)
SELECT racquet_model.*
FROM racquet_model_id
INNER JOIN racquet_model
ON racquet_model_id.model_id = racquet_model.id;

selectRacquetListInfo:
WITH racquet_info AS (
    SELECT
        id,
        model_id,
        identifier,
        weight,
        swing_weight
    FROM racquet
), racquet_model_info AS (
    SELECT
        id,
        brand,
        model,
        head_size
    FROM racquet_model
), racquet_with_model AS (
    SELECT
        racquet_info.id,
        racquet_info.identifier,
        racquet_info.weight,
        racquet_info.swing_weight,
        racquet_model_info.brand,
        racquet_model_info.model,
        racquet_model_info.head_size
        FROM racquet_info
        INNER JOIN racquet_model_info
            ON racquet_info.model_id = racquet_model_info.id
), latest_string_bed AS (
    SELECT
        id,
        racquet_id,
        mains_id,
        cross_id,
        mains_tension,
        cross_tension,
        MAX(strung_at) AS strung_at
    FROM string_bed
    GROUP BY racquet_id
), latest_string_bed_info AS (
    SELECT
        latest_string_bed.id AS string_bed_id,
        latest_string_bed.racquet_id,
        latest_string_bed.mains_id,
        latest_string_bed.cross_id,
        latest_string_bed.mains_tension,
        latest_string_bed.cross_tension,
        mains_variant.thickness AS mains_thickness,
        mains_model.brand AS mains_brand,
        mains_model.model AS mains_model,
        cross_variant.thickness AS cross_thickness,
        cross_model.brand AS cross_brand,
        cross_model.model AS cross_model
    FROM latest_string_bed
    INNER JOIN string_variant AS mains_variant
        ON latest_string_bed.mains_id = mains_variant.id
    INNER JOIN string_variant AS cross_variant
        ON latest_string_bed.cross_id = cross_variant.id
    INNER JOIN string_model AS mains_model
        ON mains_variant.model_id = mains_model.id
    INNER JOIN string_model AS cross_model
        ON cross_variant.model_id = cross_model.id
)
SELECT *
FROM racquet_with_model
LEFT JOIN latest_string_bed_info
    ON racquet_with_model.id = latest_string_bed_info.racquet_id;