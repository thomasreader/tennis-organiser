package com.gitlab.thomasreader.tennisorganiser.screen.string.edit

import com.gitlab.thomasreader.tennisorganiser.core.navigation.AnimatedFragmentKey
import kotlinx.parcelize.Parcelize

@Parcelize
class EditStringModelScreen(
    val stringModelId: Long
): AnimatedFragmentKey() {
    override fun instantiateFragment() = EditStringModelFragment()
    override fun hideBottomNavigation(): Boolean = true
}