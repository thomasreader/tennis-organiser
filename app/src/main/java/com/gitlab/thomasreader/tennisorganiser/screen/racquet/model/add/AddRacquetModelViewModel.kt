package com.gitlab.thomasreader.tennisorganiser.screen.racquet.model.add

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.times
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.add.AddRacquetScreen
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat
import java.sql.SQLException

data class AddRacquetModelState(
    val brands: List<String>,
    val saveEnabled: Boolean,
    val brandErr: Boolean,
    val modelErr: Boolean
)

sealed interface AddRacquetModelEvent {
    data class Saved(val modelId: Long) : AddRacquetModelEvent
}

class AddRacquetModelViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>,
    private val savedStateHandle: SavedStateHandle
) : KeyedViewModel<AddRacquetScreen>(_key) {
    private val _events = Channel<AddRacquetModelEvent>(Channel.BUFFERED)
    val events: Flow<AddRacquetModelEvent> = _events.receiveAsFlow()

    var weightUnit: DroidUnit<Mass>? = null
        get() {
            val unitName: String? = savedStateHandle["weight_unit"]
            return unitName?.let { DroidUnits.MassUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["weight_unit"] = value?.name
            }
        }

    var lengthUnit: DroidUnit<Length>? = null
        get() {
            val unitName: String? = savedStateHandle["length_unit"]
            return unitName?.let { DroidUnits.LengthUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["length_unit"] = value?.name
            }
        }

    var headSizeUnit: DroidUnit<Area>? = null
        get() {
            val unitName: String? = savedStateHandle["head_size_unit"]
            return unitName?.let { DroidUnits.AreaUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["head_size_unit"] = value?.name
            }
        }

    var balancePointUnit: DroidUnit<Length>? = null
        get() {
            val unitName: String? = savedStateHandle["balance_point_unit"]
            return unitName?.let { DroidUnits.LengthUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["balance_point_unit"] = value?.name
            }
        }

    var tensionUnit: DroidUnit<Mass>? = null
        get() {
            val unitName: String? = savedStateHandle["tension_unit"]
            return unitName?.let { DroidUnits.MassUnits.getUnit(it) }
        }
        set(value) {
            if (field != value) {
                field = value
                savedStateHandle["tension_unit"] = value?.name
            }
        }

    private val brandError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val modelError: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val isSaving: MutableStateFlow<Boolean> = MutableStateFlow(false)

    val uiState: StateFlow<AddRacquetModelState?> = combine(
        database.miscQueries.selectAllBrands()
            .asFlow()
            .mapToList(Dispatchers.IO),
        brandError,
        modelError,
        isSaving
    ) { brands, brandErr, modelErr, isSaving ->
        val saveDisabled = isSaving || brandErr || modelErr

        AddRacquetModelState(
            brands = brands,
            saveEnabled = !saveDisabled,
            brandErr = brandErr,
            modelErr = modelErr
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)


    fun validateBrand(brand: String?) {
        brandError.value = brand.isNullOrBlank()
    }

    fun validateModel(model: String?) {
        modelError.value = model.isNullOrBlank()
    }

    fun addRacquetModel(
        brand: String?,
        model: String?,
        weight: Double?,
        length: Double?,
        headSize: Double?,
        balancePoint: Double?,
        swingWeight: Int?,
        stiffness: Int?,
        widthBeam: Double?,
        widthTip: Double?,
        widthShaft: Double?,
        numMains: Int?,
        numCrosses: Int?,
        tensionLow: Double?,
        tensionHigh: Double?,
        mainsSkipThroat: Int?,
        mainsSkipHead: Int?,
        stringPieces: Int?,
        sharedHoles: Int?
    ) {
        println(headSize)
        println(headSizeUnit)
        println(length)
        println(lengthUnit)
        println(weight)
        println(weightUnit)
        println(balancePoint)
        println(balancePointUnit)
        println(tensionLow)
        println(tensionHigh)
        println(tensionUnit)
        if (brand != null && model != null && !isSaving.value) {
            isSaving.value = true
            viewModelScope.launch(Dispatchers.IO + NonCancellable) {
                val weightUnit = this@AddRacquetModelViewModel.weightUnit
                val lengthUnit = this@AddRacquetModelViewModel.lengthUnit
                val headSizeUnit = this@AddRacquetModelViewModel.headSizeUnit
                val balancePointUnit = this@AddRacquetModelViewModel.balancePointUnit
                val tensionUnit = this@AddRacquetModelViewModel.tensionUnit

                val internalWeight = when (weight != null && weightUnit != null) {
                    true -> weight * weightUnit
                    else -> null
                }
                val internalLength = when (length != null && lengthUnit != null) {
                    true -> length * lengthUnit
                    else -> null
                }
                val internalHeadSize = when (headSize != null && headSizeUnit != null) {
                    true -> headSize * headSizeUnit
                    else -> null
                }
                val internalBalancePoint = when (balancePoint != null && balancePointUnit != null) {
                    true -> balancePoint * balancePointUnit
                    else -> null
                }
                val internalTensionLow = when (tensionLow != null && tensionUnit != null) {
                    true -> tensionLow * tensionUnit
                    else -> null
                }
                val internalTensionHigh = when (tensionHigh != null && tensionUnit != null) {
                    true -> tensionHigh * tensionUnit
                    else -> null
                }
                try {
                    val modelId = database.transactionWithResult<Long> {
                        database.racquetModelQueries.insertModel(
                            brand = brand,
                            model = model,
                            head_size = internalHeadSize,
                            length = internalLength,
                            weight = internalWeight,
                            balance_point = internalBalancePoint,
                            swing_weight = swingWeight,
                            stiffness = stiffness,
                            width_beam = widthBeam?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                            width_tip = widthTip?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                            width_shaft = widthShaft?.let { it * DroidUnits.LengthUnits.MILLIMETRE },
                            num_mains = numMains,
                            num_crosses = numCrosses,
                            tension_low = internalTensionLow,
                            tension_high = internalTensionHigh,
                            mains_skip_throat = mainsSkipThroat,
                            mains_skip_head = mainsSkipHead,
                            string_pieces = stringPieces,
                            shared_holes = sharedHoles
                        )
                        database.utilQueries.selectLastInsertRowId().executeAsOne()
                    }
                    _events.send(AddRacquetModelEvent.Saved(modelId))
                } catch (sqlException: SQLException) {
                    isSaving.value = false
                }
            }
        }
    }
}