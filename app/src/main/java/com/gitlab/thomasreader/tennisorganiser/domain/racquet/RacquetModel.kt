package com.gitlab.thomasreader.tennisorganiser.domain.racquet

import com.gitlab.thomasreader.kuantity.quantity.Area
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.Racquet_model

fun Racquet_model.toDomain(
    lengthUnit: DroidUnit<Length>,
    weightUnit: DroidUnit<Mass>,
    headSizeUnit: DroidUnit<Area>,
    balanceUnit: DroidUnit<Length>,
    tensionUnit: DroidUnit<Mass>
): RacquetModel {
    val tensionFormat = Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)

    val headSizeString = this.head_size?.toSymbolString(headSizeUnit, Constants.DisplayOptions.Format.headSizeFormatter(headSizeUnit))
    val lengthString = this.length?.toSymbolString(lengthUnit, Constants.DisplayOptions.Format.lengthFormatter(lengthUnit))
    val weightString = this.weight?.toSymbolString(weightUnit, Constants.DisplayOptions.Format.weightFormatter(weightUnit))
    val balanceString = this.balance_point?.toSymbolString(balanceUnit, Constants.DisplayOptions.Format.balancePointFormatter(balanceUnit))

    val beamWidthRes = when (this.width_tip != null && this.width_beam != null && this.width_shaft != null) {
        true -> DroidTextProvider.stringId(
            R.string.beam_width_format,
            this.width_tip!!.convertTo(DroidUnits.LengthUnits.MILLIMETRE),
            this.width_beam!!.convertTo(DroidUnits.LengthUnits.MILLIMETRE),
            this.width_shaft!!.convertTo(DroidUnits.LengthUnits.MILLIMETRE)
        )
        false -> null
    }

    val skipRes = when (this.mains_skip_throat != null && this.mains_skip_head != null) {
        true -> DroidTextProvider.stringId(R.string.racquet_string_skip_join,
            this.mains_skip_throat!!, mains_skip_head!!
        )
        false -> null
    }
    val tensionRangeRes = when (this.tension_low != null && this.tension_high != null) {
        true -> {
            DroidTextProvider.stringId(
                R.string.tension_range_join,
                tensionFormat.format(this.tension_low!!.convertTo(tensionUnit)),
                tensionFormat.format(this.tension_high!!.convertTo(tensionUnit)),
                tensionUnit.symbol ?: ""
            )
        }
        false -> null
    }
    val stringPatternRes = when (this.num_mains != null && this.num_crosses != null) {
        true -> DroidTextProvider.stringId(R.string.string_pattern_join,
            this.num_mains!!, this.num_crosses!!
        )
        false -> null
    }

    return RacquetModel(
        databaseModel = this,
        headSizeString = headSizeString,
        lengthString = lengthString,
        weightString = weightString,
        balanceString = balanceString,
        beamWidthRes = beamWidthRes,
        tensionRangeRes = tensionRangeRes,
        skipRes = skipRes,
        stringPatternRes = stringPatternRes
    )
}

class RacquetModel(
    private val databaseModel: Racquet_model,
    val headSizeString: String?,
    val lengthString: String?,
    val weightString: String?,
    val balanceString: String?,
    val beamWidthRes: DroidTextProvider?,
    val tensionRangeRes: DroidTextProvider?,
    val skipRes: DroidTextProvider?,
    val stringPatternRes: DroidTextProvider?
) {
    val id: Long get() = databaseModel.id
    val brand: String get() = databaseModel.brand
    val model: String get() = databaseModel.model
    val brandModelRes: DroidTextProvider = DroidTextProvider.stringId(R.string.brand_model_join, brand, model)
    val headSize: Quantity<Area>? get() = databaseModel.head_size
    val length: Quantity<Length>? get() = databaseModel.length
    val weight: Quantity<Mass>? get() = databaseModel.weight
    val balancePoint: Quantity<Length>? get() = databaseModel.balance_point
    val swingWeight: Int? get() = databaseModel.swing_weight
    val stiffness: Int? get() = databaseModel.stiffness
    val widthBeam: Quantity<Length>? get() = databaseModel.width_beam
    val widthTip: Quantity<Length>? get() = databaseModel.width_tip
    val widthShaft: Quantity<Length>? get() = databaseModel.width_shaft
    val numMains: Int? get() = databaseModel.num_mains
    val numCrosses: Int? get() = databaseModel.num_crosses
    val tensionLow: Quantity<Mass>? get() = databaseModel.tension_low
    val tensionHigh: Quantity<Mass>? get() = databaseModel.tension_high
    val mainsSkipThroat: Int? get() = databaseModel.mains_skip_throat
    val mainsSkipHead: Int? get() = databaseModel.mains_skip_head
    val stringPieces: Int? get() = databaseModel.string_pieces
    val sharedHoles: Int? get() = databaseModel.shared_holes

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RacquetModel

        if (databaseModel != other.databaseModel) return false

        return true
    }

    override fun hashCode(): Int {
        return databaseModel.hashCode()
    }
}