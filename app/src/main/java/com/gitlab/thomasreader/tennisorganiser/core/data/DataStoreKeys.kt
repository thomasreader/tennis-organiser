package com.gitlab.thomasreader.tennisorganiser.core.data

import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.datastore.preferences.core.MutablePreferences
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits

fun <T, M> Preferences.getMapped(key: MappedDataStoreKey<T, M>): M = key.getMapped(this)
operator fun <T> Preferences.get(key: DataStoreKey<T>): T = key.get(this)
operator fun <T> MutablePreferences.set(key: DataStoreKey<T>, value: T) = key.set(this, value)
fun MutablePreferences.remove(key: DataStoreKey<*>) = key.remove(this)

interface DataStoreKeys {
    companion object {
        private fun <T> key(key: Preferences.Key<T>, defaultValue: T): DataStoreKey<T> {
            return NonNullDataStoreKey(key, defaultValue)
        }

        private fun <T, M> key(key: Preferences.Key<T>, defaultValue: T, mapper: (T) -> M): MappedDataStoreKey<T, M> {
            return NonNullMappedDataStoreKey(key, defaultValue, mapper)
        }

        @JvmStatic
        val DARK_MODE = key(
            intPreferencesKey("dark_mode_setting"),
            when (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                true -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                false -> AppCompatDelegate.MODE_NIGHT_NO
            }
        ) { darkMode ->
            when (darkMode) {
                AppCompatDelegate.MODE_NIGHT_YES -> R.string.dark_mode
                AppCompatDelegate.MODE_NIGHT_NO -> R.string.light_mode
                AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM -> R.string.follow_system
                else -> throw IllegalStateException("Invalid dark mode used")
            }
        }

        @JvmStatic
        val USERNAME = key(
            stringPreferencesKey("user_name_setting"),
            ""
            ) { string ->
            when (string.isBlank()) {
                true -> DroidTextProvider.stringId(R.string.default_username)
                false -> DroidTextProvider.text(string)
            }
        }

        @JvmStatic
        val RACQUET_LENGTH = key(
            stringPreferencesKey("racquet_length_unit_setting"),
            DroidUnits.LengthUnits.CENTIMETRE.name
        ) {
            when (it) {
                DroidUnits.LengthUnits.CENTIMETRE.name -> DroidUnits.LengthUnits.CENTIMETRE
                DroidUnits.LengthUnits.INCH.name -> DroidUnits.LengthUnits.INCH
                else -> throw IllegalStateException("Invalid string tension unit $it")
            }
        }

        @JvmStatic
        val RACQUET_WEIGHT = key(
            stringPreferencesKey("racquet_weight_unit_setting"),
            DroidUnits.MassUnits.GRAM.name
        ) {
            when (it) {
                DroidUnits.MassUnits.GRAM.name -> DroidUnits.MassUnits.GRAM
                DroidUnits.MassUnits.OUNCE.name -> DroidUnits.MassUnits.OUNCE
                else -> throw IllegalStateException("Invalid weight unit $it")
            }
        }

        @JvmStatic
        val RACQUET_HEAD_SIZE = key(
            stringPreferencesKey("racquet_head_size_unit_setting"),
            DroidUnits.AreaUnits.SQUARE_CENTIMETRE.name
        ) {
            when (it) {
                DroidUnits.AreaUnits.SQUARE_CENTIMETRE.name -> DroidUnits.AreaUnits.SQUARE_CENTIMETRE
                DroidUnits.AreaUnits.SQUARE_INCH.name -> DroidUnits.AreaUnits.SQUARE_INCH
                else -> throw IllegalStateException("Invalid head size unit $it")
            }
        }

        @JvmStatic
        val RACQUET_BALANCE_POINT = key(
            stringPreferencesKey("racquet_balance_point_unit_setting"),
            DroidUnits.LengthUnits.CENTIMETRE.name
        ) {
            when (it) {
                DroidUnits.LengthUnits.CENTIMETRE.name -> DroidUnits.LengthUnits.CENTIMETRE
                DroidUnits.LengthUnits.INCH.name -> DroidUnits.LengthUnits.INCH
                DroidUnits.LengthUnits.BALANCE_POINT.name -> DroidUnits.LengthUnits.BALANCE_POINT
                else -> throw IllegalStateException("Invalid balance point unit $it")
            }
        }

        @JvmStatic
        val RACQUET_GRIP_SIZE = key(
            stringPreferencesKey("racquet_grip_size_unit_setting"),
            "L"
        ){
            when (it) {
                DroidUnits.LengthUnits.CENTIMETRE.name -> DroidUnits.LengthUnits.CENTIMETRE
                DroidUnits.LengthUnits.INCH.name -> DroidUnits.LengthUnits.INCH
                DroidUnits.LengthUnits.GRIP_SIZE.name -> DroidUnits.LengthUnits.BALANCE_POINT // TODO
                else -> DroidUnits.LengthUnits.CENTIMETRE//throw IllegalStateException("Invalid grip size unit $it")
            }
        }

        @JvmStatic
        val STRING_TENSION = key(
            stringPreferencesKey("string_tension_unit_setting"),
            DroidUnits.MassUnits.KILOGRAM.name
        ) {
            when (it) {
                DroidUnits.MassUnits.KILOGRAM.name -> DroidUnits.MassUnits.KILOGRAM
                DroidUnits.MassUnits.POUND.name -> DroidUnits.MassUnits.POUND
                else -> throw IllegalStateException("Invalid string tension unit $it")
            }
        }
    }
}

sealed interface DataStoreKey<T> {
    fun set(preferences: MutablePreferences, value: T)
    fun get(preferences: Preferences): T
    fun remove(preferences: MutablePreferences)
}

sealed interface MappedDataStoreKey<T, M>: DataStoreKey<T> {
    fun getMapped(preferences: Preferences): M
}

private open class NonNullDataStoreKey<T>(
    protected val key: Preferences.Key<T>,
    private val defaultValue: T
): DataStoreKey<T> {
    override fun set(preferences: MutablePreferences, value: T) {
        preferences[key] = value
    }

    override fun get(preferences: Preferences): T {
        return preferences[key] ?: defaultValue
    }

    override fun remove(preferences: MutablePreferences) {
        preferences.remove(key)
    }
}

private class NonNullMappedDataStoreKey<T, M>(
    key: Preferences.Key<T>,
    defaultValue: T,
    private val mapper: (T) -> M
): NonNullDataStoreKey<T>(key, defaultValue), MappedDataStoreKey<T, M> {
    override fun getMapped(preferences: Preferences): M {
        return mapper(get(preferences))
    }
}

//private open class NullableDataKey<T>(
//    protected val key: Preferences.Key<T>
//): DataKey<T?> {
//    override fun set(preferences: MutablePreferences, value: T?) {
//        if (value != null) preferences[key] = value //else remove(preferences)
//    }
//
//    override fun get(preferences: Preferences): T? {
//        return preferences[key]
//    }
//
//    override fun remove(preferences: MutablePreferences) {
//        preferences.remove(key)
//    }
//}
//private class MappedNullableDataKey<T, M>(
//    key: Preferences.Key<T>,
//    private val mapper: (T) -> M
//): NullableDataKey<T>(key), MappedDataKey<T?, M?> {
//    override fun getMapped(preferences: Preferences): M? {
//        return get(preferences)?.let { mapper(it) }
//    }
//}
//private class NonNullMappedDataKey<T, M>(
//    key: Preferences.Key<T>,
//    private val mapper: (T?) -> M
//): NullableDataKey<T>(key), MappedDataKey<T?, M> {
//    override fun getMapped(preferences: Preferences): M {
//        return mapper(get(preferences))
//    }
//}