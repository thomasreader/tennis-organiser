package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.extension.gone
import com.gitlab.thomasreader.tennisorganiser.core.extension.visible
import com.gitlab.thomasreader.tennisorganiser.core.extension.visibleWhen
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.BindingViewHolder
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.inflateBinding
import com.gitlab.thomasreader.tennisorganiser.databinding.RacquetRecyclerRowBinding
import com.gitlab.thomasreader.tennisorganiser.domain.racquet.RacquetList

class RacquetListAdapter(
    val onClickListener: (RacquetList) -> Unit
) : ListAdapter<RacquetList, RacquetListAdapter.ViewHolder>(RacquetDiffCallBack()) {
    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            inflateBinding(parent, RacquetRecyclerRowBinding::inflate)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(
        binding: RacquetRecyclerRowBinding
    ) : BindingViewHolder<RacquetRecyclerRowBinding>(binding), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        fun bind(racquet: RacquetList) {
            with(binding) {
                racquetModel.text = itemView.context.resources.getString(
                    R.string.brand_model_join,
                    racquet.brand,
                    racquet.model
                )
                racquetTitle.text = racquet.identifier

                racquetWeight.visibleWhen(racquet.weight != null)
                racquetWeight.text = racquet.weight

                racquetSwingWeight.visibleWhen(racquet.swingWeight != null)
                racquetSwingWeight.text = racquet.swingWeight?.let {
                    itemView.context.getString(R.string.swing_weight_format, it)
                }

                racquetHeadSize.visibleWhen(racquet.headSize != null)
                racquetHeadSize.text = racquet.headSize

                if (racquet.hasStringBed) {
                    racquetStringBedGroup.visible()
                    racquetStringMains.text = itemView.context.resources.getString(
                        R.string.brand_model_join,
                        racquet.mainsBrand,
                        racquet.mainsModel
                    )
                    racquetStringCrosses.text = itemView.context.resources.getString(
                        R.string.brand_model_join,
                        racquet.crossBrand,
                        racquet.crossModel
                    )
                    racquetStringMainsInfo.text = itemView.context.resources.getString(
                        R.string.string_thickness_tension_join,
                        racquet.mainsThickness,
                        racquet.mainsTension
                    )
                    racquetStringCrossesInfo.text = itemView.context.resources.getString(
                        R.string.string_thickness_tension_join,
                        racquet.crossThickness,
                        racquet.crossTension
                    )
                } else {
                    racquetStringBedGroup.gone()
                }
            }

        }

        override fun onClick(v: View) {
            onClickListener(currentList[this.bindingAdapterPosition])
        }
    }

    private class RacquetDiffCallBack : DiffUtil.ItemCallback<RacquetList>() {
        override fun areItemsTheSame(oldItem: RacquetList, newItem: RacquetList): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: RacquetList, newItem: RacquetList): Boolean {
            return oldItem == newItem
        }
    }
}