package com.gitlab.thomasreader.tennisorganiser.domain.string

import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Mass
import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.format.toSymbolString
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.SelectStringJobsByRacquetId
import java.time.Duration

fun List<SelectStringJobsByRacquetId>.toDomain(
    tensionUnit: DroidUnit<Mass>
): List<StringBed> {
    val stringTensionFormatter = Constants.DisplayOptions.Format.stringTensionFormatter(tensionUnit)
    val stringThicknessFormatter = Constants.DisplayOptions.Format.stringThicknessFormatter()

    return this.map { dbStringJob ->
        val stringAgeDays = Duration
            .ofMillis(
                when (dbStringJob.duration != null) {
                    true -> dbStringJob.duration!!
                    false -> System.currentTimeMillis() - dbStringJob.strung_at
                }
            )
            .toDays()
            .toInt()

        val dateRangeRes = when (dbStringJob.cut_at != null) {
            true -> DateTimeRangeProvider(dbStringJob.strung_at, dbStringJob.cut_at!!)
            false -> DateTimeProvider(dbStringJob.strung_at)
        }
        return@map StringBed(
            databaseModel = dbStringJob,
            mainTensionString = dbStringJob.mains_tension.toSymbolString(
                tensionUnit,
                stringTensionFormatter
            ),
            crossTensionString = dbStringJob.cross_tension.toSymbolString(
                tensionUnit,
                stringTensionFormatter
            ),
            mainThicknessString = dbStringJob.main_thickness.toSymbolString(
                DroidUnits.LengthUnits.MILLIMETRE,
                stringThicknessFormatter
            ),
            crossThicknessString = dbStringJob.cross_thickness.toSymbolString(
                DroidUnits.LengthUnits.MILLIMETRE,
                stringThicknessFormatter
            ),
            dateRangeRes = dateRangeRes,
            elapsedRes = DroidTextProvider.pluralId(
                R.plurals.string_duration_format,
                stringAgeDays,
                stringAgeDays
            ),
        )
    }
}

class StringBed(
    private val databaseModel: SelectStringJobsByRacquetId,
    val mainTensionString: String,
    val crossTensionString: String,
    val mainThicknessString: String,
    val crossThicknessString: String,
    val dateRangeRes: ContextStringProvider,
    val elapsedRes: DroidTextProvider
) {
    val mainsVariantInfo = DroidTextProvider.stringId(
        R.string.string_variant_brand_model_thickness,
        mainBrand,
        mainModel,
        mainThicknessString
    )
    val crossVariantInfo = DroidTextProvider.stringId(
        R.string.string_variant_brand_model_thickness,
        crossBrand,
        crossModel,
        crossThicknessString
    )
    val id: Long get() = databaseModel.id
    val strungAt: Long get() = databaseModel.strung_at
    val cutAt: Long? get() = databaseModel.cut_at
    val duration: Long? get() = databaseModel.duration
    val mainsId: Long get() = databaseModel.mains_id
    val crossId: Long get() = databaseModel.cross_id
    val mainsTension: Quantity<Mass> get() = databaseModel.mains_tension
    val crossTension: Quantity<Mass> get() = databaseModel.cross_tension
    val mainBrand: String get() = databaseModel.main_brand
    val mainModel: String get() = databaseModel.main_model
    val mainThickness: Quantity<Length> get() = databaseModel.main_thickness
    val crossBrand: String get() = databaseModel.cross_brand
    val crossModel: String get() = databaseModel.cross_model
    val crossThickness: Quantity<Length> get() = databaseModel.cross_thickness
    val notes: String? get() = databaseModel.notes

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StringBed

        if (databaseModel != other.databaseModel) return false

        return true
    }

    override fun hashCode(): Int {
        return databaseModel.hashCode()
    }
}