package com.gitlab.thomasreader.tennisorganiser.screen.racquet.uniqueracket.stringhistory

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.viewModelScope
import com.gitlab.thomasreader.data.TennisDatabase
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.navigation.KeyedViewModel
import com.gitlab.thomasreader.tennisorganiser.core.data.*
import com.gitlab.thomasreader.tennisorganiser.domain.string.StringBed
import com.gitlab.thomasreader.tennisorganiser.domain.string.toDomain
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.zhuinden.simplestackextensions.fragments.DefaultFragmentKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import logcat.logcat

data class StringHistoryState(
    val subtitle: DroidTextProvider,
    val stringBeds: List<StringBed>,
)

class RacquetStringHistoryViewModel(
    _key: DefaultFragmentKey,
    private val database: TennisDatabase,
    private val dataStore: DataStore<Preferences>
): KeyedViewModel<RacquetStringHistoryScreen>(_key) {
    val uiState: StateFlow<StringHistoryState?> = combine(
        database.stringBedQueries.selectStringJobsByRacquetId(key.racquetId)
            .asFlow()
            .mapToList(Dispatchers.IO),
        database.racquetQueries.selectRacquetInfo(key.racquetId)
            .asFlow()
            .mapToOne(Dispatchers.IO),
        dataStore.data
    ) { stringBeds, racquetInfo, preferences ->
        val tensionUnit = preferences.getMapped(DataStoreKeys.STRING_TENSION)
        StringHistoryState(
            subtitle = DroidTextProvider.stringId(
                R.string.racquet_identifier_brand_model,
                racquetInfo.identifier,
                racquetInfo.brand,
                racquetInfo.model
            ),
            stringBeds = stringBeds.toDomain(tensionUnit)
        )
    }
        .catch { logcat { it.stackTraceToString() } }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

    fun deleteStringJob(id: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            database.stringBedQueries.deleteStringBed(id)
        }
    }
}