package com.gitlab.thomasreader.tennisorganiser.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.gitlab.thomasreader.tennisorganiser.core.dependency.Dependencies
import com.gitlab.thomasreader.tennisorganiser.core.data.DataStoreKeys
import com.gitlab.thomasreader.tennisorganiser.core.data.get
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import logcat.AndroidLogcatLogger
import logcat.LogPriority.VERBOSE

val Application.version: String get() {
    return this.packageManager.getPackageInfo(this.packageName, 0).versionName
}

class App : Application() {
    lateinit var dependencies: Dependencies
        private set

    override fun onCreate() {
        super.onCreate()
        AndroidLogcatLogger.installOnDebuggableApp(this, minPriority = VERBOSE)
        this.dependencies = Dependencies(this)
        runBlocking {
            dependencies.dataStore(this@App).data.first { preferences ->
                AppCompatDelegate.setDefaultNightMode(preferences[DataStoreKeys.DARK_MODE])
                true
            }
        }
    }
}