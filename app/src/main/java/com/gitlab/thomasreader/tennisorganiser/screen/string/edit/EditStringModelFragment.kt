package com.gitlab.thomasreader.tennisorganiser.screen.string.edit

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.gitlab.thomasreader.tennisorganiser.R

import com.gitlab.thomasreader.tennisorganiser.core.navigation.savedViewModel
import com.gitlab.thomasreader.tennisorganiser.databinding.StringEditBinding
import com.gitlab.thomasreader.tennisorganiser.core.viewbinding.bindView
import com.gitlab.thomasreader.tennisorganiser.core.extension.updateErrorVisibility
import com.zhuinden.simplestackextensions.fragments.KeyedFragment
import com.zhuinden.simplestackextensions.fragmentsktx.backstack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class EditStringModelFragment : KeyedFragment(R.layout.string_edit) {
    private val binding: StringEditBinding by bindView(StringEditBinding::bind)
    private lateinit var viewModel: EditStringModelViewModel
    private var isFormInit = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                assertGoBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )

        with(binding) {
            toolbar.setNavigationOnClickListener {
                assertGoBack()
            }
            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.save_action -> {
                        saveStringModel()
                        true
                    }
                    else -> false
                }
            }
            binding.stringBrandInput.doOnTextChanged { text, _, _, _ ->
                val brand = text?.toString()
                if (brand != null) {
                    viewModel.validateBrand(brand)
                }
            }
            binding.stringModelInput.doOnTextChanged { text, _, _, _ ->
                val model = text?.toString()
                if (model != null) {
                    viewModel.validateModel(model)
                }
            }
        }
    }

    private fun assertGoBack() {
        AlertDialog.Builder(requireContext())
            .setTitle("Discard entry?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                backstack.goBack()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun saveStringModel() {
        val brand = binding.stringBrandInput.text?.toString()
        val model = binding.stringModelInput.text?.toString()
        val composition = binding.stringCompositionInput.text?.toString()

        viewModel.updateStringModel(
            brand = brand,
            model = model,
            composition = composition
        )
    }

    private fun initViewModel() {
        viewModel = savedViewModel()

        viewLifecycleOwner.lifecycleScope.launch {
            launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.uiState.collect { state ->
                        if (state != null) {
                            with(state) {
                                if (!viewModel.isInitiated) {
                                    viewModel.isInitiated = true
                                    binding.stringBrandInput.setText(stringModel.brand)
                                    binding.stringModelInput.setText(stringModel.model)
                                    binding.stringCompositionInput.setText(stringModel.composition)
                                }
                                if (!isFormInit) {
                                    isFormInit = true
                                    binding.toolbar.subtitle = stringModel.brandModelRes.toString(resources)

                                    val brandAdapter = ArrayAdapter<String>(
                                        requireContext(),
                                        android.R.layout.simple_dropdown_item_1line,
                                        brands
                                    )
                                    val compositionAdapter = ArrayAdapter<String>(
                                        requireContext(),
                                        android.R.layout.simple_dropdown_item_1line,
                                        compositions
                                    )
                                    binding.stringBrandInput.setAdapter(brandAdapter)
                                    binding.stringCompositionInput.setAdapter(compositionAdapter)
                                }
                                binding.stringBrandLayout.updateErrorVisibility(brandError)
                                binding.stringModelLayout.updateErrorVisibility(modelError)
                                binding.toolbar.menu.findItem(R.id.save_action)?.isEnabled = saveEnabled
                            }
                        }
                    }
                }
            }
            launch {
                viewModel.events.collect { event ->
                    when (event) {
                        is EditStringModelEvent.Saved -> backstack.goBack()
                        is EditStringModelEvent.UniqueConstraintFailed -> {
                            Toast.makeText(
                                requireContext(),
                                event.msg.toString(resources),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
            }
        }
    }
}