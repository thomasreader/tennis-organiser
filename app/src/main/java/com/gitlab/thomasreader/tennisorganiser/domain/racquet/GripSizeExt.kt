package com.gitlab.thomasreader.tennisorganiser.domain.racquet

import androidx.annotation.StringRes
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.tennisorganiser.R
import com.gitlab.thomasreader.tennisorganiser.core.data.DroidUnit
import com.gitlab.thomasreader.tennisorganiser.core.uom.DroidUnits
import com.gitlab.thomasreader.tennisorganiser.data.GripSize

val GripSize.centimetreRes get(): @StringRes Int {
    return when (this) {
        GripSize.L0 -> R.string.l0_centimetre
        GripSize.L1 -> R.string.l1_centimetre
        GripSize.L2 -> R.string.l2_centimetre
        GripSize.L3 -> R.string.l3_centimetre
        GripSize.L4 -> R.string.l4_centimetre
        GripSize.L5 -> R.string.l5_centimetre
        GripSize.L6 -> R.string.l6_centimetre
    }
}

val GripSize.inchRes get(): @StringRes Int {
    return when (this) {
        GripSize.L0 -> R.string.l0_inch
        GripSize.L1 -> R.string.l1_inch
        GripSize.L2 -> R.string.l2_inch
        GripSize.L3 -> R.string.l3_inch
        GripSize.L4 -> R.string.l4_inch
        GripSize.L5 -> R.string.l5_inch
        GripSize.L6 -> R.string.l6_inch
    }
}

fun GripSize.stringRes(unit: DroidUnit<Length>): Int {
    return when (unit) {
        DroidUnits.LengthUnits.CENTIMETRE -> this.centimetreRes
        DroidUnits.LengthUnits.INCH -> this.inchRes
        else -> throw IllegalArgumentException("Invalid unit passed for grip size")
    }
}

fun GripSize.nullStringRes(unit: DroidUnit<Length>): Int? {
    return when (unit) {
        DroidUnits.LengthUnits.CENTIMETRE -> this.centimetreRes
        DroidUnits.LengthUnits.INCH -> this.inchRes
        else -> null
    }
}