package com.gitlab.thomasreader.tennisorganiser.data

import com.squareup.sqldelight.ColumnAdapter

enum class MatchResult(
    val result: Int
) {
    RETIREMENT_WIN(2),
    WIN(1),
    DRAW(0),
    LOSS(-1),
    RETIREMENT_LOSS(-2);
}

fun matchResultAdapter(): ColumnAdapter<MatchResult, Long> {
    return object: ColumnAdapter<MatchResult, Long> {
        override fun decode(databaseValue: Long): MatchResult {
            return when (databaseValue.toInt()) {
                2 -> MatchResult.RETIREMENT_WIN
                1 -> MatchResult.WIN
                0 -> MatchResult.DRAW
                -1 -> MatchResult.LOSS
                -2 -> MatchResult.RETIREMENT_LOSS
                else -> { throw IllegalStateException("Illegal match result type") }
            }
        }

        override fun encode(value: MatchResult): Long {
            return value.result.toLong()
        }
    }
}